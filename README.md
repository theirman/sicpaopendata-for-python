# Utiliser SicpaOpenData dans un projet Python

Ce projet consiste en la cr�ation d'une librairie au format WHL pour faciliter les d�ploiements de sets de donn�es vers les portails d'ouverture des donn�es de l'INRAE

## Pr�requis :
-   Le fichier WHL `SicpaOpenDataPython-0.1.0-py3-none-any.whl`
-   `pip` pour pouvoir installer le module

## Installation de la librairie : 

-   Sur le terminal du projet Python, executer la commande : `pip install /chemin/vers/SicpaOpenDataPython-0.1.0-py3-none-any.whl`

## Pour utiliser SicpaOpenData dans votre projet Python 
```python
#Utiliser l'API ScipaOpenDataPython.API
from SicpaOpenDataPython.API.DataverseAPI import DataverseAPI
#Utiliser l'API ScipaOpenDataPython.Entity
from SicpaOpenDataPython.Entity.<Classe> import <Classe>
#Utiliser l'aide pour voir comment construire le fichier de metadata et les nodes qui le composent
from SicpaOpenDataPython.Examples.MetadataExamples import MetadataExamples
```

Et pour la suite, consulter la documentation technique de l�API