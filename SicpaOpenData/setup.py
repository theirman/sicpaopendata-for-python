#! /usr/bin/env python3
# coding: utf-8

#
# COMMAND : python setup.py bdist_wheel
#

from setuptools import find_packages, setup

setup(
    name='SicpaOpenDataPython',
    packages=
    [
        "SicpaOpenDataPython/API", 
        "SicpaOpenDataPython/Entity", 
        "SicpaOpenDataPython/Examples", 
        "SicpaOpenDataPython/Helper"
    ],
    version='1.0.0',
    description='Libririe Python pour envoyer des données sur data-preproduction',
    author='Tom VINCENT',
    license='GNU GPL v2',
    install_requires=['requests']
)
