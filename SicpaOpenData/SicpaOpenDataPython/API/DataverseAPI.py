#! /usr/bin/env python3
# coding: utf-8

import re
import requests

class DataverseAPI():
    """!
    Classe implémentant les méthodes d'accès aux dataverses INRAE
    @author Tom VINCENT
    @since Juillet 2022
    """

    #    ___ _______________  _______  __  ____________
    #   / _ /_  __/_  __/ _ \/  _/ _ )/ / / /_  __/ __/
    #  / __ |/ /   / / / , _// // _  / /_/ / / / _\ \  
    # /_/ |_/_/   /_/ /_/|_/___/____/\____/ /_/ /___/  
    #                                                  





    #   _________  _  _______________  __  ___________________  _____  ____
    #  / ___/ __ \/ |/ / __/_  __/ _ \/ / / / ___/_  __/ __/ / / / _ \/ __/
    # / /__/ /_/ /    /\ \  / / / , _/ /_/ / /__  / / / _// /_/ / , _/\ \  
    # \___/\____/_/|_/___/ /_/ /_/|_|\____/\___/ /_/ /___/\____/_/|_/___/  
    #  





    #     ___  ____________________________  _____  ____
    #    / _ |/ ___/ ___/ __/ __/ __/ __/ / / / _ \/ __/
    #   / __ / /__/ /__/ _/_\ \_\ \/ _// /_/ / , _/\ \  
    #  /_/ |_\___/\___/___/___/___/___/\____/_/|_/___/  
    #





    #    __  _______________ ______  ___  ________
    #   /  |/  / __/_  __/ // / __ \/ _ \/ __/ __/
    #  / /|_/ / _/  / / / _  / /_/ / // / _/_\ \  
    # /_/  /_/___/ /_/ /_//_/\____/____/___/___/  
    #
    




    #    __  _______________ ______  ___  ________    _____________ ______________  __  __________
    #   /  |/  / __/_ __/  // / __ \/ _ \/ __/ __/   / __/_  __/ _ /_  __/  _/ __ \/ / / / __/ __/
    #  / /|_/ / _/  / / / _  / /_/ / // / _/_\ \    _\ \  / / / __ |/ / _/ // /_/ / /_/ / _/_\ \  
    # /_/  /_/___/ /_/ /_//_/\____/____/___/___/   /___/ /_/ /_/ |_/_/ /___/\___\_\____/___/___/  
    #

    @staticmethod
    def executeGetRequest(apiToken:str, endpoint:str):
        """!
        Cette méthode permet d'exécuter une requête de type GET
        @param apiToken : jeton API de l'utilisateur
        @param endpoint : adresse vers la ressource HTTP
        @return reponse http

        <hr>
        <strong>Exemple : </strong>
        <pre>
            jetonAPI = "abcdefgh-1234-ijkl-5678-mnopqrstuvwx"
            endpoint = "http://localhost:57170/api/method?param=1234"

            reponse = DataverseAPI.executeGetRequest(jetonAPI, endpoint)
        """
        header=dict([("Accept", "application/json"),
                     ("X-Dataverse-key", apiToken)])

        r = requests.request(method="GET", url=endpoint, headers=header)
        return r.json()

    @staticmethod
    def getDatasetContents(serverBaseURI:str, apiToken:str, doi:str):
        """! 
        Cette méthode permet de lister le contenu d'un dataset sous la forme d'une trame JSON
        @param serverBaseURI : adresse du dataverse
        @param apiToken : jeton API de l'utilisateur
        @param doi : DOI ciblé
        @return trame JSON détaillant le contenu du dataset
      
        <hr>
        <strong>Exemple : </strong>
        <pre>
            adresseServeur   = "http://localhost:57170"
            jetonAPI         = "abcdefgh-1234-ijkl-5678-mnopqrstuvwx"
            doi              = "doi:10.12345/ABCDEF"
               
            contenuDataset = DataverseAPI.getDatasetContents(adresseServeur, jetonAPI, doi)
        </pre>
        """
        #Je déclare et initialise mon endpoint
        endpoint = "/api/datasets/:persistentId/?persistentId={PERSISTENT_IDENTIFIER}"

        #Je met à jour le endpoint avec les paramètres de la requête url
        endpoint = endpoint.replace("{PERSISTENT_IDENTIFIER}", doi)
        endpoint = re.sub("\\/*$", "", serverBaseURI) + endpoint
        
        #J'execute la requête et retourne le résultat
        return DataverseAPI.executeGetRequest(apiToken, endpoint) 

    @staticmethod
    def getDatasetFiles(serverBaseURI:str, apiToken:str, datasetID:str, datasetVersion:str):
        """!
        Cette méthode permet de lister les fichiers d'un dataset sous la forme d'une trame JSON
        @param serverBaseURI : adresse du dataverse
        @param apiToken : jeton API de l'utilisateur
        @param datasetID : identifiant numérique du dataset
        @param datasetVersion : numéro de version du dataset
        @return trame JSON listant les fichiers du dataset
        
        <hr>
        <strong>Exemple : </strong>
        <pre>
            adresseServeur   = "http://localhost:57170"
            jetonAPI         = "abcdefgh-1234-ijkl-5678-mnopqrstuvwx"
            idDataset        = "123456"
            versionDataset   = "3.0"
            
            listeFichiers = DataverseAPI.getDatasetFiles(adresseServeur, jetonAPI, idDataset, versionDataset)
        </pre>
        """
        #Je déclare et initialise mon endpoint
        endpoint = "/api/datasets/{DATASET_ID}/versions/{DATASET_VERSION}/files"

        #Je met à jour le endpoint avec les paramètres de la requête url
        endpoint = endpoint.replace("{DATASET_ID}", datasetID)
        endpoint = endpoint.replace("{DATASET_VERSION}", datasetVersion)
        endpoint = re.sub("\\/*$", "",serverBaseURI) + endpoint
        
        #J'execute la requête et retourne le résultat
        return DataverseAPI.executeGetRequest(apiToken, endpoint)  

    @staticmethod
    def getDatasetMetadataExport(serverBaseURI:str, apiToken:str, doi:str, metadataFormat:str):
        """!
        Cette méthode permet d'exporter les métadonnées d'un dataset dans un format de métadonnées demandé en paramètre
        @param serverBaseURI : adresse du dataverse
        @param apiToken : jeton API de l'utilisateur
        @param doi : DOI ciblé
        @param metadataFormat : format des métadonnées
        @return trame JSON détailant les métadonnées dans le format demandé

        <hr>
        <strong>Exemple : </strong>
        <pre>
            adresseServeur     = "http://localhost:57170"
            jetonAPI           = "abcdefgh-1234-ijkl-5678-mnopqrstuvwx"
            doi                = "doi:10.12345/ABCDEF"
            formatMetadonnee   = "Datacite"
            
            metadonnees = DataverseAPI.getDatasetMetadataExport(adresseServeur, jetonAPI, doi, formatMetadonnee)
        </pre>
        """
        #Je déclare et initialise mon endpoint
        endpoint = "/api/datasets/export?exporter={METADATA_FORMAT}&persistentId={PERSISTENT_IDENTIFIER}"

        #Je met à jour le endpoint avec les paramètres de la requête url
        endpoint = endpoint.replace("{PERSISTENT_IDENTIFIER}", doi)
        endpoint = endpoint.replace("{METADATA_FORMAT}", metadataFormat)
        endpoint = re.sub("\\/*$", "", serverBaseURI) + endpoint
        
        #J'execute la requête et retourne le résultat
        return DataverseAPI.executeGetRequest(apiToken, endpoint)

    @staticmethod
    def getDatasetVersion(serverBaseURI:str, apiToken:str, datasetID:str, datasetVersion:str):
        """!
        Cette méthode permet de lister le contenu d'une version d'un dataset sous la forme d'une trame JSON
        @param serverBaseURI : adresse du dataverse
        @param apiToken : jeton API de l'utilisateur
        @param datasetID : identifiant numérique du dataset
        @param datasetVersion : numéro de version du dataset
        @return trame JSON détaillant le contenu d'une version du dataset
        
        <hr>
        <strong>Exemple : </strong>
        <pre>
            adresseServeur   = "http://localhost:57170"
            jetonAPI         = "abcdefgh-1234-ijkl-5678-mnopqrstuvwx"
            idDataset        = "123456"
            versionDataset   = "3.0"
            
            versionDataset = DataverseAPI.getDatasetVersion(adresseServeur, jetonAPI, idDataset, versionDataset)
        </pre>
        """
        #Je déclare et initialise mon endpoint
        endpoint = "/api/datasets/{DATASET_ID}/versions/{DATASET_VERSION}"

        #Je met à jour le endpoint avec les paramètres de la requête url
        endpoint = endpoint.replace("{DATASET_ID}", datasetID)
        endpoint = endpoint.replace("{DATASET_VERSION}", datasetVersion)
        endpoint = re.sub("\\/*$", "", serverBaseURI) + endpoint
        
        #J'execute la requête et retourne le résultat
        return DataverseAPI.executeGetRequest(apiToken, endpoint)       

    @staticmethod
    def getDatasetVersionList(serverBaseURI:str, apiToken:str, datasetID:str):
        """!
        Cette méthode permet de lister toutes les versions d'un dataset sous la forme d'une trame JSON
        @param serverBaseURI : adresse du dataverse
        @param apiToken : jeton API de l'utilisateur
        @param datasetID : identifiant numérique du dataset
        @return trame JSON détaillant le contenu d'une version du dataset
        
        <hr>
        <strong>Exemple : </strong>
        <pre>
            adresseServeur   = "http://localhost:57170"
            jetonAPI         = "abcdefgh-1234-ijkl-5678-mnopqrstuvwx"
            idDataset        = "123456"
            
            listeVersions = DataverseAPI.getDatasetVersionList(adresseServeur, jetonAPI, idDataset)
        </pre>
        """
        #Je déclare et initialise mon endpoint
        endpoint = "/api/datasets/{DATASET_ID}/versions"

        #Je met à jour le endpoint avec les paramètres de la requête url
        endpoint = endpoint.replace("{DATASET_ID}", datasetID)
        endpoint = re.sub("\\/*$", "", serverBaseURI) + endpoint
        
        #J'execute la requête et retourne le résultat
        return DataverseAPI.executeGetRequest(apiToken, endpoint)     

    @staticmethod
    def getDataverseContents(serverBaseURI:str, apiToken:str, dataverseID:str):
        """!
        Cette méthode permet de lister le contenu d'un dataverse sous la forme d'une trame JSON
        @param serverBaseURI : adresse du dataverse
        @param apiToken : jeton API de l'utilisateur
        @param dataverseID : identifiant du dataverse
        @return trame JSON détaillant le contenu du dataverse

        <hr>
        <strong>Exemple : </strong>
        <pre>
            adresseServeur   = "http://localhost:57170"
            jetonAPI         = "abcdefgh-1234-ijkl-5678-mnopqrstuvwx"
            idDataverse      = "INRAE Dataverse"

            contenuDataverse = DataverseAPI.getDataverseContents(adresseServeur, jetonAPI, idDataverse)
        </pre>
        """
        #Je déclare et initialise mon endpoint
        endpoint = "/api/dataverses/{DATAVERSE_ID}/contents"

        #Je met à jour le endpoint avec les paramètres de la requête url
        endpoint = endpoint.replace("{DATAVERSE_ID}", dataverseID)
        endpoint = re.sub("\\/*$", "", serverBaseURI) + endpoint
        
        #J'execute la requête et retourne le résultat
        return DataverseAPI.executeGetRequest(apiToken, endpoint)

    @staticmethod
    def getDataverseInfos(serverBaseURI:str, apiToken:str, dataverseID:str):
        """!
        Cette méthode permet de lister les infos concernant un dataverse sous la forme d'une trame JSON
        @param serverBaseURI : adresse du dataverse
        @param apiToken : jeton API de l'utilisateur
        @param dataverseID : identifiant du dataverse
        @return trame JSON détaillant le contenu du dataverse

        <hr>
        <strong>Exemple : </strong>
        <pre>
            adresseServeur   = "http://localhost:57170"
            jetonAPI         = "abcdefgh-1234-ijkl-5678-mnopqrstuvwx"
            idDataverse      = "INRAE Dataverse"

            infosDataverse = DataverseAPI.getDataverseInfos(adresseServeur, jetonAPI, idDataverse)
        </pre>
        """
        #Je déclare et initialise mon endpoint
        endpoint = "/api/dataverses/{DATAVERSE_ID}"

        #Je met à jour le endpoint avec les paramètres de la requête url
        endpoint = endpoint.replace("{DATAVERSE_ID}", dataverseID)
        endpoint = re.sub("\\/*$", "", serverBaseURI) + endpoint
        
        #J'execute la requête et retourne le résultat
        return DataverseAPI.executeGetRequest(apiToken, endpoint)

    @staticmethod
    def getDataverseMetadata(serverBaseURI:str, apiToken:str, dataverseID:str):
        """!
        Cette méthode permet de lister les métdonnées d'un dataverse sous la forme d'une trame JSON
        @param serverBaseURI : adresse du dataverse
        @param apiToken : jeton API de l'utilisateur
        @param dataverseID : identifiant du dataverse
        @return trame JSON listant les métadonnées d'un dataverse

        <hr>
        <strong>Exemple : </strong>
        <pre>
            adresseServeur   = "http://localhost:57170"
            jetonAPI         = "abcdefgh-1234-ijkl-5678-mnopqrstuvwx"
            idDataverse      = "INRAE Dataverse"

            infosDataverse = DataverseAPI.getDataverseMetadata(adresseServeur, jetonAPI, idDataverse)
        </pre>
        """
        #Je déclare et initialise mon endpoint
        endpoint = "/api/dataverses/{DATAVERSE_ID}/metadatablocks"

        #Je met à jour le endpoint avec les paramètres de la requête url
        endpoint = endpoint.replace("{DATAVERSE_ID}", dataverseID)
        endpoint = re.sub("\\/*$", "", serverBaseURI) + endpoint
        
        #J'execute la requête et retourne le résultat
        return DataverseAPI.executeGetRequest(apiToken, endpoint)

    @staticmethod
    def postDatasetAddFile(serverBaseURI:str, apiToken:str, doi:str, filePath:str):
        """!
        Cette méthode permet d'uploader un fichier vers un dataset
        @param serverBaseURI : adresse du dataverse
        @param apiToken : jeton API de l'utilisateur
        @param doi : DOI ciblé
        @param filePath : chemin vers le fichier à uploader
        @return trame JSON détaillant le succès/l'échec de l'upload de fichier

        <hr>
        <strong>Exemple : </strong>
        <pre>
            adresseServeur   = "http://localhost57170"
            jetonAPI         = "abcdefgh-1234-ijkl-5678-mnopqrstuvwx"
            doi              = "doi:10.12345/ABCDEF"
            cheminFichier    = "c:\\temp\\fichier.csv"

            status = DataverseAPI.postDatasetAddFile(adresseServeur, jetonAPI, doi, cheminFichier)
        <pre>
        """

        #Je déclare et initialise mon endpoint
        endpoint = "/api/datasets/:persistentId/add?persistentId={PERSISTENT_IDENTIFIER}";

        #Je met à jour le endpoint avec les paramètres de la requête url
        endpoint = endpoint.replace("{PERSISTENT_IDENTIFIER}", doi);
        endpoint = re.sub("\\/*$", "",serverBaseURI) + endpoint;
        
        #Je prépare la requête et retourne le résultat
        with open(filePath, "r") as file:
            lines = file.readlines()
            fileContent = "".join(lines)

        files = {'file': (filePath, fileContent)}

        #J'execute la requête et retourne le résultat
        header=dict([("Accept", "application/json"),
                     ("X-Dataverse-key", apiToken)])
        r = requests.request(method="POST", url=endpoint, headers=header, files=files)
        return r.json()      

    @staticmethod
    def postDatasetCreate(serverBaseURI:str, apiToken:str, dataverseID:str, metadataJSON:str):
        """!
        Cette méthode permet de créer un dataset
        @param serverBaseURI : adresse du dataverse
        @param apiToken : jeton API de l'utilisateur
        @param dataverseID : identifiant du dataverse
        @param metadataJSON : metadata du dataset
        @return trame JSON détaillant le succès/l'échec de la création du dataset

        <hr>
        <strong>Exemple : </strong>
        <pre>
            adresseServeur = "http://localhost57170"
            jetonAPI = "abcdefgh-1234-ijkl-5678-mnopqrstuvwx"
            idDataverse = "INRAE Dataverse"

            #Créer une chaîne de caractères à partir d'un fichier json
            with open("c:\\temp\\metadata.json", "r") as file:
                lines = file.readlines()
                metadata = "".join(lines)

            status = DataverseAPI.postDatasetCreate(adresseServeur, jetonAPI, idDataverse, metadata)
        <pre>
        """
        #Je déclare et initialise mon endpoint
        endpoint = "/api/dataverses/{DATAVERSE_ID}/datasets";

        #Je met à jour le endpoint avec les paramètres de la requête url
        endpoint = endpoint.replace("{DATAVERSE_ID}", dataverseID);
        endpoint = re.sub("\\/*$", "",serverBaseURI) + endpoint;
        
        #J'execute la requête et retourne le résultat
        header=dict([("Accept", "application/json"),
                     ("X-Dataverse-key", apiToken)])
        
        r = requests.request(method="POST", url=endpoint, data=metadataJSON, headers=header)
        return r.json()    
    
    @staticmethod
    def postDatasetPublish(serverBaseURI:str, apiToken:str, doi:str, versionType:str):
        """!
        Cette méthode permet de publier un dataset
        @param serverBaseURI : adresse du dataverse
        @param apiToken : jeton API de l'utilisateur
        @param doi : DOI ciblé
        @param versionType : le type de version {minor | major}
        @return trame JSON détailant le succès/l'échec de la publication du dataset

        <hr>
        <strong>Exemple : </strong>
        <pre>
            adresseServeur = "http://localhost57170"
            jetonAPI = "abcdefgh-1234-ijkl-5678-mnopqrstuvwx"
            doi = "doi:10.12345/ABCDEF"
            typeVersion = "minor"

            status = DataverseAPI.postDatasetPublish(adresseServeur, jetonAPI, doi, typeVersion)
        </pre>
        """
        #Je déclare et initialise mon endpoint
        endpoint = "/api/datasets/:persistentId/actions/:publish?persistentId={PERSISTENT_IDENTIFIER}&type={VERSION_TYPE}";

        #Je met à jour le endpoint avec les paramètres de la requête url
        endpoint = endpoint.replace("{PERSISTENT_IDENTIFIER}", doi);
        endpoint = endpoint.replace("{VERSION_TYPE}", versionType);
        endpoint = re.sub("\\/*$", "", serverBaseURI) + endpoint;
        
        #J'execute la requête et retourne le résultat
        header=dict([("Accept", "application/json"),
                     ("X-Dataverse-key", apiToken)])
        r = requests.request(method="POST", url=endpoint, headers=header)
        return r.json()    