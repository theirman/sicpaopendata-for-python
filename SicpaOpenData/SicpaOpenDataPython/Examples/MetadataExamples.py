#! /usr/bin/env python3
# coding: utf-8

class MetadataExamples:
    """!
    Classe montrant comment utiliser les classes de <c>SicpaOpenData.Metadata</c>
    @author Tom VINCENT
    @since Juillet 2022
    """
    def HowToBuildABiomedicalNode():
        """!
        <c>HowToBuildABiomedicalNode</c> montre la manière de construire un <c>BiomedicalNode</c>
        
        <hr>
        <strong>Exemple de code : </strong>
        <pre>
            bn.addField(MultipleControlledVocabularyNode("studyDesignType", ["Case Control", "Cross Sectional"]))
            bn.addField(MultiplePrimitiveNode("studyDesignTypeOther", ["studyDesignTypeOther1", "studyDesignTypeOther2"]))
            bn.addField(MultipleControlledVocabularyNode("studyFactorType", ["Age", "Biomarkers"]))
            bn.addField(MultiplePrimitiveNode("studyFactorTypeOther", ["studyFactorTypeOther1", "studyFactorTypeOther2"]))
            bn.addField(MultipleControlledVocabularyNode("studyAssayOrganism", ["Arabidopsis thaliana", "Bos taurus"]))
            bn.addField(MultiplePrimitiveNode("studyAssayOtherOrganism", ["studyAssayOtherOrganism1", "studyAssayOtherOrganism2"]))
            bn.addField(MultipleControlledVocabularyNode("studyAssayMeasurementType", ["clinical chemistry analysis", "cell sorting"]))
            bn.addField(MultiplePrimitiveNode("studyAssayOtherMeasurmentType", ["studyAssayOtherMeasurmentType1", "studyAssayOtherMeasurmentType2"]))
            bn.addField(MultipleControlledVocabularyNode("studyAssayTechnologyType", ["culture based drug susceptibility testing, single concentration", "culture based drug susceptibility testing, two concentrations"]))
            bn.addField(MultiplePrimitiveNode("studyAssayTechnologyTypeOther", ["studyAssayTechnologyTypeOther1", "studyAssayTechnologyTypeOther2"]))
            bn.addField(MultipleControlledVocabularyNode("studyAssayPlatform", ["210-MS GC Ion Trap (Varian)", "220-MS GC Ion Trap (Varian)"]))
            bn.addField(MultiplePrimitiveNode("studyAssayPlatformOther", ["studyAssayPlatformOther1", "studyAssayPlatformOther2"]))
            bn.addField(MultiplePrimitiveNode("studyAssayCellType", ["studyAssayCellType1", "studyAssayCellType2"]))
            bn.addField(MultiplePrimitiveNode("studySampleType", ["studySampleType1", "studySampleType2"]))
            bn.addField(MultiplePrimitiveNode("studyProtocolType", ["studyProtocolType1", "studyProtocolType2"]))

            print(bn.toJSON())
        </pre>
        """

        print('bn.addField(MultipleControlledVocabularyNode("studyDesignType", ["Case Control", "Cross Sectional"]))')
        print('bn.addField(MultiplePrimitiveNode("studyDesignTypeOther", ["studyDesignTypeOther1", "studyDesignTypeOther2"]))')
        print('bn.addField(MultipleControlledVocabularyNode("studyFactorType", ["Age", "Biomarkers"]))')
        print('bn.addField(MultiplePrimitiveNode("studyFactorTypeOther", ["studyFactorTypeOther1", "studyFactorTypeOther2"]))')
        print('bn.addField(MultipleControlledVocabularyNode("studyAssayOrganism", ["Arabidopsis thaliana", "Bos taurus"]))')
        print('bn.addField(MultiplePrimitiveNode("studyAssayOtherOrganism", ["studyAssayOtherOrganism1", "studyAssayOtherOrganism2"]))')
        print('bn.addField(MultipleControlledVocabularyNode("studyAssayMeasurementType", ["clinical chemistry analysis", "cell sorting"]))')
        print('bn.addField(MultiplePrimitiveNode("studyAssayOtherMeasurmentType", ["studyAssayOtherMeasurmentType1", "studyAssayOtherMeasurmentType2"]))')
        print('bn.addField(MultipleControlledVocabularyNode("studyAssayTechnologyType", ["culture based drug susceptibility testing, single concentration", "culture based drug susceptibility testing, two concentrations"]))')
        print('bn.addField(MultiplePrimitiveNode("studyAssayTechnologyTypeOther", ["studyAssayTechnologyTypeOther1", "studyAssayTechnologyTypeOther2"]))')
        print('bn.addField(MultipleControlledVocabularyNode("studyAssayPlatform", ["210-MS GC Ion Trap (Varian)", "220-MS GC Ion Trap (Varian)"]))')
        print('bn.addField(MultiplePrimitiveNode("studyAssayPlatformOther", ["studyAssayPlatformOther1", "studyAssayPlatformOther2"]))')
        print('bn.addField(MultiplePrimitiveNode("studyAssayCellType", ["studyAssayCellType1", "studyAssayCellType2"]))')
        print('bn.addField(MultiplePrimitiveNode("studySampleType", ["studySampleType1", "studySampleType2"]))')
        print('bn.addField(MultiplePrimitiveNode("studyProtocolType", ["studyProtocolType1", "studyProtocolType2"]))')
        print("")
        print('print(bn.toJSON())')

    
    def HowToBuildACitationNode():
        """!
        <c>HowToBuildACitationNode</c> montre la manière de construire un <c>CitationNode</c> complet

        <hr>
        <strong>Exemple de code : </strong>
        <pre>
            # Je crée les auteurs du dataset
            htAuteur1 = dict()
            htAuteur1["authorAffiliation"] = SimplePrimitiveNode("authorAffiliation", "authorAffiliation1")
            htAuteur1["authorIdentifier"] = SimplePrimitiveNode("authorIdentifier", "authorIdentifier1")
            htAuteur1["authorIdentifierScheme"] = SimpleControlledVocabularyNode("authorIdentifierScheme", "ORCID")
            htAuteur1["authorName"] = SimplePrimitiveNode("authorName", "authorName1")

            htAuteur2 = dict()
            htAuteur2["authorAffiliation"] = SimplePrimitiveNode("authorAffiliation", "authorAffiliation2")
            htAuteur2["authorIdentifier"] = SimplePrimitiveNode("authorIdentifier", "authorIdentifier2")
            htAuteur2["authorIdentifierScheme"] = SimpleControlledVocabularyNode("authorIdentifierScheme", "idHAL")
            htAuteur2["authorName"] = SimplePrimitiveNode("authorName", "authorName2")

            # Je crée les autres identifiants
            htAutreId1 = dict()
            htAutreId1["otherIdAgency"] = SimplePrimitiveNode("otherIdAgency", "otherIdAgency1")
            htAutreId1["otherIdValue"] = SimplePrimitiveNode("otherIdValue", "otherIdAgencyIdentifier1")

            htAutreId2 = dict()
            htAutreId2["otherIdAgency"] = SimplePrimitiveNode("otherIdAgency", "otherIdAgency2")
            htAutreId2["otherIdValue"] = SimplePrimitiveNode("otherIdValue", "otherIdAgencyIdentifier2")

            # Je crée les contacts du dataset
            htContact1 = dict()
            htContact1["datasetContactAffiliation"] = SimplePrimitiveNode("datasetContactAffiliation", "contactAffiliation1")
            htContact1["datasetContactEmail"] = SimplePrimitiveNode("datasetContactEmail", "contact1@mail.com")
            htContact1["datasetContactName"] = SimplePrimitiveNode("datasetContactName", "contactName1")

            htContact2 = dict()
            htContact2["datasetContactAffiliation"] = SimplePrimitiveNode("datasetContactAffiliation", "contactAffiliation2")
            htContact2["datasetContactEmail"] = SimplePrimitiveNode("datasetContactEmail", "contact2@mail.com")
            htContact2["datasetContactName"] = SimplePrimitiveNode("datasetContactName", "contactName2")

            # Je crée les contributeurs du dataset
            htContributeur1 = dict()
            htContributeur1["contributorAffiliation"] = SimplePrimitiveNode("contributorAffiliation", "contributorAffiliation1")
            htContributeur1["contributorIdentifier"] = SimplePrimitiveNode("contributorIdentifier", "contributorIdentifier1")
            htContributeur1["contributorIdentifierScheme"] = SimpleControlledVocabularyNode("contributorIdentifierScheme", "ORCID")
            htContributeur1["contributorName"] = SimplePrimitiveNode("contributorName", "contributorName1")
            htContributeur1["contributorType"] = SimpleControlledVocabularyNode("contributorType", "Data Collector")

            htContributeur2 = dict()
            htContributeur2["contributorAffiliation"] = SimplePrimitiveNode("contributorAffiliation", "contributorAffiliation2")
            htContributeur2["contributorIdentifier"] = SimplePrimitiveNode("contributorIdentifier", "contributorIdentifier2")
            htContributeur2["contributorIdentifierScheme"] = SimpleControlledVocabularyNode("contributorIdentifierScheme", "ORCID")
            htContributeur2["contributorName"] = SimplePrimitiveNode("contributorName", "contributorName2")
            htContributeur2["contributorType"] = SimpleControlledVocabularyNode("contributorType", "Editor")

            # Je crée les dates des collections du dataset
            htDates1 = dict()
            htDates1["dateOfCollectionStart"] = SimplePrimitiveNode("dateOfCollectionStart", "2000-01-01")
            htDates1["dateOfCollectionEnd"] = SimplePrimitiveNode("dateOfCollectionEnd", "2000-01-02")

            htDates2 = dict()
            htDates2["dateOfCollectionStart"] = SimplePrimitiveNode("dateOfCollectionStart", "2000-01-03")
            htDates2["dateOfCollectionEnd"] = SimplePrimitiveNode("dateOfCollectionEnd", "2000-01-04")

            # Je crée les descriptions du dataset
            htDescription1 = dict()
            htDescription1["dsDescriptionDate"] = SimplePrimitiveNode("dsDescriptionDate", "2000-01-01")
            htDescription1["dsDescriptionValue"] = SimplePrimitiveNode("dsDescriptionValue", "descriptionText1")

            htDescription2 = dict()
            htDescription2["dsDescriptionDate"] = SimplePrimitiveNode("dsDescriptionDate", "2000-01-01")
            htDescription2["dsDescriptionValue"] = SimplePrimitiveNode("dsDescriptionValue", "descriptionText2")

            # Je crée les distributeurs du dataset
            htDistributeur1 = dict()
            htDistributeur1["distributorAbbreviation"] = SimplePrimitiveNode("distributorAbbreviation", "distributorAbbreviation1")
            htDistributeur1["distributorAffiliation"] = SimplePrimitiveNode("distributorAffiliation", "distributorAffiliation1")
            htDistributeur1["distributorLogoURL"] = SimplePrimitiveNode("distributorLogoURL", "http://distributorLogoURL1")
            htDistributeur1["distributorName"] = SimplePrimitiveNode("distributorName", "distributorName1")
            htDistributeur1["distributorURL"] = SimplePrimitiveNode("distributorURL", "http://distributorURL1")

            htDistributeur2 = dict()
            htDistributeur2["distributorAbbreviation"] = SimplePrimitiveNode("distributorAbbreviation", "distributorAbbreviation2")
            htDistributeur2["distributorAffiliation"] = SimplePrimitiveNode("distributorAffiliation", "distributorAffiliation2")
            htDistributeur2["distributorLogoURL"] = SimplePrimitiveNode("distributorLogoURL", "http://distributorLogoURL2")
            htDistributeur2["distributorName"] = SimplePrimitiveNode("distributorName", "distributorName2")
            htDistributeur2["distributorURL"] = SimplePrimitiveNode("distributorURL", "http://distributorURL2")

            # Je crée les grant number du dataset
            htGrantNumber1 = dict()
            htGrantNumber1["grantNumberAgency"] = SimplePrimitiveNode("grantNumberAgency", "grantAgency1")
            htGrantNumber1["grantNumberValue"] = SimplePrimitiveNode("grantNumberValue", "grantNumber1")

            htGrantNumber2 = dict()
            htGrantNumber2["grantNumberAgency"] = SimplePrimitiveNode("grantNumberAgency", "grantAgency2")
            htGrantNumber2["grantNumberValue"] = SimplePrimitiveNode("grantNumberValue", "grantNumber2")

            # Je crée les logiciels du dataset
            htLogiciel1 = dict()
            htLogiciel1["softwareName"] = SimplePrimitiveNode("softwareName", "softwareName1")
            htLogiciel1["softwareVersion"] = SimplePrimitiveNode("softwareVersion", "softwareVersion1")

            htLogiciel2 = dict()
            htLogiciel2["softwareName"] = SimplePrimitiveNode("softwareName", "softwareName2")
            htLogiciel2["softwareVersion"] = SimplePrimitiveNode("softwareVersion", "softwareVersion2")

            # Je crée les mots clés du dataset
            htMotCle1 = dict()
            htMotCle1["keywordTermURI"] = SimplePrimitiveNode("keywordTermURI", "http://keywordTermURI1")
            htMotCle1["keywordValue"] = SimplePrimitiveNode("keywordValue", "keywordValue1")
            htMotCle1["keywordVocabulary"] = SimplePrimitiveNode("keywordVocabulary", "keywordVocabulary1")
            htMotCle1["keywordVocabularyURI"] = SimplePrimitiveNode("keywordVocabularyURI", "http://keywordVocabularyURI1")

            htMotCle2 = dict()
            htMotCle2["keywordTermURI"] = SimplePrimitiveNode("keywordTermURI", "http://keywordTermURI2")
            htMotCle2["keywordValue"] = SimplePrimitiveNode("keywordValue", "keywordValue2")
            htMotCle2["keywordVocabulary"] = SimplePrimitiveNode("keywordVocabulary", "keywordVocabulary2")
            htMotCle2["keywordVocabularyURI"] = SimplePrimitiveNode("keywordVocabularyURI", "http://keywordVocabularyURI2")

            # Je crée les producteurs du dataset
            htProducteur1 = dict()
            htProducteur1["producerAbbreviation"] = SimplePrimitiveNode("producerAbbreviation", "producerAbbreviation1")
            htProducteur1["producerAffiliation"] = SimplePrimitiveNode("producerAffiliation", "producerAffiliation1")
            htProducteur1["producerLogoURL"] = SimplePrimitiveNode("producerLogoURL", "http://producerLogoURL1")
            htProducteur1["producerName"] = SimplePrimitiveNode("producerName", "producerName1")
            htProducteur1["producerURL"] = SimplePrimitiveNode("producerURL", "http://producerURL1")

            htProducteur2 = dict()
            htProducteur2["producerAbbreviation"] = SimplePrimitiveNode("producerAbbreviation", "producerAbbreviation2")
            htProducteur2["producerAffiliation"] = SimplePrimitiveNode("producerAffiliation", "producerAffiliation2")
            htProducteur2["producerLogoURL"] = SimplePrimitiveNode("producerLogoURL", "http://producerLogoURL2")
            htProducteur2["producerName"] = SimplePrimitiveNode("producerName", "producerName2")
            htProducteur2["producerURL"] = SimplePrimitiveNode("producerURL", "http://producerURL2")

            # Je crée la période du dataset
            htPeriode1 = dict()
            htPeriode1["timePeriodCoveredStart"] = SimplePrimitiveNode("timePeriodCoveredStart", "2000-01-01")
            htPeriode1["timePeriodCoveredEnd"] = SimplePrimitiveNode("timePeriodCoveredEnd", "2000-01-02")

            htPeriode2 = dict()
            htPeriode2["timePeriodCoveredStart"] = SimplePrimitiveNode("timePeriodCoveredStart", "2000-01-03")
            htPeriode2["timePeriodCoveredEnd"] = SimplePrimitiveNode("timePeriodCoveredEnd", "2000-01-04")

            # Je crée le projet du dataset
            htProjet = dict()
            htProjet["projectAcronym"] = SimplePrimitiveNode("projectAcronym", "projectAcronym")
            htProjet["projectId"] = SimplePrimitiveNode("projectId", "projectId")
            htProjet["projectTask"] = SimplePrimitiveNode("projectTask", "projectTask")
            htProjet["projectTitle"] = SimplePrimitiveNode("projectTitle", "projectTitle")
            htProjet["projectURL"] = SimplePrimitiveNode("projectURL", "http://project.url")
            htProjet["projectWorkPackage"] = SimplePrimitiveNode("projectWorkPackage", "projectWP")

            # Je crée les producteurs du dataset
            htPublication1 = dict()
            htPublication1["publicationCitation"] = SimplePrimitiveNode("publicationCitation", "relatedPublicationCitation1")
            htPublication1["publicationIDNumber"] = SimplePrimitiveNode("publicationIDNumber", "relatedPublicationIDNumber1")
            htPublication1["publicationIDType"] = SimpleControlledVocabularyNode("publicationIDType", "ark")
            htPublication1["publicationURL"] = SimplePrimitiveNode("publicationURL", "http://related.publication.url1")

            htPublication2 = dict()
            htPublication2["publicationCitation"] = SimplePrimitiveNode("publicationCitation", "relatedPublicationCitation2")
            htPublication2["publicationIDNumber"] = SimplePrimitiveNode("publicationIDNumber", "relatedPublicationIDNumber2")
            htPublication2["publicationIDType"] = SimpleControlledVocabularyNode("publicationIDType", "arXiv")
            htPublication2["publicationURL"] = SimplePrimitiveNode("publicationURL", "http://related.publication.url2")

            # Je crée les relatedDataset du dataset
            htRelatedDataset1 = dict()
            htRelatedDataset1["relatedDatasetCitation"] = SimplePrimitiveNode("relatedDatasetCitation", "relatedDatasetCitation1")
            htRelatedDataset1["relatedDatasetIDNumber"] = SimplePrimitiveNode("relatedDatasetIDNumber", "relatedDatasetIDNumber1")
            htRelatedDataset1["relatedDatasetIDType"] = SimpleControlledVocabularyNode("relatedDatasetIDType", "ark")
            htRelatedDataset1["relatedDatasetURL"] = SimplePrimitiveNode("relatedDatasetURL", "http://related.dataset.url1")

            htRelatedDataset2 = dict()
            htRelatedDataset2["relatedDatasetCitation"] = SimplePrimitiveNode("relatedDatasetCitation", "relatedDatasetCitation2")
            htRelatedDataset2["relatedDatasetIDNumber"] = SimplePrimitiveNode("relatedDatasetIDNumber", "relatedDatasetIDNumber2")
            htRelatedDataset2["relatedDatasetIDType"] = SimpleControlledVocabularyNode("relatedDatasetIDType", "doi")
            htRelatedDataset2["relatedDatasetURL"] = SimplePrimitiveNode("relatedDatasetURL", "http://related.dataset.url2")

            # Je crée la série du dataset
            htSerie = dict()
            htSerie["seriesInformation"] = SimplePrimitiveNode("seriesInformation", "seriesInformation")
            htSerie["seriesName"] = SimplePrimitiveNode("seriesName", "seriesName")

            # Je crée les topics classifications du dataset
            htTopicClassification1 = dict()
            htTopicClassification1["topicClassValue"] = SimplePrimitiveNode("topicClassValue", "topicClassValue1")
            htTopicClassification1["topicClassVocab"] = SimplePrimitiveNode("topicClassVocab", "topicClassVocab1")
            htTopicClassification1["topicClassVocabURI"] = SimplePrimitiveNode("topicClassVocabURI", "http://topicClassVocabURI1")

            htTopicClassification2 = dict()
            htTopicClassification2["topicClassValue"] = SimplePrimitiveNode("topicClassValue", "topicClassValue2")
            htTopicClassification2["topicClassVocab"] = SimplePrimitiveNode("topicClassVocab", "topicClassVocab2")
            htTopicClassification2["topicClassVocabURI"] = SimplePrimitiveNode("topicClassVocabURI", "http://topicClassVocabURI2")

            # Je crée un node citation
            cn = CitationNode();
            cn.addField(SimplePrimitiveNode("title", "title"))
            cn.addField(SimplePrimitiveNode("subtitle", "subtitle"))
            cn.addField(SimplePrimitiveNode("alternativeTitle", "alternativeTitle"))
            cn.addField(SimplePrimitiveNode("alternativeURL", "http://link.to.data"))
            cn.addField(MultipleCompoundNode("otherId", [htAutreId1, htAutreId2]))
            cn.addField(MultipleCompoundNode("datasetContact", [htContact1, htContact2]))
            cn.addField(MultipleCompoundNode("author", [htAuteur1, htAuteur2]))
            cn.addField(MultipleCompoundNode("contributor", [htContributeur1, htContributeur2]))
            cn.addField(MultipleCompoundNode("producer", [htProducteur1, htProducteur2]))
            cn.addField(SimplePrimitiveNode("productionDate", "2000-01-01"))
            cn.addField(SimplePrimitiveNode("productionPlace", "productionPlace"))
            cn.addField(MultipleCompoundNode("distributor", [htDistributeur1, htDistributeur2]))
            cn.addField(SimplePrimitiveNode("distributionDate", "2000-01-01"))
            cn.addField(MultipleCompoundNode("dsDescription", [htDescription1, htDescription2]))
            cn.addField(MultipleControlledVocabularyNode("language", ["Abkhaz", "Afar"]))
            cn.addField(MultipleControlledVocabularyNode("subject", ["Agricultural Sciences", "Arts and Humanities"]))
            cn.addField(MultipleCompoundNode("keyword", [htMotCle1, htMotCle1]))
            cn.addField(MultipleCompoundNode("topicClassification", [htTopicClassification1, htTopicClassification2]))
            cn.addField(MultipleControlledVocabularyNode("kindOfData", ["Audiovisual", "Collection"]))
            cn.addField(MultiplePrimitiveNode("kindOfDataOther", ["otherKindOfData1", "otherKindOfData2"]))
            cn.addField(MultipleControlledVocabularyNode("dataOrigin", ["observational data", "experimental data"]))
            cn.addField(MultiplePrimitiveNode("dataSources", ["dataSource1", "dataSource2"]))
            cn.addField(SimplePrimitiveNode("originOfSources", "originOfSource"))
            cn.addField(SimplePrimitiveNode("characteristicOfSources", "characteristicOfSource"))
            cn.addField(SimplePrimitiveNode("accessToSources", "accessToSource"))
            cn.addField(MultipleCompoundNode("software", [htLogiciel1, htLogiciel2]))
            cn.addField(SimpleCompoundNode("series", htSerie))
            cn.addField(MultipleControlledVocabularyNode("lifeCycleStep", ["Study proposal", "Funding"]))
            cn.addField(SimplePrimitiveNode("notesText", "notes"))
            cn.addField(MultipleCompoundNode("publication", [htPublication1, htPublication2]))
            cn.addField(MultiplePrimitiveNode("relatedMaterial", ["relatedMaterial1", "relatedMaterial2"]))
            cn.addField(MultipleCompoundNode("relatedDataset", [htRelatedDataset1, htRelatedDataset2]))
            cn.addField(MultiplePrimitiveNode("otherReferences", ["otherReference1", "otherReference2"]))
            cn.addField(MultipleCompoundNode("grantNumber", [htGrantNumber1, htGrantNumber2]))
            cn.addField(SimpleCompoundNode("project", htProjet))
            cn.addField(MultipleCompoundNode("timePeriodCovered", [htPeriode1, htPeriode2]))
            cn.addField(MultipleCompoundNode("dateOfCollection", [htDates1, htDates2]))
            cn.addField(SimplePrimitiveNode("depositor", "depositor"))
            cn.addField(SimplePrimitiveNode("dateOfDeposit", "2000-01-01"))

            print(cn.toJSON)
        </pre>
        """
        # Je crée les auteurs du dataset
        print('Auteur1 = dict()')
        print('Auteur1["authorAffiliation"] = SimplePrimitiveNode("authorAffiliation", "authorAffiliation1")')
        print('Auteur1["authorIdentifier"] = SimplePrimitiveNode("authorIdentifier", "authorIdentifier1")')
        print('Auteur1["authorIdentifierScheme"] = SimpleControlledVocabularyNode("authorIdentifierScheme", "ORCID")')
        print('Auteur1["authorName"] = SimplePrimitiveNode("authorName", "authorName1")')
        print("")
        print('Auteur2 = dict()')
        print('Auteur2["authorAffiliation"] = SimplePrimitiveNode("authorAffiliation", "authorAffiliation2")')
        print('Auteur2["authorIdentifier"] = SimplePrimitiveNode("authorIdentifier", "authorIdentifier2")')
        print('Auteur2["authorIdentifierScheme"] = SimpleControlledVocabularyNode("authorIdentifierScheme", "idHAL")')
        print('Auteur2["authorName"] = SimplePrimitiveNode("authorName", "authorName2")')
        print("")
        print("# Je crée les autres identifiants")
        print('AutreId1 = dict()')
        print('AutreId1["otherIdAgency"] = SimplePrimitiveNode("otherIdAgency", "otherIdAgency1")')
        print('AutreId1["otherIdValue"] = SimplePrimitiveNode("otherIdValue", "otherIdAgencyIdentifier1")')
        print("")
        print('AutreId2 = dict()')
        print('AutreId2["otherIdAgency"] = SimplePrimitiveNode("otherIdAgency", "otherIdAgency2")')
        print('AutreId2["otherIdValue"] = SimplePrimitiveNode("otherIdValue", "otherIdAgencyIdentifier2")')
        print("")
        print("Je crée les contacts du dataset")
        print('Contact1 = dict()')
        print('Contact1["datasetContactAffiliation"] = SimplePrimitiveNode("datasetContactAffiliation", "contactAffiliation1")')
        print('Contact1["datasetContactEmail"] = SimplePrimitiveNode("datasetContactEmail", "contact1@mail.com")')
        print('Contact1["datasetContactName"] = SimplePrimitiveNode("datasetContactName", "contactName1")')
        print("")
        print('Contact2 = dict()')
        print('Contact2["datasetContactAffiliation"] = SimplePrimitiveNode("datasetContactAffiliation", "contactAffiliation2")')
        print('Contact2["datasetContactEmail"] = SimplePrimitiveNode("datasetContactEmail", "contact2@mail.com")')
        print('Contact2["datasetContactName"] = SimplePrimitiveNode("datasetContactName", "contactName2")')
        print("")
        print("Je crée les contributeurs du dataset")
        print('Contributeur1 = dict()')
        print('Contributeur1["contributorAffiliation"] = SimplePrimitiveNode("contributorAffiliation", "contributorAffiliation1")')
        print('Contributeur1["contributorIdentifier"] = SimplePrimitiveNode("contributorIdentifier", "contributorIdentifier1")')
        print('Contributeur1["contributorIdentifierScheme"] = SimpleControlledVocabularyNode("contributorIdentifierScheme", "ORCID")')
        print('Contributeur1["contributorName"] = SimplePrimitiveNode("contributorName", "contributorName1")')
        print('Contributeur1["contributorType"] = SimpleControlledVocabularyNode("contributorType", "Data Collector")')
        print("")
        print('Contributeur2 = dict()')
        print('Contributeur2["contributorAffiliation"] = SimplePrimitiveNode("contributorAffiliation", "contributorAffiliation2")')
        print('Contributeur2["contributorIdentifier"] = SimplePrimitiveNode("contributorIdentifier", "contributorIdentifier2")')
        print('Contributeur2["contributorIdentifierScheme"] = SimpleControlledVocabularyNode("contributorIdentifierScheme", "ORCID")')
        print('Contributeur2["contributorName"] = SimplePrimitiveNode("contributorName", "contributorName2")')
        print('Contributeur2["contributorType"] = SimpleControlledVocabularyNode("contributorType", "Editor")')
        print("")
        print("Je crée les dates des collections du dataset")
        print('Dates1 = dict()')
        print('Dates1["dateOfCollectionStart"] = SimplePrimitiveNode("dateOfCollectionStart", "2000-01-01")')
        print('Dates1["dateOfCollectionEnd"] = SimplePrimitiveNode("dateOfCollectionEnd", "2000-01-02")')
        print("")
        print('Dates2 = dict()')
        print('Dates2["dateOfCollectionStart"] = SimplePrimitiveNode("dateOfCollectionStart", "2000-01-03")')
        print('Dates2["dateOfCollectionEnd"] = SimplePrimitiveNode("dateOfCollectionEnd", "2000-01-04")')
        print("")
        print("Je crée les descriptions du dataset")
        print('Description1 = dict()')
        print('Description1["dsDescriptionDate"] = SimplePrimitiveNode("dsDescriptionDate", "2000-01-01")')
        print('Description1["dsDescriptionValue"] = SimplePrimitiveNode("dsDescriptionValue", "descriptionText1")')
        print("")
        print('Description2 = dict()')
        print('Description2["dsDescriptionDate"] = SimplePrimitiveNode("dsDescriptionDate", "2000-01-01")')
        print('Description2["dsDescriptionValue"] = SimplePrimitiveNode("dsDescriptionValue", "descriptionText2")')
        print("")
        print("Je crée les distributeurs du dataset")
        print('Distributeur1 = dict()')
        print('Distributeur1["distributorAbbreviation"] = SimplePrimitiveNode("distributorAbbreviation", "distributorAbbreviation1")')
        print('Distributeur1["distributorAffiliation"] = SimplePrimitiveNode("distributorAffiliation", "distributorAffiliation1")')
        print('Distributeur1["distributorLogoURL"] = SimplePrimitiveNode("distributorLogoURL", "http://distributorLogoURL1")')
        print('Distributeur1["distributorName"] = SimplePrimitiveNode("distributorName", "distributorName1")')
        print('Distributeur1["distributorURL"] = SimplePrimitiveNode("distributorURL", "http://distributorURL1")')
        print("")
        print('Distributeur2 = dict()')
        print('Distributeur2["distributorAbbreviation"] = SimplePrimitiveNode("distributorAbbreviation", "distributorAbbreviation2")')
        print('Distributeur2["distributorAffiliation"] = SimplePrimitiveNode("distributorAffiliation", "distributorAffiliation2")')
        print('Distributeur2["distributorLogoURL"] = SimplePrimitiveNode("distributorLogoURL", "http://distributorLogoURL2")')
        print('Distributeur2["distributorName"] = SimplePrimitiveNode("distributorName", "distributorName2")')
        print('Distributeur2["distributorURL"] = SimplePrimitiveNode("distributorURL", "http://distributorURL2")')
        print("")
        print("Je crée les grant number du dataset")
        print('GrantNumber1 = dict()')
        print('GrantNumber1["grantNumberAgency"] = SimplePrimitiveNode("grantNumberAgency", "grantAgency1")')
        print('GrantNumber1["grantNumberValue"] = SimplePrimitiveNode("grantNumberValue", "grantNumber1")')
        print("")
        print('GrantNumber2 = dict()')
        print('GrantNumber2["grantNumberAgency"] = SimplePrimitiveNode("grantNumberAgency", "grantAgency2")')
        print('GrantNumber2["grantNumberValue"] = SimplePrimitiveNode("grantNumberValue", "grantNumber2")')
        print("")
        print("Je crée les logiciels du dataset")
        print('Logiciel1 = dict()')
        print('Logiciel1["softwareName"] = SimplePrimitiveNode("softwareName", "softwareName1")')
        print('Logiciel1["softwareVersion"] = SimplePrimitiveNode("softwareVersion", "softwareVersion1")')
        print("")
        print('Logiciel2 = dict()')
        print('Logiciel2["softwareName"] = SimplePrimitiveNode("softwareName", "softwareName2")')
        print('Logiciel2["softwareVersion"] = SimplePrimitiveNode("softwareVersion", "softwareVersion2")')
        print("")
        print("Je crée les mots clés du dataset")
        print('MotCle1 = dict()')
        print('MotCle1["keywordTermURI"] = SimplePrimitiveNode("keywordTermURI", "http://keywordTermURI1")')
        print('MotCle1["keywordValue"] = SimplePrimitiveNode("keywordValue", "keywordValue1")')
        print('MotCle1["keywordVocabulary"] = SimplePrimitiveNode("keywordVocabulary", "keywordVocabulary1")')
        print('MotCle1["keywordVocabularyURI"] = SimplePrimitiveNode("keywordVocabularyURI", "http://keywordVocabularyURI1")')
        print("")
        print('MotCle2 = dict()')
        print('MotCle2["keywordTermURI"] = SimplePrimitiveNode("keywordTermURI", "http://keywordTermURI2")')
        print('MotCle2["keywordValue"] = SimplePrimitiveNode("keywordValue", "keywordValue2")')
        print('MotCle2["keywordVocabulary"] = SimplePrimitiveNode("keywordVocabulary", "keywordVocabulary2")')
        print('htMotCle2["keywordVocabularyURI"] = SimplePrimitiveNode("keywordVocabularyURI", "http://keywordVocabularyURI2")')
        print("")
        print("Je crée les producteurs du dataset")
        print('Producteur1 = dict()')
        print('Producteur1["producerAbbreviation"] = SimplePrimitiveNode("producerAbbreviation", "producerAbbreviation1")')
        print('Producteur1["producerAffiliation"] = SimplePrimitiveNode("producerAffiliation", "producerAffiliation1")')
        print('Producteur1["producerLogoURL"] = SimplePrimitiveNode("producerLogoURL", "http://producerLogoURL1")')
        print('Producteur1["producerName"] = SimplePrimitiveNode("producerName", "producerName1")')
        print('Producteur1["producerURL"] = SimplePrimitiveNode("producerURL", "http://producerURL1")')
        print("")
        print('Producteur2 = dict()')
        print('Producteur2["producerAbbreviation"] = SimplePrimitiveNode("producerAbbreviation", "producerAbbreviation2")')
        print('Producteur2["producerAffiliation"] = SimplePrimitiveNode("producerAffiliation", "producerAffiliation2")')
        print('Producteur2["producerLogoURL"] = SimplePrimitiveNode("producerLogoURL", "http://producerLogoURL2")')
        print('Producteur2["producerName"] = SimplePrimitiveNode("producerName", "producerName2")')
        print('Producteur2["producerURL"] = SimplePrimitiveNode("producerURL", "http://producerURL2")')
        print("")
        print("Je crée la période du dataset")
        print('Periode1 = dict()')
        print('Periode1["timePeriodCoveredStart"] = SimplePrimitiveNode("timePeriodCoveredStart", "2000-01-01")')
        print('Periode1["timePeriodCoveredEnd"] = SimplePrimitiveNode("timePeriodCoveredEnd", "2000-01-02")')
        print("")
        print('Periode2 = dict()')
        print('Periode2["timePeriodCoveredStart"] = SimplePrimitiveNode("timePeriodCoveredStart", "2000-01-03")')
        print('Periode2["timePeriodCoveredEnd"] = SimplePrimitiveNode("timePeriodCoveredEnd", "2000-01-04")')
        print("")
        print("Je crée le projet du dataset")
        print('Projet = dict()')
        print('Projet["projectAcronym"] = SimplePrimitiveNode("projectAcronym", "projectAcronym")')
        print('Projet["projectId"] = SimplePrimitiveNode("projectId", "projectId")')
        print('Projet["projectTask"] = SimplePrimitiveNode("projectTask", "projectTask")')
        print('Projet["projectTitle"] = SimplePrimitiveNode("projectTitle", "projectTitle")')
        print('Projet["projectURL"] = SimplePrimitiveNode("projectURL", "http://project.url")')
        print('Projet["projectWorkPackage"] = SimplePrimitiveNode("projectWorkPackage", "projectWP")')
        print("")
        print("Je crée les producteurs du dataset")
        print('Publication1 = dict()')
        print('Publication1["publicationCitation"] = SimplePrimitiveNode("publicationCitation", "relatedPublicationCitation1")')
        print('Publication1["publicationIDNumber"] = SimplePrimitiveNode("publicationIDNumber", "relatedPublicationIDNumber1")')
        print('Publication1["publicationIDType"] = SimpleControlledVocabularyNode("publicationIDType", "ark")')
        print('Publication1["publicationURL"] = SimplePrimitiveNode("publicationURL", "http://related.publication.url1")')
        print("")
        print('Publication2 = dict()')
        print('Publication2["publicationCitation"] = SimplePrimitiveNode("publicationCitation", "relatedPublicationCitation2")')
        print('Publication2["publicationIDNumber"] = SimplePrimitiveNode("publicationIDNumber", "relatedPublicationIDNumber2")')
        print('Publication2["publicationIDType"] = SimpleControlledVocabularyNode("publicationIDType", "arXiv")')
        print('Publication2["publicationURL"] = SimplePrimitiveNode("publicationURL", "http://related.publication.url2")')
        print("")
        print("Je crée les relatedDataset du dataset")
        print('RelatedDataset1 = dict()')
        print('RelatedDataset1["relatedDatasetCitation"] = SimplePrimitiveNode("relatedDatasetCitation", "relatedDatasetCitation1")')
        print('RelatedDataset1["relatedDatasetIDNumber"] = SimplePrimitiveNode("relatedDatasetIDNumber", "relatedDatasetIDNumber1")')
        print('RelatedDataset1["relatedDatasetIDType"] = SimpleControlledVocabularyNode("relatedDatasetIDType", "ark")')
        print('RelatedDataset1["relatedDatasetURL"] = SimplePrimitiveNode("relatedDatasetURL", "http://related.dataset.url1")')
        print("")
        print('RelatedDataset2 = dict()')
        print('RelatedDataset2["relatedDatasetCitation"] = SimplePrimitiveNode("relatedDatasetCitation", "relatedDatasetCitation2")')
        print('RelatedDataset2["relatedDatasetIDNumber"] = SimplePrimitiveNode("relatedDatasetIDNumber", "relatedDatasetIDNumber2")')
        print('RelatedDataset2["relatedDatasetIDType"] = SimpleControlledVocabularyNode("relatedDatasetIDType", "doi")')
        print('RelatedDataset2["relatedDatasetURL"] = SimplePrimitiveNode("relatedDatasetURL", "http://related.dataset.url2")')
        print("")
        print("Je crée la série du dataset")
        print('Serie = dict()')
        print('Serie["seriesInformation"] = SimplePrimitiveNode("seriesInformation", "seriesInformation")')
        print('Serie["seriesName"] = SimplePrimitiveNode("seriesName", "seriesName")')
        print("")
        print("Je crée les topics classifications du dataset")
        print('TopicClassification1 = dict()')
        print('TopicClassification1["topicClassValue"] = SimplePrimitiveNode("topicClassValue", "topicClassValue1")')
        print('TopicClassification1["topicClassVocab"] = SimplePrimitiveNode("topicClassVocab", "topicClassVocab1")')
        print('TopicClassification1["topicClassVocabURI"] = SimplePrimitiveNode("topicClassVocabURI", "http://topicClassVocabURI1")')
        print("")
        print('TopicClassification2 = dict()')
        print('TopicClassification2["topicClassValue"] = SimplePrimitiveNode("topicClassValue", "topicClassValue2")')
        print('TopicClassification2["topicClassVocab"] = SimplePrimitiveNode("topicClassVocab", "topicClassVocab2")')
        print('TopicClassification2["topicClassVocabURI"] = SimplePrimitiveNode("topicClassVocabURI", "http://topicClassVocabURI2")')
        print("")
        print("Je crée un node citation")
        print('cn = CitationNode()')
        print('cn.addField(SimplePrimitiveNode("title", "title"))')
        print('cn.addField(SimplePrimitiveNode("subtitle", "subtitle"))')
        print('cn.addField(SimplePrimitiveNode("alternativeTitle", "alternativeTitle"))')
        print('cn.addField(SimplePrimitiveNode("alternativeURL", "http://link.to.data"))')
        print('cn.addField(MultipleCompoundNode("otherId", [htAutreId1, htAutreId2]))')
        print('cn.addField(MultipleCompoundNode("datasetContact", [htContact1, htContact2]))')
        print('cn.addField(MultipleCompoundNode("author", [htAuteur1, htAuteur2]))')
        print('cn.addField(MultipleCompoundNode("contributor", [htContributeur1, htContributeur2]))')
        print('cn.addField(MultipleCompoundNode("producer", [htProducteur1, htProducteur2]))')
        print('cn.addField(SimplePrimitiveNode("productionDate", "2000-01-01"))')
        print('cn.addField(SimplePrimitiveNode("productionPlace", "productionPlace"))')
        print('cn.addField(MultipleCompoundNode("distributor", [htDistributeur1, htDistributeur2]))')
        print('cn.addField(SimplePrimitiveNode("distributionDate", "2000-01-01"))')
        print('cn.addField(MultipleCompoundNode("dsDescription", [htDescription1, htDescription2]))')
        print('cn.addField(MultipleControlledVocabularyNode("language", ["Abkhaz", "Afar"]))')
        print('cn.addField(MultipleControlledVocabularyNode("subject", ["Agricultural Sciences", "Arts and Humanities"]))')
        print('cn.addField(MultipleCompoundNode("keyword", [htMotCle1, htMotCle1]))')
        print('cn.addField(MultipleCompoundNode("topicClassification", [htTopicClassification1, htTopicClassification2]))')
        print('cn.addField(MultipleControlledVocabularyNode("kindOfData", ["Audiovisual", "Collection"]))')
        print('cn.addField(MultiplePrimitiveNode("kindOfDataOther", ["otherKindOfData1", "otherKindOfData2"]))')
        print('cn.addField(MultipleControlledVocabularyNode("dataOrigin", ["observational data", "experimental data"]))')
        print('cn.addField(MultiplePrimitiveNode("dataSources", ["dataSource1", "dataSource2"]))')
        print('cn.addField(SimplePrimitiveNode("originOfSources", "originOfSource"))')
        print('cn.addField(SimplePrimitiveNode("characteristicOfSources", "characteristicOfSource"))')
        print('cn.addField(SimplePrimitiveNode("accessToSources", "accessToSource"))')
        print('cn.addField(MultipleCompoundNode("software", [htLogiciel1, htLogiciel2]))')
        print('cn.addField(SimpleCompoundNode("series", htSerie))')
        print('cn.addField(MultipleControlledVocabularyNode("lifeCycleStep", ["Study proposal", "Funding"]))')
        print('cn.addField(SimplePrimitiveNode("notesText", "notes"))')
        print('cn.addField(MultipleCompoundNode("publication", [htPublication1, htPublication2]))')
        print('cn.addField(MultiplePrimitiveNode("relatedMaterial", ["relatedMaterial1", "relatedMaterial2"]))')
        print('cn.addField(MultipleCompoundNode("relatedDataset", [htRelatedDataset1, htRelatedDataset2]))')
        print('cn.addField(MultiplePrimitiveNode("otherReferences", ["otherReference1", "otherReference2"]))')
        print('cn.addField(MultipleCompoundNode("grantNumber", [htGrantNumber1, htGrantNumber2]))')
        print('cn.addField(SimpleCompoundNode("project", htProjet))')
        print('cn.addField(MultipleCompoundNode("timePeriodCovered", [htPeriode1, htPeriode2]))')
        print('cn.addField(MultipleCompoundNode("dateOfCollection", [htDates1, htDates2]))')
        print('cn.addField(SimplePrimitiveNode("depositor", "depositor"))')
        print('cn.addField(SimplePrimitiveNode("dateOfDeposit", "2000-01-01"))')
        print("")
        print('print(cn.toJSON())')

    def  HowToBuildADerivedTextNode():
        """!
        <c>HowToBuildADerivedTextNode</c> montre la manière de construire un <c>DerivedTextNode</c>

        <hr>
        <strong>Exemple de code : </strong>
        <pre>
            htSource1 = dict()
            htSource1["ageOfSource"] = MultiplePrimitiveNode("ageOfSource", ["sourceAge1"])
            htSource1["citations"] = MultiplePrimitiveNode("citations", ["1"])
            htSource1["experimentNumber"] = MultiplePrimitiveNode("experimentNumber", ["1"])
            htSource1["typeOfSource"] = MultiplePrimitiveNode("typeOfSource", ["sourceType1"])

            htSource2 = dict()
            htSource2["ageOfSource"] = MultiplePrimitiveNode("ageOfSource", ["sourceAge2"])
            htSource2["citations"] = MultiplePrimitiveNode("citations", ["2"])
            htSource2["experimentNumber"] = MultiplePrimitiveNode("experimentNumber", ["2"])
            htSource2["typeOfSource"] = MultiplePrimitiveNode("typeOfSource", ["sourceType2"])

            # Je crée un node derived-text
            dtn = DerivedTextNode()
            dtn.addField(MultipleCompoundNode("source", [htSource1, htSource2]))

            print(dtn.toJSON())
        </pre>
        """
        print('htSource1 = dict()')
        print('htSource1["ageOfSource"] = MultiplePrimitiveNode("ageOfSource", ["sourceAge1"])')
        print('htSource1["citations"] = MultiplePrimitiveNode("citations", ["1"])')
        print('htSource1["experimentNumber"] = MultiplePrimitiveNode("experimentNumber", ["1"])')
        print('htSource1["typeOfSource"] = MultiplePrimitiveNode("typeOfSource", ["sourceType1"])')
        print("")
        print('htSource2 = dict()')
        print('htSource2["ageOfSource"] = MultiplePrimitiveNode("ageOfSource", ["sourceAge2"])')
        print('htSource2["citations"] = MultiplePrimitiveNode("citations", ["2"])')
        print('htSource2["experimentNumber"] = MultiplePrimitiveNode("experimentNumber", ["2"])')
        print('htSource2["typeOfSource"] = MultiplePrimitiveNode("typeOfSource", ["sourceType2"])')
        print("")
        print('# Je crée un node derived-text')
        print('dtn = DerivedTextNode()')
        print('dtn.addField(MultipleCompoundNode("source", [htSource1, htSource2]))')
        print("")
        print('print(dtn.toJSON())')

    def  HowToBuildAGeospatialNode():
        """!
        <c>HowToBuildAGeospatialNode</c> montre la manière de construire un <c>GeospatialNode</c>

        <hr>
        <strong>Exemple de code : </strong>
        <pre>
            htConformite1 = dict()
            htConformite1["specification"] = MultiplePrimitiveNode("specification", ["conformitySpecification1"])
            htConformite1["degree"] = SimpleControlledVocabularyNode("degree", "Conformant")

            htConformite2 = dict()
            htConformite2["specification"] = MultiplePrimitiveNode("specification", ["conformitySpecification2"])
            htConformite2["degree"] = SimpleControlledVocabularyNode("degree", "Not Conformant")

            # Je crée la boite geographique du dataset
            htBoite1 = dict()
            htBoite1["eastLongitude"] = SimplePrimitiveNode("eastLongitude", "0")
            htBoite1["northLongitude"] = SimplePrimitiveNode("northLongitude", "1")
            htBoite1["southLongitude"] = SimplePrimitiveNode("southLongitude", "2")
            htBoite1["westLongitude"] = SimplePrimitiveNode("westLongitude", "3")

            htBoite2 = dict()
            htBoite2["eastLongitude"] = SimplePrimitiveNode("eastLongitude", "4")
            htBoite2["northLongitude"] = SimplePrimitiveNode("northLongitude", "5")
            htBoite2["southLongitude"] = SimplePrimitiveNode("southLongitude", "6")
            htBoite2["westLongitude"] = SimplePrimitiveNode("westLongitude", "7")

            # Je crée la couverture geographique du dataset
            htCouverture1 = dict()
            htCouverture1["city"] = SimplePrimitiveNode("city", "geographicCoverageCity1")
            htCouverture1["country"] = SimpleControlledVocabularyNode("country", "Afghanistan")
            htCouverture1["otherGeographicCoverage"] = SimplePrimitiveNode("otherGeographicCoverage", "geographicCoverageOther1")
            htCouverture1["state"] = SimplePrimitiveNode("state", "geographicCoverageProvince1")

            htCouverture2 = dict()
            htCouverture2["city"] = SimplePrimitiveNode("city", "geographicCoverageCity2")
            htCouverture2["country"] = SimpleControlledVocabularyNode("country", "Albania")
            htCouverture2["otherGeographicCoverage"] = SimplePrimitiveNode("otherGeographicCoverage", "geographicCoverageOther2")
            htCouverture2["state"] = SimplePrimitiveNode("state", "geographicCoverageProvince2")

            # Je crée la qualité du dataset
            htQualite1 = dict()
            htQualite1["lineage"] = MultiplePrimitiveNode("lineage", ["QualityAndValidityLineage1"])
            htQualite1["spatialResolution"] = MultiplePrimitiveNode("lineage", ["QualityAndValiditySpatialResolution1"])

            htQualite2 = dict()
            htQualite2["lineage"] = MultiplePrimitiveNode("lineage", ["QualityAndValidityLineage2"])
            htQualite2["spatialResolution"] = MultiplePrimitiveNode("lineage", ["QualityAndValiditySpatialResolution2"])


            # Je crée un node geospatial
            gn = GeospatialNode()
            gn.addField(MultipleCompoundNode("geographicCoverage", [htCouverture1, htCouverture2]))
            gn.addField(MultiplePrimitiveNode("geographicUnit", ["geographicUnit1", "geographicUnit2"]))
            gn.addField(MultipleCompoundNode("geographicBoundingBox", [htBoite1, htBoite2]))
            gn.addField(MultipleCompoundNode("qualityValidity", [htQualite1, htQualite2]))
            gn.addField(MultipleCompoundNode("conformity", [htConformite1, htConformite2]))

            print(gn.toJSON())
        </pre>
        """
        print('htConformite1 = dict()')
        print('htConformite1["specification"] = MultiplePrimitiveNode("specification", ["conformitySpecification1"])')
        print('htConformite1["degree"] = SimpleControlledVocabularyNode("degree", "Conformant")')
        print("")
        print('htConformite2 = dict()')
        print('htConformite2["specification"] = MultiplePrimitiveNode("specification", ["conformitySpecification2"])')
        print('htConformite2["degree"] = SimpleControlledVocabularyNode("degree", "Not Conformant")')
        print("")
        print('# Je crée la boite geographique du dataset')
        print('htBoite1 = dict()')
        print('htBoite1["eastLongitude"] = SimplePrimitiveNode("eastLongitude", "0")')
        print('htBoite1["northLongitude"] = SimplePrimitiveNode("northLongitude", "1")')
        print('htBoite1["southLongitude"] = SimplePrimitiveNode("southLongitude", "2")')
        print('htBoite1["westLongitude"] = SimplePrimitiveNode("westLongitude", "3")')
        print("")
        print('htBoite2 = dict()')
        print('htBoite2["eastLongitude"] = SimplePrimitiveNode("eastLongitude", "4")')
        print('htBoite2["northLongitude"] = SimplePrimitiveNode("northLongitude", "5")')
        print('htBoite2["southLongitude"] = SimplePrimitiveNode("southLongitude", "6")')
        print('htBoite2["westLongitude"] = SimplePrimitiveNode("westLongitude", "7")')
        print("")
        print('# Je crée la couverture geographique du dataset')
        print('htCouverture1 = dict()')
        print('htCouverture1["city"] = SimplePrimitiveNode("city", "geographicCoverageCity1")')
        print('htCouverture1["country"] = SimpleControlledVocabularyNode("country", "Afghanistan")')
        print('htCouverture1["otherGeographicCoverage"] = SimplePrimitiveNode("otherGeographicCoverage", "geographicCoverageOther1")')
        print('htCouverture1["state"] = SimplePrimitiveNode("state", "geographicCoverageProvince1")')
        print("")
        print('htCouverture2 = dict()')
        print('htCouverture2["city"] = SimplePrimitiveNode("city", "geographicCoverageCity2")')
        print('htCouverture2["country"] = SimpleControlledVocabularyNode("country", "Albania")')
        print('htCouverture2["otherGeographicCoverage"] = SimplePrimitiveNode("otherGeographicCoverage", "geographicCoverageOther2")')
        print('htCouverture2["state"] = SimplePrimitiveNode("state", "geographicCoverageProvince2")')
        print("")
        print('# Je crée la qualité du dataset')
        print('htQualite1 = dict()')
        print('htQualite1["lineage"] = MultiplePrimitiveNode("lineage", ["QualityAndValidityLineage1"])')
        print('htQualite1["spatialResolution"] = MultiplePrimitiveNode("lineage", ["QualityAndValiditySpatialResolution1"])')
        print("")
        print('htQualite2 = dict()')
        print('htQualite2["lineage"] = MultiplePrimitiveNode("lineage", ["QualityAndValidityLineage2"])')
        print('htQualite2["spatialResolution"] = MultiplePrimitiveNode("lineage", ["QualityAndValiditySpatialResolution2"])')
        print("")
        print('# Je crée un node geospatial')
        print('gn = GeospatialNode()')
        print('gn.addField(MultipleCompoundNode("geographicCoverage", [htCouverture1, htCouverture2]))')
        print('gn.addField(MultiplePrimitiveNode("geographicUnit", ["geographicUnit1", "geographicUnit2"]))')
        print('gn.addField(MultipleCompoundNode("geographicBoundingBox", [htBoite1, htBoite2]))')
        print('gn.addField(MultipleCompoundNode("qualityValidity", [htQualite1, htQualite2]))')
        print('gn.addField(MultipleCompoundNode("conformity", [htConformite1, htConformite2]))')
        print("")
        print('print(gn.toJSON())')

    def HowToBuildAJournalNode():
        """!
        <c>HowToBuildAJournalNode</c> montre la manière de construire un <c>JournalNode</c>

        <hr>
        <strong>Exemple de code : </strong>
        <pre>
            # Je crée les support de publication du dataset
            htJournalVolumeIssue1 = dict();
            htJournalVolumeIssue1["journalIssue"] = SimplePrimitiveNode("journalIssue", "journalIssue1")
            htJournalVolumeIssue1["journalPubDate"] = SimplePrimitiveNode("journalPubDate", "2000-01-01")
            htJournalVolumeIssue1["journalVolume"] = SimplePrimitiveNode("journalVolume", "journalVolume1")

            htJournalVolumeIssue2 = dict();
            htJournalVolumeIssue2["journalIssue"] = SimplePrimitiveNode("journalIssue", "journalIssue2")
            htJournalVolumeIssue2["journalPubDate"] = SimplePrimitiveNode("journalPubDate", "2000-01-02")
            htJournalVolumeIssue2["journalVolume"] = SimplePrimitiveNode("journalVolume", "journalVolume2")

            # Je crée un node journal
            jn = JournalNode();
            jn.addField(MultipleCompoundNode("journalVolumeIssue", [htJournalVolumeIssue1, htJournalVolumeIssue2]))
            jn.addField(SimpleControlledVocabularyNode("journalArticleType", "abstract"))

            print(jn.toJSON())
        </pre>
        """
        print('# Je crée les support de publication du dataset')
        print('htJournalVolumeIssue1 = dict()')
        print('htJournalVolumeIssue1["journalIssue"] = SimplePrimitiveNode("journalIssue", "journalIssue1")')
        print('htJournalVolumeIssue1["journalPubDate"] = SimplePrimitiveNode("journalPubDate", "2000-01-01")')
        print('htJournalVolumeIssue1["journalVolume"] = SimplePrimitiveNode("journalVolume", "journalVolume1")')
        print("")
        print('htJournalVolumeIssue2 = dict();')
        print('htJournalVolumeIssue2["journalIssue"] = SimplePrimitiveNode("journalIssue", "journalIssue2")')
        print('htJournalVolumeIssue2["journalPubDate"] = SimplePrimitiveNode("journalPubDate", "2000-01-02")')
        print('htJournalVolumeIssue2["journalVolume"] = SimplePrimitiveNode("journalVolume", "journalVolume2")')
        print("")
        print('# Je crée un node journal')
        print('jn = JournalNode();')
        print('jn.addField(MultipleCompoundNode("journalVolumeIssue", [htJournalVolumeIssue1, htJournalVolumeIssue2]))')
        print('jn.addField(SimpleControlledVocabularyNode("journalArticleType", "abstract"))')
        print("")
        print('print(jn.toJSON())')

    def HowToBuildAMetadataDocument():
        """!
        <c>HowToBuildAMetadataDocument()</c> montre la manière de construire un <c>MetadataDocument</c>

        <hr>
        <strong>Exemple de code : </strong>
        <pre>
            bn = BiomedicalNode()       # ... puis configurer bn  comme ici : @see HowToBuildABiomedicalNode
            cn = CitationNode()         # ... puis configurer cn  comme ici : @see HowToBuildACitationNode
            dtn = DerivedTextNode()     # ... puis configurer dtn comme ici : @see HowToBuildADerivedTextNode
            gn = GeospatialNode()       # ... puis configurer gn  comme ici : @see HowToBuildAGeospatialNode
            jn = JournalNode()          # ... puis configurer jn  comme ici : @see HowToBuildAJournalNode
            sn = SemanticsNode()        # ... puis configurer sn  comme ici : @see HowToBuildASemanticsNode
            ssn = SocialScienceNode()   # ... puis configurer ssn comme ici : @see HowToBuildASocialScienceNode

            md = MetadataDocument()
            md.addCitationNode(getCitationNode())
            md.addGeospatialNode(getGeospatialNode())
            md.addSocialScienceNode(getSocialScienceNode())
            md.addBiomedicalNode(getBiomedicalNode())
            md.addJournalNode(getJournalNode())
            md.addDerivedTextNode(getDerivedTextNode())
            md.addSemanticsNode(getSemanticsNode())

            print(md.toJSON())
        </pre>
        """
        print('bn = BiomedicalNode()       # ... puis configurer bn')
        print('cn = CitationNode()         # ... puis configurer cn')
        print('dtn = DerivedTextNode()     # ... puis configurer dtn')
        print('gn = GeospatialNode()       # ... puis configurer gn')
        print('jn = JournalNode()          # ... puis configurer jn')
        print('sn = SemanticsNode()        # ... puis configurer sn')
        print('ssn = SocialScienceNode()   # ... puis configurer ssn')
        print("")
        print('md = MetadataDocument()')
        print('md.addCitationNode(getCitationNode())')
        print('md.addGeospatialNode(getGeospatialNode())')
        print('md.addSocialScienceNode(getSocialScienceNode())')
        print('md.addBiomedicalNode(getBiomedicalNode())')
        print('md.addJournalNode(getJournalNode())')
        print('md.addDerivedTextNode(getDerivedTextNode())')
        print('md.addSemanticsNode(getSemanticsNode())')
        print("")
        print('print(md.toJSON())')

    def HowToBuildAMinimalCitationNode():
        """!
        <c>HowToBuildAMinimalCitationNode</c> montre la manière de construire un <c>CitationNode</c> minimal

        <hr>
        <strong>Exemple de code : </strong>
        <pre>
            # Je crée les auteurs du dataset
            htAuteur1 = dict()
            htAuteur1["authorAffiliation"] = SimplePrimitiveNode("authorAffiliation", "authorAffiliation1")
            htAuteur1["authorIdentifier"] = SimplePrimitiveNode("authorIdentifier", "authorIdentifier1")
            htAuteur1["authorIdentifierScheme"] = SimpleControlledVocabularyNode("authorIdentifierScheme", "ORCID")
            htAuteur1["authorName"] = SimplePrimitiveNode("authorName", "authorName1")

            htAuteur2 = dict()
            htAuteur2["authorAffiliation"] = SimplePrimitiveNode("authorAffiliation", "authorAffiliation2")
            htAuteur2["authorIdentifier"] = SimplePrimitiveNode("authorIdentifier", "authorIdentifier2")
            htAuteur2["authorIdentifierScheme"] = SimpleControlledVocabularyNode("authorIdentifierScheme", "idHAL")
            htAuteur2["authorName"] = SimplePrimitiveNode("authorName", "authorName2")

            # Je crée les contacts du dataset
            htContact1 = dict()
            htContact1["datasetContactAffiliation"] = SimplePrimitiveNode("datasetContactAffiliation", "contactAffiliation1")
            htContact1["datasetContactEmail"] = SimplePrimitiveNode("datasetContactEmail", "contact1@mail.com")
            htContact1["datasetContactName"] = SimplePrimitiveNode("datasetContactName", "contactName1")

            htContact2 = dict()
            htContact2["datasetContactAffiliation"] = SimplePrimitiveNode("datasetContactAffiliation", "contactAffiliation2")
            htContact2["datasetContactEmail"] = SimplePrimitiveNode("datasetContactEmail", "contact2@mail.com")
            htContact2["datasetContactName"] = SimplePrimitiveNode("datasetContactName", "contactName2")

            # Je crée les descriptions du dataset
            htDescription1 = dict()
            htDescription1["dsDescriptionDate"] = SimplePrimitiveNode("dsDescriptionDate", "2000-01-01")
            htDescription1["dsDescriptionValue"] = SimplePrimitiveNode("dsDescriptionValue", "descriptionText1")

            htDescription2 = dict()
            htDescription2["dsDescriptionDate"] = SimplePrimitiveNode("dsDescriptionDate", "2000-01-01")
            htDescription2["dsDescriptionValue"] = SimplePrimitiveNode("dsDescriptionValue", "descriptionText2")

            # Je crée un node citation
            cn = CitationNode()
            cn.addField(SimplePrimitiveNode("title", "title"))
            cn.addField(MultipleCompoundNode("datasetContact", [htContact1, htContact2]))
            cn.addField(MultipleCompoundNode("author", [htAuteur1, htAuteur2]))
            cn.addField(MultipleCompoundNode("dsDescription", [htDescription1, htDescription2]))    
            cn.addField(MultipleControlledVocabularyNode("subject", ["Agricultural Sciences", "Arts and Humanities"]))    
            cn.addField(MultipleControlledVocabularyNode("kindOfData", ["Audiovisual", "Collection"]))

            print(cn.toJSON())
        </pre>
        """
        print('# Je crée les auteurs du dataset')
        print('htAuteur1 = dict()')
        print('htAuteur1["authorAffiliation"] = SimplePrimitiveNode("authorAffiliation", "authorAffiliation1")')
        print('htAuteur1["authorIdentifier"] = SimplePrimitiveNode("authorIdentifier", "authorIdentifier1")')
        print('htAuteur1["authorIdentifierScheme"] = SimpleControlledVocabularyNode("authorIdentifierScheme", "ORCID")')
        print('htAuteur1["authorName"] = SimplePrimitiveNode("authorName", "authorName1")')
        print("")
        print('htAuteur2 = dict()')
        print('htAuteur2["authorAffiliation"] = SimplePrimitiveNode("authorAffiliation", "authorAffiliation2")')
        print('htAuteur2["authorIdentifier"] = SimplePrimitiveNode("authorIdentifier", "authorIdentifier2")')
        print('htAuteur2["authorIdentifierScheme"] = SimpleControlledVocabularyNode("authorIdentifierScheme", "idHAL")')
        print('htAuteur2["authorName"] = SimplePrimitiveNode("authorName", "authorName2")')
        print("")
        print('# Je crée les contacts du dataset')
        print('htContact1 = dict()')
        print('htContact1["datasetContactAffiliation"] = SimplePrimitiveNode("datasetContactAffiliation", "contactAffiliation1")')
        print('htContact1["datasetContactEmail"] = SimplePrimitiveNode("datasetContactEmail", "contact1@mail.com")')
        print('htContact1["datasetContactName"] = SimplePrimitiveNode("datasetContactName", "contactName1")')
        print("")
        print('htContact2 = dict()')
        print('htContact2["datasetContactAffiliation"] = SimplePrimitiveNode("datasetContactAffiliation", "contactAffiliation2")')
        print('htContact2["datasetContactEmail"] = SimplePrimitiveNode("datasetContactEmail", "contact2@mail.com")')
        print('htContact2["datasetContactName"] = SimplePrimitiveNode("datasetContactName", "contactName2")')
        print("")
        print('# Je crée les descriptions du dataset')
        print('htDescription1 = dict()')
        print('htDescription1["dsDescriptionDate"] = SimplePrimitiveNode("dsDescriptionDate", "2000-01-01")')
        print('htDescription1["dsDescriptionValue"] = SimplePrimitiveNode("dsDescriptionValue", "descriptionText1")')
        print("")
        print('htDescription2 = dict()')
        print('htDescription2["dsDescriptionDate"] = SimplePrimitiveNode("dsDescriptionDate", "2000-01-01")')
        print('htDescription2["dsDescriptionValue"] = SimplePrimitiveNode("dsDescriptionValue", "descriptionText2")')
        print("")
        print('# Je crée un node citation')
        print('cn = CitationNode()')
        print('cn.addField(SimplePrimitiveNode("title", "title"))')
        print('cn.addField(MultipleCompoundNode("datasetContact", [htContact1, htContact2]))')
        print('cn.addField(MultipleCompoundNode("author", [htAuteur1, htAuteur2]))')
        print('cn.addField(MultipleCompoundNode("dsDescription", [htDescription1, htDescription2]))')    
        print('cn.addField(MultipleControlledVocabularyNode("subject", ["Agricultural Sciences", "Arts and Humanities"]))')    
        print('cn.addField(MultipleControlledVocabularyNode("kindOfData", ["Audiovisual", "Collection"]))')

        print('print(cn.toJSON())')

    def HowToBuildAMultipleCompoundNode():
        """!
        <c>HowToBuildAMultipleCompoundNode</c> montre la manière de construire un <c>MultipleCompoundNode</c>

        <hr>
        <strong>Exemple de code : </strong>
        <pre>
            auteur1 = dict([("authorName", SimplePrimmitiveNode("authorName", "Vincent, Tom"),
                            ("authorAffiliation", SimplePrimitiveNode("authorAffiliation", "INRAE")),
                            ("authorIdentifier", SimplePrimitiveNode("authorIdentifier", "https://orcid.org/0000-0002-3466-1099")),
                            ("authorIdentifierScheme", SimpleControlledVocabularyNode("authorIdentifierScheme", "ORCID"))])
            
            auteur2 = dict([("authorName", SimplePrimmitiveNode("authorName", "Rave, Beth"),
                            ("authorAffiliation", SimplePrimitiveNode("authorAffiliation", "INRAE"))])

            mcn = MultipleCompoundNode("author", [auteur1, auteur2])
            print(mcn.toJSON())
        </pre>
        """
        print('auteur1 = dict([("authorName", SimplePrimmitiveNode("authorName", "Vincent, Tom"),')
        print('                ("authorAffiliation", SimplePrimitiveNode("authorAffiliation", "INRAE")),')
        print('                ("authorIdentifier", SimplePrimitiveNode("authorIdentifier", "https://orcid.org/0000-0002-3466-1099")),')
        print('                ("authorIdentifierScheme", SimpleControlledVocabularyNode("authorIdentifierScheme", "ORCID"))])')
        print("")    
        print('auteur2 = dict([("authorName", SimplePrimmitiveNode("authorName", "Rave, Beth"),')
        print('                ("authorAffiliation", SimplePrimitiveNode("authorAffiliation", "INRAE"))])')
        print("")
        print('mcn = MultipleCompoundNode("author", [auteur1, auteur2])')
        print('print(mcn.toJSON())')

    def HowToBuildAMultipleControlledVocabularyNode():
        """!
        <c>HowToBuildAMultipleControlledVocabularyNode</c> montre la manière de construire un <c>MultipleControlledVocabularyNode</c>

        <hr>
        <strong>Exemple de code : </strong>
        <pre>
            values = ["French","English","Occitan"]
            mcvn = MultipleControlledVocabularyNode("language", values)
            print(mcvn.toJSON())
        </pre>
        """
        print('values = ["French","English","Occitan"]')
        print('mcvn = MultipleControlledVocabularyNode("language", values)')
        print('print(mcvn.toJSON())')

    def HowToBuildAMultiplePrimitiveNode():
        """!
        <c>HowToBuildAMultiplePrimitiveNode</c> montre la manière de construire un <c>MultiplePrimitiveNode</c>

        <hr>
        <strong>Exemple de code : </strong>
        <pre>
        values = ["value1","value2","value3"]
        mpn = MultiplePrimitiveNode("dataSources", values)
        print(mpn.toJSON())
        </pre>
        """
        print('values = ["value1","value2","value3"]')
        print('mpn = MultiplePrimitiveNode("dataSources", values)')
        print('print(mpn.toJSON())')

    def HowToBuildASemanticsNode():
        """!
        
        <hr>
        <strong>Exemple de code : </strong>
        <pre>
            # Je crée la version de ressource du dataset
            htResourceVersion = dict()
            htResourceVersion["versionStatus"] = MultipleControlledVocabularyNode("versionStatus", ["alpha", "beta"])
            htResourceVersion["versionInfo"] = SimplePrimitiveNode("versionInfo", "version")
            htResourceVersion["priorVersion"] = MultiplePrimitiveNode("priorVersion", ["http://prior.version"])
            htResourceVersion["modificationDate"] = SimplePrimitiveNode("modificationDate", "2000-01-01")
            htResourceVersion["changes"] = SimplePrimitiveNode("changes", "http://semantic.version.changes")

            # Je crée un node semantics
            sn = SemanticsNode()
            sn.addField(SimpleControlledVocabularyNode("hasFormalityLevel", "Classification scheme"))
            sn.addField(MultipleControlledVocabularyNode("hasOntologyLanguage", "JSON-LD"))
            sn.addField(SimpleControlledVocabularyNode("typeOfSR", "Application Ontology"))
            sn.addField(MultipleControlledVocabularyNode("designedForOntologyTask", ["Annotation Task", "Configuration Task"]))
            sn.addField(SimplePrimitiveNode("knownUsage", "knownUsage"))
            sn.addField(SimpleCompoundNode("resourceVersion", htResourceVersion))
            sn.addField(MultiplePrimitiveNode("imports", ["http://semantic.imports1", "http://semantic.imports2"]))
            sn.addField(SimplePrimitiveNode("bugDatabase", "http://semantic.bug.tracker"))
            sn.addField(SimplePrimitiveNode("endpoint", "http://semantic.sparql.endpoint"))
            sn.addField(MultiplePrimitiveNode("imports", ["http://semantic.uri1", "http://semantic.uri2"]))

            print(sn.toJSON())
        </pre>
        """
        print('# Je crée la version de ressource du dataset')
        print('htResourceVersion = dict()')
        print('htResourceVersion["versionStatus"] = MultipleControlledVocabularyNode("versionStatus", ["alpha", "beta"])')
        print('htResourceVersion["versionInfo"] = SimplePrimitiveNode("versionInfo", "version")')
        print('htResourceVersion["priorVersion"] = MultiplePrimitiveNode("priorVersion", ["http://prior.version"])')
        print('htResourceVersion["modificationDate"] = SimplePrimitiveNode("modificationDate", "2000-01-01")')
        print('htResourceVersion["changes"] = SimplePrimitiveNode("changes", "http://semantic.version.changes")')
        print("")
        print('# Je crée un node semantics')
        print('sn = SemanticsNode()')
        print('sn.addField(SimpleControlledVocabularyNode("hasFormalityLevel", "Classification scheme"))')
        print('sn.addField(MultipleControlledVocabularyNode("hasOntologyLanguage", "JSON-LD"))')
        print('sn.addField(SimpleControlledVocabularyNode("typeOfSR", "Application Ontology"))')
        print('sn.addField(MultipleControlledVocabularyNode("designedForOntologyTask", ["Annotation Task", "Configuration Task"]))')
        print('sn.addField(SimplePrimitiveNode("knownUsage", "knownUsage"))')
        print('sn.addField(SimpleCompoundNode("resourceVersion", htResourceVersion))')
        print('sn.addField(MultiplePrimitiveNode("imports", ["http://semantic.imports1", "http://semantic.imports2"]))')
        print('sn.addField(SimplePrimitiveNode("bugDatabase", "http://semantic.bug.tracker"))')
        print('sn.addField(SimplePrimitiveNode("endpoint", "http://semantic.sparql.endpoint"))')
        print('sn.addField(MultiplePrimitiveNode("imports", ["http://semantic.uri1", "http://semantic.uri2"]))')
        print("")
        print('print(sn.toJSON())')

    def HowToBuildASimpleCompoundNode():
        """!
        <c>HowToBuildASimpleCompoundNode</c> montre la manière de construire un <c>SimpleCompoundNode</c>

        <hr>
        <strong>Exemple de code : </strong>
        <pre>
            project = dict([("projectAcronym", SimplePrimitiveNode("projectAcronym", "projectAcronym")),
                            ("prrojectId", SimplePrimitiveNode("projectId", "projectId")),
                            ("projectTitle", SimplePrimitiveNode("projectTitle", "projectTitle")),
                            ("projectTask", SimplePrimitiveNode("projectTask", "projectTask")),
                            ("projectURL", SimplePrimitiveNode("projectURL", "projectURL")),
                            ("projectWorkPackage", SimplePrimitiveNode("projectWorkPackage", "projectWorkPackage"))])

            scn = SimpleCompoundNode("project", project)
            print(scn.toJSON())
        </pre>
        """
        print('project = dict([("projectAcronym", SimplePrimitiveNode("projectAcronym", "projectAcronym")),')
        print('                ("prrojectId", SimplePrimitiveNode("projectId", "projectId")),')
        print('                ("projectTitle", SimplePrimitiveNode("projectTitle", "projectTitle")),')
        print('                ("projectTask", SimplePrimitiveNode("projectTask", "projectTask")),')
        print('                ("projectURL", SimplePrimitiveNode("projectURL", "projectURL")),')
        print('                ("projectWorkPackage", SimplePrimitiveNode("projectWorkPackage", "projectWorkPackage"))])')
        print("")
        print('scn = SimpleCompoundNode("project", project)')
        print('print(scn.toJSON())')

    def HowToBuildASimpleControlledVocabularyNode():
        """!
        <c>HowToBuildASimpleControlledVocabularyNode</c> montre la manière de construire un <c>SimpleControlledVocabularyNode</c>

        <hr>
        <strong>Exemple de code : </strong>
        <pre>
            scvn = SimpleControlledVocabularyNode("journalArticleType", "news")
            print(scvn.toJSON())
        </pre>
        """
        print('scvn = SimpleControlledVocabularyNode("journalArticleType", "news")')
        print('print(scvn.toJSON())')

    def HowToBuildASimplePrimitiveNode():
        """!
        <c>HowToBuildASimplePrimitiveNode</c> montre la manière de construire un <c>SimplePrimitiveNode</c>

        <hr>
        <strong>Exemple de code : </strong>
        <pre>
            spn = SimplePrimitiveNode("softwareName", "softwareName")
            print(spn.toJSON())
        </pre>
        """
        print('spn = SimplePrimitiveNode("softwareName", "softwareName")')
        print('print(spn.toJSON())')

    def HowToBuildASocialScienceNode():
        """!
        <c>HowToBuildASocialScienceNode</c> montre la manière de construire un <c>SocialScienceNode</c>

        <hr>
        <strong>Exemple de code : </strong>
        <pre>
            # Je crée la taille de l'échantillon 
            htTargetSampleSize = dict()
            htTargetSampleSize["targetSampleActualSize"] = SimplePrimitiveNode("targetSampleActualSize", "42")
            htTargetSampleSize["targetSampleSizeFormula"] = SimplePrimitiveNode("targetSampleSizeFormula", "targetSampleSizeFormula")

            # Je crée notes du node
            htSocialScienceNotes = dict()
            htSocialScienceNotes["socialScienceNotesSubject"] = SimplePrimitiveNode("socialScienceNotesSubject", "socialScienceNotesSubject")
            htSocialScienceNotes["socialScienceNotesText"] = SimplePrimitiveNode("socialScienceNotesText", "socialScienceNotesText")
            htSocialScienceNotes["socialScienceNotesType"] = SimplePrimitiveNode("socialScienceNotesType", "socialScienceNotesType")

            # Je crée notes du node
            htGeographicalReferential1 = dict()
            htGeographicalReferential1["level"] = SimplePrimitiveNode("level", "geographicalReferentialLevel1")
            htGeographicalReferential1["version"] = SimplePrimitiveNode("version", "geographicalReferentialVersion1")

            htGeographicalReferential2 = dict()
            htGeographicalReferential2["level"] = SimplePrimitiveNode("level", "geographicalReferentialLevel2")
            htGeographicalReferential2["version"] = SimplePrimitiveNode("version", "geographicalReferentialVersion2")


            # Je crée un node socialscience
            ssn = SocialScienceNode()
            ssn.addField(MultipleControlledVocabularyNode("unitOfAnalysis", ["Individual","Organization"]))
            ssn.addField(MultiplePrimitiveNode("universe", ["universe1","universe2"]))
            ssn.addField(SimpleControlledVocabularyNode("timeMethod", "Longitudinal"))
            ssn.addField(SimplePrimitiveNode("timeMethodOther", "timeMethodOther"))
            ssn.addField(SimplePrimitiveNode("dataCollector", "dataCollector"))
            ssn.addField(SimplePrimitiveNode("collectorTraining", "collectorTraining"))
            ssn.addField(SimplePrimitiveNode("frequencyOfDataCollection", "frequency"))
            ssn.addField(SimpleControlledVocabularyNode("samplingProcedure", "Total universe/Complete enumeration"))
            ssn.addField(SimplePrimitiveNode("samplingProcedureOther", "samplingProcedureOther"))
            ssn.addField(SimpleCompoundNode("targetSampleSize", htTargetSampleSize))
            ssn.addField(SimplePrimitiveNode("deviationsFromSampleDesign", "deviationsFromSampleDesign"))
            ssn.addField(SimpleControlledVocabularyNode("collectionMode", "Interview"))
            ssn.addField(SimplePrimitiveNode("collectionModeOther", "collectionModeOther"))
            ssn.addField(SimplePrimitiveNode("researchInstrument", "researchInstrument"))
            ssn.addField(SimplePrimitiveNode("dataCollectionSituation", "dataCollectionSituation"))
            ssn.addField(SimplePrimitiveNode("actionsToMinimizeLoss", "actionsToMinimizeLoss"))
            ssn.addField(SimplePrimitiveNode("controlOperations", "controlOperations"))
            ssn.addField(SimplePrimitiveNode("weighting", "weighting"))
            ssn.addField(SimplePrimitiveNode("cleaningOperations", "cleaningOperations"))
            ssn.addField(SimplePrimitiveNode("datasetLevelErrorNotes", "datasetLevelErrorNotes"))
            ssn.addField(SimplePrimitiveNode("responseRate", "responseRate"))
            ssn.addField(SimplePrimitiveNode("samplingErrorEstimates", "samplingErrorEstimates"))
            ssn.addField(SimplePrimitiveNode("otherDataAppraisal", "otherDataAppraisal"))
            ssn.addField(SimpleCompoundNode("socialScienceNotes", htSocialScienceNotes))
            ssn.addField(MultipleCompoundNode("geographicalReferential", [htGeographicalReferential1,htGeographicalReferential2]))

            print(ssn.toJSON())
        </pre>
        """
        print("# Je crée la taille de l'échantillon")
        print('htTargetSampleSize = dict()')
        print('htTargetSampleSize["targetSampleActualSize"] = SimplePrimitiveNode("targetSampleActualSize", "42")')
        print('htTargetSampleSize["targetSampleSizeFormula"] = SimplePrimitiveNode("targetSampleSizeFormula", "targetSampleSizeFormula")')
        print("")
        print('# Je crée notes du node')
        print('htSocialScienceNotes = dict()')
        print('htSocialScienceNotes["socialScienceNotesSubject"] = SimplePrimitiveNode("socialScienceNotesSubject", "socialScienceNotesSubject")')
        print('htSocialScienceNotes["socialScienceNotesText"] = SimplePrimitiveNode("socialScienceNotesText", "socialScienceNotesText")')
        print('htSocialScienceNotes["socialScienceNotesType"] = SimplePrimitiveNode("socialScienceNotesType", "socialScienceNotesType")')
        print("")
        print('# Je crée notes du node')
        print('htGeographicalReferential1 = dict()')
        print('htGeographicalReferential1["level"] = SimplePrimitiveNode("level", "geographicalReferentialLevel1")')
        print('htGeographicalReferential1["version"] = SimplePrimitiveNode("version", "geographicalReferentialVersion1")')
        print("")
        print('htGeographicalReferential2 = dict()')
        print('htGeographicalReferential2["level"] = SimplePrimitiveNode("level", "geographicalReferentialLevel2")')
        print('htGeographicalReferential2["version"] = SimplePrimitiveNode("version", "geographicalReferentialVersion2")')
        print("")
        print('# Je crée un node socialscience')
        print('ssn = SocialScienceNode()')
        print('ssn.addField(MultipleControlledVocabularyNode("unitOfAnalysis", ["Individual","Organization"]))')
        print('ssn.addField(MultiplePrimitiveNode("universe", ["universe1","universe2"]))')
        print('ssn.addField(SimpleControlledVocabularyNode("timeMethod", "Longitudinal"))')
        print('ssn.addField(SimplePrimitiveNode("timeMethodOther", "timeMethodOther"))')
        print('ssn.addField(SimplePrimitiveNode("dataCollector", "dataCollector"))')
        print('ssn.addField(SimplePrimitiveNode("collectorTraining", "collectorTraining"))')
        print('ssn.addField(SimplePrimitiveNode("frequencyOfDataCollection", "frequency"))')
        print('ssn.addField(SimpleControlledVocabularyNode("samplingProcedure", "Total universe/Complete enumeration"))')
        print('ssn.addField(SimplePrimitiveNode("samplingProcedureOther", "samplingProcedureOther"))')
        print('ssn.addField(SimpleCompoundNode("targetSampleSize", htTargetSampleSize))')
        print('ssn.addField(SimplePrimitiveNode("deviationsFromSampleDesign", "deviationsFromSampleDesign"))')
        print('ssn.addField(SimpleControlledVocabularyNode("collectionMode", "Interview"))')
        print('ssn.addField(SimplePrimitiveNode("collectionModeOther", "collectionModeOther"))')
        print('ssn.addField(SimplePrimitiveNode("researchInstrument", "researchInstrument"))')
        print('ssn.addField(SimplePrimitiveNode("dataCollectionSituation", "dataCollectionSituation"))')
        print('ssn.addField(SimplePrimitiveNode("actionsToMinimizeLoss", "actionsToMinimizeLoss"))')
        print('ssn.addField(SimplePrimitiveNode("controlOperations", "controlOperations"))')
        print('ssn.addField(SimplePrimitiveNode("weighting", "weighting"))')
        print('ssn.addField(SimplePrimitiveNode("cleaningOperations", "cleaningOperations"))')
        print('ssn.addField(SimplePrimitiveNode("datasetLevelErrorNotes", "datasetLevelErrorNotes"))')
        print('ssn.addField(SimplePrimitiveNode("responseRate", "responseRate"))')
        print('ssn.addField(SimplePrimitiveNode("samplingErrorEstimates", "samplingErrorEstimates"))')
        print('ssn.addField(SimplePrimitiveNode("otherDataAppraisal", "otherDataAppraisal"))')
        print('ssn.addField(SimpleCompoundNode("socialScienceNotes", htSocialScienceNotes))')
        print('ssn.addField(MultipleCompoundNode("geographicalReferential", [htGeographicalReferential1,htGeographicalReferential2]))')
        print("")
        print('print(ssn.toJSON())')

    def ShowMetadataMap():
        """!
        <c>ShowMetadataMap</c> détaille la carte des nodes du document de métadonnées
       
        <hr>
        <strong>Exemple de code : </strong>
        <pre>
            metadata
            |  citation
            |  |  accessToSources                       SimplePrimitiveNode
            |  |  alternativeTitle                      SimplePrimitiveNode
            |  |  alternativeURL                        SimplePrimitiveNode
            |  |  author                                MultipleCompoundNode
            |  |  |  authorAffiliation                  SimplePrimitiveNode
            |  |  |  authorIdentifier                   SimplePrimitiveNode
            |  |  |  authorIdentifierScheme             SimpleControlledVocabularyNode
            |  |  |  authorName                         SimplePrimitiveNode
            |  |  characteristicOfSources               SimplePrimitiveNode
            |  |  contributor                           MultipleCompoundNode
            |  |  |  contributorAffiliation             SimplePrimitiveNode
            |  |  |  contributorIdentifier              SimplePrimitiveNode
            |  |  |  contributorIdentifierScheme        SimpleControlledVocabularyNode
            |  |  |  contributorName                    SimplePrimitiveNode
            |  |  |  contributorType                    SimpleControlledVocabularyNode
            |  |  dataOrigin                            MultipleControlledVocabularyNode
            |  |  datasetContact                        MultipleCompoundNode
            |  |  |  datasetContactAffiliation          SimplePrimitiveNode
            |  |  |  datasetContactEmail                SimplePrimitiveNode
            |  |  |  datasetContactName                 SimplePrimitiveNode
            |  |  dataSources                           MultiplePrimitiveNode
            |  |  dateOfCollection                      MultipleCompoundNode
            |  |  |  dateOfCollectionStart              SimplePrimitiveNode
            |  |  |  dateOfCollectionEnd                SimplePrimitiveNode
            |  |  dateOfDeposit                         SimplePrimitiveNode
            |  |  depositor                             SimplePrimitiveNode
            |  |  distributionDate                      SimplePrimitiveNode
            |  |  distributor                           MultipleCompoundNode
            |  |  |  distributorAbbreviation            SimplePrimitiveNode
            |  |  |  distributorAffiliation             SimplePrimitiveNode
            |  |  |  distributorLogoURL                 SimplePrimitiveNode
            |  |  |  distributorName                    SimplePrimitiveNode
            |  |  |  distributorURL                     SimplePrimitiveNode
            |  |  dsDescription                         MultipleCompoundNode
            |  |  |  dsDescriptionDate                  SimplePrimitiveNode
            |  |  |  dsDescriptionValue                 SimplePrimitiveNode
            |  |  grantNumber                           MultipleCompoundNode
            |  |  |  grantNumberAgency                  SimplePrimitiveNode
            |  |  |  grantNumberValue                   SimplePrimitiveNode
            |  |  keyword                               MultipleCompoundNode
            |  |  |  keywordTermURI                     SimplePrimitiveNode
            |  |  |  keywordValue                       SimplePrimitiveNode
            |  |  |  keywordVocabulary                  SimplePrimitiveNode
            |  |  |  keywordVocabularyURI               SimplePrimitiveNode
            |  |  kindOfData                            MultipleControlledVocabularyNode
            |  |  kindOfDataOther                       MultiplePrimitiveNode
            |  |  language                              MultipleControlledVocabularyNode
            |  |  lifeCycleStep                         MultipleControlledVocabularyNode
            |  |  notesText                             SimplePrimitiveNode
            |  |  originOfSources                       SimplePrimitiveNode
            |  |  otherId                               MultipleCompoundNode
            |  |  |  otherIdAgency                      SimplePrimitiveNode
            |  |  |  otherIdValue                       SimplePrimitiveNode
            |  |  otherReferences                       MultiplePrimitiveNode
            |  |  producer                              MultipleCompoundNode
            |  |  |  producerAbbreviation               SimplePrimitiveNode
            |  |  |  producerAffiliation                SimplePrimitiveNode
            |  |  |  producerLogoURL                    SimplePrimitiveNode
            |  |  |  producerName                       SimplePrimitiveNode
            |  |  |  producerURL                        SimplePrimitiveNode
            |  |  productionDate                        SimplePrimitiveNode
            |  |  productionPlace                       SimplePrimitiveNode
            |  |  project                               SimpleCompoundNode
            |  |  |  projectAcronym                     SimplePrimitiveNode
            |  |  |  projectId                          SimplePrimitiveNode
            |  |  |  projectTask                        SimplePrimitiveNode
            |  |  |  projectTitle                       SimplePrimitiveNode
            |  |  |  projectURL                         SimplePrimitiveNode
            |  |  |  projectWorkPackage                 SimplePrimitiveNode
            |  |  publication                           MultipleCompoundNode
            |  |  |  publicationCitation                SimplePrimitiveNode
            |  |  |  publicationIDNumber                SimplePrimitiveNode
            |  |  |  publicationIDType                  SimpleControlledVocabularyNode
            |  |  |  publicationURL                     SimplePrimitiveNode
            |  |  relatedDataset                        MultipleCompoundNode
            |  |  |  relatedDatasetCitation             SimplePrimitiveNode
            |  |  |  relatedDatasetIDNumber             SimplePrimitiveNode
            |  |  |  relatedDatasetIDType               SimpleControlledVocabularyNode
            |  |  |  relatedDatasetURL                  SimplePrimitiveNode
            |  |  relatedMaterial                       MultiplePrimitiveNode
            |  |  series                                SimpleCompoundNode
            |  |  |  seriesInformation                  SimplePrimitiveNode
            |  |  |  seriesName                         SimplePrimitiveNode
            |  |  software                              MultipleCompoundNode
            |  |  |  softwareName                       SimplePrimitiveNode
            |  |  |  softwareVersion                    SimplePrimitiveNode
            |  |  subject                               MultipleControlledVocabularyNode
            |  |  subtitle                              SimplePrimitiveNode
            |  |  timePeriodCovered                     MultipleCompoundNode
            |  |  |  timePeriodCoveredStart             SimplePrimitiveNode
            |  |  |  timePeriodCoveredEnd               SimplePrimitiveNode
            |  |  title                                 SimplePrimitiveNode
            |  |  topicClassification                   MultipleCompoundNode
            |  |  |  topicClassValue                    SimplePrimitiveNode
            |  |  |  topicClassVocab                    SimplePrimitiveNode
            |  |  |  topicClassVocabURI                 SimplePrimitiveNode
            |  geospatial
            |  |  conformity                            MultipleCompoundNode
            |  |  |  degree                             SimpleControlledVocabularyNode
            |  |  |  specification                      MultiplePrimitiveNode
            |  |  geographicBoundingBox                 MultipleCompoundNode
            |  |  |  eastLongitude                      SimplePrimitiveNode
            |  |  |  northLongitude                     SimplePrimitiveNode
            |  |  |  southLongitude                     SimplePrimitiveNode
            |  |  |  westLongitude                      SimplePrimitiveNode
            |  |  geographicCoverage                    MultipleCompoundNode
            |  |  |  city                               SimplePrimitiveNode
            |  |  |  country                            SimpleControlledVocabularyNode
            |  |  |  otherGeographicCoverage            SimplePrimitiveNode
            |  |  |  state                              SimplePrimitiveNode
            |  |  geographicUnit                        MultiplePrimitiveNode
            |  |  qualityValidity                       MultipleCompoundNode
            |  |  |  lineage                            MultiplePrimitiveNode
            |  |  |  spatialResolution                  MultiplePrimitiveNode
            |  socialscience
            |  |  actionsToMinimizeLoss                 SimplePrimitiveNode
            |  |  cleaningOperations                    SimplePrimitiveNode
            |  |  collectionMode                        SimpleControlledVocabularyNode
            |  |  collectionModeOther                   SimplePrimitiveNode
            |  |  collectorTraining                     SimplePrimitiveNode
            |  |  controlOperations                     SimplePrimitiveNode
            |  |  dataCollectionSituation               SimplePrimitiveNode
            |  |  dataCollector                         SimplePrimitiveNode
            |  |  datasetLevelErrorNotes                SimplePrimitiveNode
            |  |  deviationsFromSampleDesign            SimplePrimitiveNode
            |  |  frequencyOfDataCollection             SimplePrimitiveNode
            |  |  geographicalReferential               MultipleCompoundNode
            |  |  |  level                              SimplePrimitiveNode
            |  |  |  version                            SimplePrimitiveNode
            |  |  otherDataAppraisal                    SimplePrimitiveNode
            |  |  researchInstrument                    SimplePrimitiveNode
            |  |  responseRate                          SimplePrimitiveNode
            |  |  samplingErrorEstimates                SimplePrimitiveNode
            |  |  samplingProcedure                     SimpleControlledVocabularyNode
            |  |  samplingProcedureOther                SimplePrimitiveNode
            |  |  socialScienceNotes                    SimpleCompoundNode
            |  |  |  socialScienceNotesSubject          SimplePrimitiveNode
            |  |  |  socialScienceNotesText             SimplePrimitiveNode
            |  |  |  socialScienceNotesType             SimplePrimitiveNode
            |  |  targetSampleSize                      SimpleCompoundNode
            |  |  |  targetSampleActualSize             SimplePrimitiveNode
            |  |  |  targetSampleSizeFormula            SimplePrimitiveNode
            |  |  timeMethod                            SimpleControlledVocabularyNode
            |  |  timeMethodOther                       SimplePrimitiveNode
            |  |  unitOfAnalysis                        MultipleControlledVocabularyNode
            |  |  unitOfAnalysisOther                   SimplePrimitiveNode
            |  |  universe                              MultiplePrimitiveNode
            |  |  weighting                             SimplePrimitiveNode
            |  biomedical
            |  |  studyAssayCellType                    MultiplePrimitiveNode
            |  |  studyAssayMeasurementType             MultipleControlledVocabularyNode
            |  |  studyAssayOrganism                    MultipleControlledVocabularyNode
            |  |  studyAssayOtherOrganism               MultiplePrimitiveNode
            |  |  studyAssayOtherMeasurmentType         MultiplePrimitiveNode
            |  |  studyAssayPlatform                    MultipleControlledVocabularyNode
            |  |  studyAssayPlatformOther               MultiplePrimitiveNode
            |  |  studyAssayTechnologyType              MultipleControlledVocabularyNode
            |  |  studyAssayTechnologyTypeOther         MultiplePrimitiveNode
            |  |  studyDesignType                       MultipleControlledVocabularyNode
            |  |  studyDesignTypeOther                  MultiplePrimitiveNode
            |  |  studyFactorType                       MultipleControlledVocabularyNode
            |  |  studyFactorTypeOther                  MultiplePrimitiveNode
            |  |  studySampleType                       MultiplePrimitiveNode
            |  |  studyProtocolType                     MultiplePrimitiveNode
            |  journal
            |  |  journalArticleType                    SimpleControlledVocabularyNode
            |  |  journalVolumeIssue                    MultipleCompoundNode
            |  |  |  journalIssue                       SimplePrimitiveNode
            |  |  |  journalPubDate                     SimplePrimitiveNode
            |  |  |  journalVolume                      SimplePrimitiveNode
            |  Derived-text
            |  |  source                                MultipleCompoundNode
            |  |  |  ageOfSource                        MultiplePrimitiveNode
            |  |  |  citations                          MultiplePrimitiveNode
            |  |  |  experimentNumber                   MultiplePrimitiveNode
            |  |  |  typeOfSource                       MultiplePrimitiveNode
            |  semantics
            |  |  bugDatabase                           SimplePrimitiveNode
            |  |  designedForOntologyTask               MultipleControlledVocabularyNode
            |  |  endpoint                              SimplePrimitiveNode
            |  |  hasFormalityLevel                     SimpleControlledVocabularyNode
            |  |  hasOntologyLanguage                   MultipleControlledVocabularyNode
            |  |  knownUsage                            SimplePrimitiveNode
            |  |  imports                               MultiplePrimitiveNode
            |  |  resourceVersion                       SimpleCompoundNode
            |  |  |  changes                            SimplePrimitiveNode
            |  |  |  modificationDate                   SimplePrimitiveNode
            |  |  |  priorVersion                       MultiplePrimitiveNode
            |  |  |  versionInfo                        SimplePrimitiveNode
            |  |  |  versionStatus                      MultipleControlledVocabularyNode
            |  |  typeOfSR                              SimpleControlledVocabularyNode
            |  |  URI                                   MultiplePrimitiveNode
        </pre>
        """
        print("metadata") 
        print("|  citation") 
        print("|  |  accessToSources                       SimplePrimitiveNode") 
        print("|  |  alternativeTitle                      SimplePrimitiveNode") 
        print("|  |  alternativeURL                        SimplePrimitiveNode") 
        print("|  |  author                                MultipleCompoundNode") 
        print("|  |  |  authorAffiliation                  SimplePrimitiveNode") 
        print("|  |  |  authorIdentifier                   SimplePrimitiveNode") 
        print("|  |  |  authorIdentifierScheme             SimpleControlledVocabularyNode") 
        print("|  |  |  authorName                         SimplePrimitiveNode") 
        print("|  |  characteristicOfSources               SimplePrimitiveNode") 
        print("|  |  contributor                           MultipleCompoundNode") 
        print("|  |  |  contributorAffiliation             SimplePrimitiveNode") 
        print("|  |  |  contributorIdentifier              SimplePrimitiveNode") 
        print("|  |  |  contributorIdentifierScheme        SimpleControlledVocabularyNode") 
        print("|  |  |  contributorName                    SimplePrimitiveNode") 
        print("|  |  |  contributorType                    SimpleControlledVocabularyNode") 
        print("|  |  dataOrigin                            MultipleControlledVocabularyNode") 
        print("|  |  datasetContact                        MultipleCompoundNode") 
        print("|  |  |  datasetContactAffiliation          SimplePrimitiveNode") 
        print("|  |  |  datasetContactEmail                SimplePrimitiveNode") 
        print("|  |  |  datasetContactName                 SimplePrimitiveNode") 
        print("|  |  dataSources                           MultiplePrimitiveNode") 
        print("|  |  dateOfCollection                      MultipleCompoundNode") 
        print("|  |  |  dateOfCollectionStart              SimplePrimitiveNode") 
        print("|  |  |  dateOfCollectionEnd                SimplePrimitiveNode") 
        print("|  |  dateOfDeposit                         SimplePrimitiveNode") 
        print("|  |  depositor                             SimplePrimitiveNode") 
        print("|  |  distributionDate                      SimplePrimitiveNode") 
        print("|  |  distributor                           MultipleCompoundNode") 
        print("|  |  |  distributorAbbreviation            SimplePrimitiveNode") 
        print("|  |  |  distributorAffiliation             SimplePrimitiveNode") 
        print("|  |  |  distributorLogoURL                 SimplePrimitiveNode") 
        print("|  |  |  distributorName                    SimplePrimitiveNode") 
        print("|  |  |  distributorURL                     SimplePrimitiveNode") 
        print("|  |  dsDescription                         MultipleCompoundNode") 
        print("|  |  |  dsDescriptionDate                  SimplePrimitiveNode") 
        print("|  |  |  dsDescriptionValue                 SimplePrimitiveNode") 
        print("|  |  grantNumber                           MultipleCompoundNode") 
        print("|  |  |  grantNumberAgency                  SimplePrimitiveNode") 
        print("|  |  |  grantNumberValue                   SimplePrimitiveNode") 
        print("|  |  keyword                               MultipleCompoundNode") 
        print("|  |  |  keywordTermURI                     SimplePrimitiveNode") 
        print("|  |  |  keywordValue                       SimplePrimitiveNode") 
        print("|  |  |  keywordVocabulary                  SimplePrimitiveNode") 
        print("|  |  |  keywordVocabularyURI               SimplePrimitiveNode") 
        print("|  |  kindOfData                            MultipleControlledVocabularyNode") 
        print("|  |  kindOfDataOther                       MultiplePrimitiveNode") 
        print("|  |  language                              MultipleControlledVocabularyNode") 
        print("|  |  lifeCycleStep                         MultipleControlledVocabularyNode") 
        print("|  |  notesText                             SimplePrimitiveNode") 
        print("|  |  originOfSources                       SimplePrimitiveNode") 
        print("|  |  otherId                               MultipleCompoundNode") 
        print("|  |  |  otherIdAgency                      SimplePrimitiveNode") 
        print("|  |  |  otherIdValue                       SimplePrimitiveNode") 
        print("|  |  otherReferences                       MultiplePrimitiveNode") 
        print("|  |  producer                              MultipleCompoundNode") 
        print("|  |  |  producerAbbreviation               SimplePrimitiveNode") 
        print("|  |  |  producerAffiliation                SimplePrimitiveNode") 
        print("|  |  |  producerLogoURL                    SimplePrimitiveNode") 
        print("|  |  |  producerName                       SimplePrimitiveNode") 
        print("|  |  |  producerURL                        SimplePrimitiveNode") 
        print("|  |  productionDate                        SimplePrimitiveNode") 
        print("|  |  productionPlace                       SimplePrimitiveNode") 
        print("|  |  project                               SimpleCompoundNode") 
        print("|  |  |  projectAcronym                     SimplePrimitiveNode") 
        print("|  |  |  projectId                          SimplePrimitiveNode") 
        print("|  |  |  projectTask                        SimplePrimitiveNode") 
        print("|  |  |  projectTitle                       SimplePrimitiveNode") 
        print("|  |  |  projectURL                         SimplePrimitiveNode") 
        print("|  |  |  projectWorkPackage                 SimplePrimitiveNode") 
        print("|  |  publication                           MultipleCompoundNode") 
        print("|  |  |  publicationCitation                SimplePrimitiveNode") 
        print("|  |  |  publicationIDNumber                SimplePrimitiveNode") 
        print("|  |  |  publicationIDType                  SimpleControlledVocabularyNode") 
        print("|  |  |  publicationURL                     SimplePrimitiveNode") 
        print("|  |  relatedDataset                        MultipleCompoundNode") 
        print("|  |  |  relatedDatasetCitation             SimplePrimitiveNode") 
        print("|  |  |  relatedDatasetIDNumber             SimplePrimitiveNode") 
        print("|  |  |  relatedDatasetIDType               SimpleControlledVocabularyNode") 
        print("|  |  |  relatedDatasetURL                  SimplePrimitiveNode") 
        print("|  |  relatedMaterial                       MultiplePrimitiveNode") 
        print("|  |  series                                SimpleCompoundNode") 
        print("|  |  |  seriesInformation                  SimplePrimitiveNode") 
        print("|  |  |  seriesName                         SimplePrimitiveNode") 
        print("|  |  software                              MultipleCompoundNode") 
        print("|  |  |  softwareName                       SimplePrimitiveNode") 
        print("|  |  |  softwareVersion                    SimplePrimitiveNode") 
        print("|  |  subject                               MultipleControlledVocabularyNode") 
        print("|  |  subtitle                              SimplePrimitiveNode") 
        print("|  |  timePeriodCovered                     MultipleCompoundNode") 
        print("|  |  |  timePeriodCoveredStart             SimplePrimitiveNode") 
        print("|  |  |  timePeriodCoveredEnd               SimplePrimitiveNode") 
        print("|  |  title                                 SimplePrimitiveNode") 
        print("|  |  topicClassification                   MultipleCompoundNode") 
        print("|  |  |  topicClassValue                    SimplePrimitiveNode") 
        print("|  |  |  topicClassVocab                    SimplePrimitiveNode") 
        print("|  |  |  topicClassVocabURI                 SimplePrimitiveNode") 
        print("|  geospatial") 
        print("|  |  conformity                            MultipleCompoundNode") 
        print("|  |  |  degree                             SimpleControlledVocabularyNode") 
        print("|  |  |  specification                      MultiplePrimitiveNode") 
        print("|  |  geographicBoundingBox                 MultipleCompoundNode") 
        print("|  |  |  eastLongitude                      SimplePrimitiveNode") 
        print("|  |  |  northLongitude                     SimplePrimitiveNode") 
        print("|  |  |  southLongitude                     SimplePrimitiveNode") 
        print("|  |  |  westLongitude                      SimplePrimitiveNode") 
        print("|  |  geographicCoverage                    MultipleCompoundNode") 
        print("|  |  |  city                               SimplePrimitiveNode") 
        print("|  |  |  country                            SimpleControlledVocabularyNode") 
        print("|  |  |  otherGeographicCoverage            SimplePrimitiveNode") 
        print("|  |  |  state                              SimplePrimitiveNode") 
        print("|  |  geographicUnit                        MultiplePrimitiveNode") 
        print("|  |  qualityValidity                       MultipleCompoundNode") 
        print("|  |  |  lineage                            MultiplePrimitiveNode") 
        print("|  |  |  spatialResolution                  MultiplePrimitiveNode") 
        print("|  socialscience") 
        print("|  |  actionsToMinimizeLoss                 SimplePrimitiveNode") 
        print("|  |  cleaningOperations                    SimplePrimitiveNode") 
        print("|  |  collectionMode                        SimpleControlledVocabularyNode") 
        print("|  |  collectionModeOther                   SimplePrimitiveNode") 
        print("|  |  collectorTraining                     SimplePrimitiveNode") 
        print("|  |  controlOperations                     SimplePrimitiveNode") 
        print("|  |  dataCollectionSituation               SimplePrimitiveNode") 
        print("|  |  dataCollector                         SimplePrimitiveNode") 
        print("|  |  datasetLevelErrorNotes                SimplePrimitiveNode") 
        print("|  |  deviationsFromSampleDesign            SimplePrimitiveNode") 
        print("|  |  frequencyOfDataCollection             SimplePrimitiveNode") 
        print("|  |  geographicalReferential               MultipleCompoundNode") 
        print("|  |  |  level                              SimplePrimitiveNode") 
        print("|  |  |  version                            SimplePrimitiveNode") 
        print("|  |  otherDataAppraisal                    SimplePrimitiveNode") 
        print("|  |  researchInstrument                    SimplePrimitiveNode") 
        print("|  |  responseRate                          SimplePrimitiveNode") 
        print("|  |  samplingErrorEstimates                SimplePrimitiveNode") 
        print("|  |  samplingProcedure                     SimpleControlledVocabularyNode") 
        print("|  |  samplingProcedureOther                SimplePrimitiveNode") 
        print("|  |  socialScienceNotes                    SimpleCompoundNode") 
        print("|  |  |  socialScienceNotesSubject          SimplePrimitiveNode") 
        print("|  |  |  socialScienceNotesText             SimplePrimitiveNode") 
        print("|  |  |  socialScienceNotesType             SimplePrimitiveNode") 
        print("|  |  targetSampleSize                      SimpleCompoundNode") 
        print("|  |  |  targetSampleActualSize             SimplePrimitiveNode") 
        print("|  |  |  targetSampleSizeFormula            SimplePrimitiveNode") 
        print("|  |  timeMethod                            SimpleControlledVocabularyNode") 
        print("|  |  timeMethodOther                       SimplePrimitiveNode") 
        print("|  |  unitOfAnalysis                        MultipleControlledVocabularyNode") 
        print("|  |  unitOfAnalysisOther                   SimplePrimitiveNode") 
        print("|  |  universe                              MultiplePrimitiveNode") 
        print("|  |  weighting                             SimplePrimitiveNode") 
        print("|  biomedical") 
        print("|  |  studyAssayCellType                    MultiplePrimitiveNode") 
        print("|  |  studyAssayMeasurementType             MultipleControlledVocabularyNode") 
        print("|  |  studyAssayOrganism                    MultipleControlledVocabularyNode") 
        print("|  |  studyAssayOtherOrganism               MultiplePrimitiveNode") 
        print("|  |  studyAssayOtherMeasurmentType         MultiplePrimitiveNode") 
        print("|  |  studyAssayPlatform                    MultipleControlledVocabularyNode") 
        print("|  |  studyAssayPlatformOther               MultiplePrimitiveNode") 
        print("|  |  studyAssayTechnologyType              MultipleControlledVocabularyNode") 
        print("|  |  studyAssayTechnologyTypeOther         MultiplePrimitiveNode") 
        print("|  |  studyDesignType                       MultipleControlledVocabularyNode") 
        print("|  |  studyDesignTypeOther                  MultiplePrimitiveNode") 
        print("|  |  studyFactorType                       MultipleControlledVocabularyNode") 
        print("|  |  studyFactorTypeOther                  MultiplePrimitiveNode") 
        print("|  |  studySampleType                       MultiplePrimitiveNode") 
        print("|  |  studyProtocolType                     MultiplePrimitiveNode") 
        print("|  journal") 
        print("|  |  journalArticleType                    SimpleControlledVocabularyNode") 
        print("|  |  journalVolumeIssue                    MultipleCompoundNode") 
        print("|  |  |  journalIssue                       SimplePrimitiveNode") 
        print("|  |  |  journalPubDate                     SimplePrimitiveNode") 
        print("|  |  |  journalVolume                      SimplePrimitiveNode") 
        print("|  Derived-text") 
        print("|  |  source                                MultipleCompoundNode") 
        print("|  |  |  ageOfSource                        MultiplePrimitiveNode") 
        print("|  |  |  citations                          MultiplePrimitiveNode") 
        print("|  |  |  experimentNumber                   MultiplePrimitiveNode") 
        print("|  |  |  typeOfSource                       MultiplePrimitiveNode") 
        print("|  semantics") 
        print("|  |  bugDatabase                           SimplePrimitiveNode") 
        print("|  |  designedForOntologyTask               MultipleControlledVocabularyNode") 
        print("|  |  endpoint                              SimplePrimitiveNode") 
        print("|  |  hasFormalityLevel                     SimpleControlledVocabularyNode") 
        print("|  |  hasOntologyLanguage                   MultipleControlledVocabularyNode") 
        print("|  |  knownUsage                            SimplePrimitiveNode") 
        print("|  |  imports                               MultiplePrimitiveNode") 
        print("|  |  resourceVersion                       SimpleCompoundNode") 
        print("|  |  |  changes                            SimplePrimitiveNode") 
        print("|  |  |  modificationDate                   SimplePrimitiveNode") 
        print("|  |  |  priorVersion                       MultiplePrimitiveNode") 
        print("|  |  |  versionInfo                        SimplePrimitiveNode") 
        print("|  |  |  versionStatus                      MultipleControlledVocabularyNode") 
        print("|  |  typeOfSR                              SimpleControlledVocabularyNode") 
        print("|  |  URI                                   MultiplePrimitiveNode")