#! /usr/bin/env python3
# coding: utf-8

import json
from SicpaOpenDataPython.Entity.CategoryNode import CategoryNode
from SicpaOpenDataPython.Entity.AuthorizedFields import AuthorizedFields
from SicpaOpenDataPython.Entity.RequiredFields import RequiredFields
from SicpaOpenDataPython.Entity.SimplePrimitiveNode import SimplePrimitiveNode
from SicpaOpenDataPython.Entity.SimpleControlledVocabularyNode import SimpleControlledVocabularyNode
from SicpaOpenDataPython.Entity.SimpleCompoundNode import SimpleCompoundNode
from SicpaOpenDataPython.Entity.MultiplePrimitiveNode import MultiplePrimitiveNode
from SicpaOpenDataPython.Entity.MultipleControlledVocabularyNode import MultipleControlledVocabularyNode
from SicpaOpenDataPython.Entity.MultipleCompoundNode import MultipleCompoundNode

class GeospatialNode(CategoryNode):
    """!
    Classe implémentant le node Geospatial
    @author Tom VINCENT
    @since Juillet 2022
    """

    #    ___ _______________  _______  __  ____________
    #   / _ /_  __/_  __/ _ \/  _/ _ )/ / / /_  __/ __/
    #  / __ |/ /   / / / , _// // _  / /_/ / / / _\ \  
    # /_/ |_/_/   /_/ /_/|_/___/____/\____/ /_/ /___/  
    #                                                  





    #   _________  _  _______________  __  ___________________  _____  ____
    #  / ___/ __ \/ |/ / __/_  __/ _ \/ / / / ___/_  __/ __/ / / / _ \/ __/
    # / /__/ /_/ /    /\ \  / / / , _/ /_/ / /__  / / / _// /_/ / , _/\ \  
    # \___/\____/_/|_/___/ /_/ /_/|_|\____/\___/ /_/ /___/\____/_/|_/___/  
    #   
    
    def __init__(self):
        """!
        Constructeur sans paramètre
          
        <hr>
        <strong>Exemple : </strong>
        <pre>
            gn = GeospatialNode()
        </pre>
        """
        super().__init__()
        self.initFields()
        self.setDisplayName("Geospatial Metadata")

        ##Cette propriété recense la liste des fields autorisés pour ce node
        self.__authorizedFields = AuthorizedFields.listAuthorizedFieldsForGeospatialNode()

        ##Cette propriété recense la liste des fields requis pour ce node
        self.__requiredFields = RequiredFields.listRequiredFieldsForGeospatialNode()





    #     ___  ____________________________  _____  ____
    #    / _ |/ ___/ ___/ __/ __/ __/ __/ / / / _ \/ __/
    #   / __ / /__/ /__/ _/_\ \_\ \/ _// /_/ / , _/\ \  
    #  /_/ |_\___/\___/___/___/___/___/\____/_/|_/___/  
    #





    #    __  _______________ ______  ___  ________
    #   /  |/  / __/_  __/ // / __ \/ _ \/ __/ __/
    #  / /|_/ / _/  / / / _  / /_/ / // / _/_\ \  
    # /_/  /_/___/ /_/ /_//_/\____/____/___/___/  
    #

    def addField(self, node:MultipleCompoundNode or MultipleControlledVocabularyNode or MultiplePrimitiveNode or SimpleCompoundNode or SimpleControlledVocabularyNode or SimplePrimitiveNode):
        """!
        <strong>addField</strong> est une méthode qui permet d'ajouter un noeud à la liste des fields du node
        @param node : node à ajouter
        @see SicpaOpenDataPython.Entity.SimplePrimitiveNode
        @see SicpaOpenDataPython.Entity.SimpleControlledVocabularyNode
        @see SicpaOpenDataPython.Entity.SimpleCompoundNode
        @see SicpaOpenDataPython.Entity.MultiplePrimitiveNode
        @see SicpaOpenDataPython.Entity.MultipleControlledVocabularyNode
        @see SicpaOpenDataPython.Entity.MultipleCompoundNode

        <hr>
        <strong>Exemple : </strong>
        <pre>
        </pre>
        """
        if isinstance(node, MultipleCompoundNode):
            if not node.isValid() or not node.getTypeName() in self.__authorizedFields or self.__authorizedFields.get(node.getTypeName()) != "MultipleCompoundNode":
                return False

            self.fields.append(node)
            return node in self.fields

        if isinstance(node, MultipleControlledVocabularyNode):
            if not node.isValid() or not node.getTypeName() in self.__authorizedFields or self.__authorizedFields.get(node.getTypeName()) != "MultipleControlledVocabularyNode":
                return False

            self.fields.append(node)
            return node in self.fields

        if isinstance(node, MultiplePrimitiveNode):
            if not node.isValid() or not node.getTypeName() in self.__authorizedFields or self.__authorizedFields.get(node.getTypeName()) != "MultiplePrimitiveNode":
                return False

            self.fields.append(node)
            return node in self.fields

        if isinstance(node, SimpleCompoundNode):
            if not node.isValid() or not node.getTypeName() in self.__authorizedFields or self.__authorizedFields.get(node.getTypeName()) != "SimpleCompoundNode":
                return False

            self.fields.append(node)
            return node in self.fields

        if isinstance(node, SimpleControlledVocabularyNode):
            if not node.isValid() or not node.getTypeName() in self.__authorizedFields or self.__authorizedFields.get(node.getTypeName()) != "SimpleControlledVocabularyNode":
                return False

            self.fields.append(node)
            return node in self.fields

        if isinstance(node, SimplePrimitiveNode):
            if not node.isValid() or not node.getTypeName() in self.__authorizedFields or self.__authorizedFields.get(node.getTypeName()) != "SimplePrimitiveNode":
                return False

            self.fields.append(node)
            return node in self.fields

    def isValid(self):
        """!
        <strong>isValid</strong> est une méthode qui permet de s'assurer de la validité du node
        @return true si le node est valide, false sinon

        <hr>
        <strong>Exemple : </strong>
        <pre>
            if gn.isValid():
                ...
        </pre>
        """
        isRequiredPresent = True
        isPresentAuthorized = True

        for key in self.__requiredFields.keys():
            if not key in self.fieldNames():
                isRequiredPresent = False
                break

        for field in self.fields:
            if not field.getTypeName() in self.__authorizedFields:
                isPresentAuthorized = False
                break

            if not self.__authorizedFields.get(field.getTypeName()) in field.__class__.__name__:
                isPresentAuthorized = False
                break

        return self.getDisplayName() == "Geospatial Metadata" and isRequiredPresent and isPresentAuthorized

    def toDict(self):
        """!
        <strong>toDict</strong> est une méthode qui permet d'obtenir le node sous forme d'un tableau
        @return la représentation du node sous forme de chaine JSON
        
        <hr>
        <strong>Exemple : </strong>
        <pre>
            array = self.toDict() 
        </pre>
        """
        subNodes = []
        for node in self.getFields():
            subNodes.append(node.toDict())
        return dict([
                        ('displayName',    self.getDisplayName()  ),
                        ('fields',         subNodes               )
                    ])

    def toJSON(self, prettyPrint=True):
        """!
        <strong>toJSON</strong> est une méthode qui permet d'obtenir une représentation JSON du node
        @param  prettyPrint : indique si l'on doit adapter la chaine JSON à la lecture par l'humain
        @return la représentation du node sous forme de chaine JSON
        
        <hr>
        <strong>Exemple : </strong>
        <pre>
            json = gn.toJSON() 
        </pre>
        """
        if not self.isValid():
            return ""

        if prettyPrint:
            return json.dumps(self.toDict(), indent=4)
        else:
            return json.dumps(self.toDict())

    def toString(self):
        """!
        <strong>toString</strong> est une méthode qui permet d'obtenir une représentation textuelle du node (JSON minifié)
        @return la représentation du node sous forme de chaine de caractère
        
        <hr>
        <strong>Exemple : </strong>
        <pre>
            json = gn.toString() 
        </pre>
        """
        return self.toJSON(False)





    #    __  _______________ ______  ___  ________    _____________ ______________  __  __________
    #   /  |/  / __/_ __/  // / __ \/ _ \/ __/ __/   / __/_  __/ _ /_  __/  _/ __ \/ / / / __/ __/
    #  / /|_/ / _/  / / / _  / /_/ / // / _/_\ \    _\ \  / / / __ |/ / _/ // /_/ / /_/ / _/_\ \  
    # /_/  /_/___/ /_/ /_//_/\____/____/___/___/   /___/ /_/ /_/ |_/_/ /___/\___\_\____/___/___/  
    #

                
