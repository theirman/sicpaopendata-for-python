#! /usr/bin/env python3
# coding: utf-8

import json
from SicpaOpenDataPython.Entity.CategoryNode import CategoryNode
from SicpaOpenDataPython.Entity.BiomedicalNode import BiomedicalNode
from SicpaOpenDataPython.Entity.CitationNode import CitationNode
from SicpaOpenDataPython.Entity.DerivedTextNode import DerivedTextNode
from SicpaOpenDataPython.Entity.GeospatialNode import GeospatialNode
from SicpaOpenDataPython.Entity.JournalNode import JournalNode
from SicpaOpenDataPython.Entity.SemanticsNode import SemanticsNode
from SicpaOpenDataPython.Entity.SocialScienceNode import SocialScienceNode
from SicpaOpenDataPython.Entity.AuthorizedFields import AuthorizedFields
from SicpaOpenDataPython.Entity.RequiredFields import RequiredFields
from SicpaOpenDataPython.Entity.SimplePrimitiveNode import SimplePrimitiveNode
from SicpaOpenDataPython.Entity.SimpleControlledVocabularyNode import SimpleControlledVocabularyNode
from SicpaOpenDataPython.Entity.SimpleCompoundNode import SimpleCompoundNode
from SicpaOpenDataPython.Entity.MultiplePrimitiveNode import MultiplePrimitiveNode
from SicpaOpenDataPython.Entity.MultipleControlledVocabularyNode import MultipleControlledVocabularyNode
from SicpaOpenDataPython.Entity.MultipleCompoundNode import MultipleCompoundNode

class MetadataDocument():
    """!
    Classe implémentant le node citation
    @author Tom VINCENT
    @since Juillet 2022
    """

    #    ___ _______________  _______  __  ____________
    #   / _ /_  __/_  __/ _ \/  _/ _ )/ / / /_  __/ __/
    #  / __ |/ /   / / / , _// // _  / /_/ / / / _\ \  
    # /_/ |_/_/   /_/ /_/|_/___/____/\____/ /_/ /___/  
    #                                                  





    #   _________  _  _______________  __  ___________________  _____  ____
    #  / ___/ __ \/ |/ / __/_  __/ _ \/ / / / ___/_  __/ __/ / / / _ \/ __/
    # / /__/ /_/ /    /\ \  / / / , _/ /_/ / /__  / / / _// /_/ / , _/\ \  
    # \___/\____/_/|_/___/ /_/ /_/|_|\____/\___/ /_/ /___/\____/_/|_/___/  
    #   
    
    def __init__(self, license="NONE", termsOfUse="<img src=\"https://www.etalab.gouv.fr/wp-content/uploads/2011/10/licence-ouverte-open-licence.gif\" alt=\"Licence Ouverte\" height=\"100\">" +
                                "<a href=\"https://www.etalab.gouv.fr/licence-ouverte-open-licence\">Licence Ouverte / Open Licence Version 2.0</a> " +
                                "compatible CC BY"):
        """!
        Constructeur
        @param license : valeur à enregistrer dans l'attribut <strong>license</strong>
        @param termsOfUse : valeur à enregistrer dans l'attribut <strong>termsOfUse</strong>
          
        <hr>
        <strong>Exemple : </strong>
        <pre>
            md = MetadataDocument("license", "termsOfUse")
        </pre>
        """
        self.setLicense(license)
        self.setTermsOfUse(termsOfUse)

        ##Cette propriété permet de gérer la liste des category nodes du document
        self.metadataBlocks = dict()
        




    #     ___  ____________________________  _____  ____
    #    / _ |/ ___/ ___/ __/ __/ __/ __/ / / / _ \/ __/
    #   / __ / /__/ /__/ _/_\ \_\ \/ _// /_/ / , _/\ \  
    #  /_/ |_\___/\___/___/___/___/___/\____/_/|_/___/  
    #

    def getLicense(self):
        """!
        Permet d'obtenir la valeur de l'attribut <strong>license</strong> 
        @return valeur de l'attribut <strong>license</strong> 
        
        <hr>
        <strong>Exemple : </strong>
        <pre>
            license = md.getLicense()
        </pre>
        """
        return self.__license

    def getTermsOfUse(self):
        """!
        Permet d'obtenir la valeur de l'attribut <strong>termsOfUse</strong> 
        @return valeur de l'attribut <strong>termsOfUse</strong> 
        
        <hr>
        <strong>Exemple : </strong>
        <pre>
            termsOfUse = md.getTermsOfUse()
        </pre>
        """
        return self.__termsOfUse

    def getMetadataBlocks(self):
        """!
        Permet d'obtenir la valeur de l'attribut <strong>metadataBlocks</strong> 
        @return valeur de l'attribut <strong>metadataBlocks</strong> 
        
        <hr>
        <strong>Exemple : </strong>
        <pre>
            metadataBlocks = md.getMetadataBlocks()
        </pre>
        """
        return self.metadataBlocks

    def setLicense(self, license:str):
        """!
        Permet de mettre à jour la valeur de l'attribut <strong>license</strong> 
        @param license : valeur à enregistrer dans l'attribut <strong>license</strong> 
        
        <hr>
        <strong>Exemple : </strong>
        <pre>
            md.setLicense("License")
        </pre>
        """
        ##Cette propriété indique la licence du dataset
        self.__license = license

    def setTermsOfUse(self, termsOfUse:str):
        """!
        Permet de mettre à jour la valeur de l'attribut <strong>termsOfUse</strong> 
        @param license : valeur à enregistrer dans l'attribut <strong>termsOfUse</strong> 
        
        <hr>
        <strong>Exemple : </strong>
        <pre>
            md.setTermsOfUse("termsOfUse")
        </pre>
        """
        ##Cette propriété indique les termes d'utilisation du dataset
        self.__termsOfUse = termsOfUse





    #    __  _______________ ______  ___  ________
    #   /  |/  / __/_  __/ // / __ \/ _ \/ __/ __/
    #  / /|_/ / _/  / / / _  / /_/ / // / _/_\ \  
    # /_/  /_/___/ /_/ /_//_/\____/____/___/___/  
    #

    def addBiomedicalNode(self, biomedicalNode:BiomedicalNode):
        """!
        <strong>addBiomedicalNode</strong> est une méthode qui permet d'ajouter le node biomedical à la liste des category nodes
        @param biomedicalNode : la valeur du node biomedical

        <hr>
        <strong>Exemple : </strong>
        <pre>
        bn = BiomedicalNode()
        ...
        ...

        md.addBiomedicalNode(bn)
        </pre>
        """
        self.metadataBlocks.update({"biomedical" : biomedicalNode})

    def addCitationNode(self, citationNode:CitationNode):
        """!
        <strong>addCitationNode</strong> est une méthode qui permet d'ajouter le node citation à la liste des category nodes
        @param citationNode : la valeur du node citation

        <hr>
        <strong>Exemple : </strong>
        <pre>
        cn = CitationNode()
        ...
        ...

        md.addCitationNode(cn)
        </pre>
        """
        self.metadataBlocks.update({"citation" : citationNode})

    def addDerivedTextNode(self, derivedTextNode:DerivedTextNode):
        """!
        <strong>addDerivedTextNode</strong> est une méthode qui permet d'ajouter le node derivedText à la liste des category nodes
        @param derivedTextNode : la valeur du node derivedText

        <hr>
        <strong>Exemple : </strong>
        <pre>
        dtn = DerivedTextNode()
        ...
        ...

        md.addCitationNode(dtn)
        </pre>
        """
        self.metadataBlocks.update({"Derived-text" : derivedTextNode})

    def addGeospatialNode(self, geospatialNode:GeospatialNode):
        """!
        <strong>addGeospatialNode</strong> est une méthode qui permet d'ajouter le node geospatial à la liste des category nodes
        @param geospatialNode : la valeur du node geospatial

        <hr>
        <strong>Exemple : </strong>
        <pre>
        gn = GeospatialNode()
        ...
        ...

        md.addGeospatialNode(gn)
        </pre>
        """
        self.metadataBlocks.update({"geospatial" : geospatialNode})

    def addJournalNode(self, journalNode:JournalNode):
        """!
        <strong>addJournalNode</strong> est une méthode qui permet d'ajouter le node journal à la liste des category nodes
        @param journalNode : la valeur du node journal

        <hr>
        <strong>Exemple : </strong>
        <pre>
        jn = JournalNode()
        ...
        ...

        md.addJournalNode(jn)
        </pre>
        """
        self.metadataBlocks.update({"journal" : journalNode})

    def addSemanticsNode(self, semanticsNode:SemanticsNode):
        """!
        <strong>addSemanticsNode</strong> est une méthode qui permet d'ajouter le node semantics à la liste des category nodes
        @param semanticsNode : la valeur du node semantics

        <hr>
        <strong>Exemple : </strong>
        <pre>
        sn = SemanticsNode()
        ...
        ...

        md.addSemanticsNode(sn)
        </pre>
        """
        self.metadataBlocks.update({"semantics" : semanticsNode})

    def addSocialScienceNode(self, socialScienceNode:SocialScienceNode):
        """!
        <strong>addSocialScienceNode</strong> est une méthode qui permet d'ajouter le node socialScience à la liste des category nodes
        @param socialScienceNode : la valeur du node socialScience

        <hr>
        <strong>Exemple : </strong>
        <pre>
        ssn = SocialScienceNode()
        ...
        ...

        md.addSocialScienceNode(ssn)
        </pre>
        """
        self.metadataBlocks.update({"socialscience" : socialScienceNode})

    def toDict(self):
        """!
        <strong>toDict</strong> est une méthode qui permet d'obtenir le node sous forme d'un tableau
        @return la représentation du node sous forme de chaine JSON
        
        <hr>
        <strong>Exemple : </strong>
        <pre>
            array = self.toDict() 
        </pre>
        """
        subNodes=dict()
        for categoryNode in self.getMetadataBlocks().keys():
            subNodes.update({categoryNode : (self.getMetadataBlocks().get(categoryNode).toDict())})
        
            document = dict([
                        ('license',         self.getLicense()        ),
                        ('termsOfUse',      self.getTermsOfUse()     ),
                        ('metadataBlocks',  subNodes )
                    ])

        return dict([("datasetVersion", document)])

    def toJSON(self, prettyPrint=True):
        """!
        <strong>toJSON</strong> est une méthode qui permet d'obtenir une représentation JSON du node
        @param  prettyPrint : indique si l'on doit adapter la chaine JSON à la lecture par l'humain
        @return la représentation du node sous forme de chaine JSON
        
        <hr>
        <strong>Exemple : </strong>
        <pre>
            json = md.toJSON() 
        </pre>
        """
        if prettyPrint:
            return json.dumps(self.toDict(), indent=4)
        else:
            return json.dumps(self.toDict())

    def toString(self):
        """!
        <strong>toString</strong> est une méthode qui permet d'obtenir une représentation textuelle du node (JSON minifié)
        @return la représentation du node sous forme de chaine de caractère
        
        <hr>
        <strong>Exemple : </strong>
        <pre>
            json = md.toString() 
        </pre>
        """
        return self.toJSON(False)