#! /usr/bin/env python3
# coding: utf-8

class CategoryNode():
    """!
    Classe implémentant les nodes catégorie
    @author Tom VINCENT
    @since Juillet 2022
    """

    #    ___ _______________  _______  __  ____________
    #   / _ /_  __/_  __/ _ \/  _/ _ )/ / / /_  __/ __/
    #  / __ |/ /   / / / , _// // _  / /_/ / / / _\ \  
    # /_/ |_/_/   /_/ /_/|_/___/____/\____/ /_/ /___/  
    #                                                  





    #   _________  _  _______________  __  ___________________  _____  ____
    #  / ___/ __ \/ |/ / __/_  __/ _ \/ / / / ___/_  __/ __/ / / / _ \/ __/
    # / /__/ /_/ /    /\ \  / / / , _/ /_/ / /__  / / / _// /_/ / , _/\ \  
    # \___/\____/_/|_/___/ /_/ /_/|_|\____/\___/ /_/ /___/\____/_/|_/___/  
    #          

    def __init__(self):
        """!
        Constructeur sans paramètre
          
        <hr>
        <strong>Exemple : </strong>
        <pre>
            bn = BaseNode()
        </pre>
        """
        self.setDisplayName("")
        self.initFields()





    #     ___  ____________________________  _____  ____
    #    / _ |/ ___/ ___/ __/ __/ __/ __/ / / / _ \/ __/
    #   / __ / /__/ /__/ _/_\ \_\ \/ _// /_/ / , _/\ \  
    #  /_/ |_\___/\___/___/___/___/___/\____/_/|_/___/  
    #

    def getDisplayName(self):
        """!
        Permet d'obtenir la valeur de l'attribut <strong>displayName</strong> 
        @return valeur de l'attribut <strong>displayName</strong>
        
        <hr>
        <strong>Exemple : </strong>
        <pre>
            displayName = cn.getDisplayname()
        </pre>
        """
        return self.__displayName

    def getFields(self):
        """!
        Permet d'obtenir la valeur de l'attribut <strong>fields</strong>
        @return valeur de l'attribut <strong>fields</strong>

        <hr>
        <strong>Exemple : </strong>
        <pre>
            fields = cn.getFields()
        </pre>
        """
        return self.fields

    def setDisplayName(self, displayName:str):
        """!
        Permet de mettre à jour la valeur de l'attribut <strong>displayName</strong>
        @param valeur de l'attribut <strong>displayName</strong>

        <hr>
        <strong>Exemple : </strong>
        <pre>
            cn.setDisplayName("displayName")
        </pre>
        """
        ##Cette propriété indique la valeur à afficher dans le fichier de métadonnées pour ce node
        self.__displayName = displayName





    #    __  _______________ ______  ___  ________
    #   /  |/  / __/_  __/ // / __ \/ _ \/ __/ __/
    #  / /|_/ / _/  / / / _  / /_/ / // / _/_\ \  
    # /_/  /_/___/ /_/ /_//_/\____/____/___/___/  
    #

    def clearFields(self):
        """!
        <strong>clearFields</strong> est une méthode qui permet de réinitialiser la liste des fields du node

        <hr>
        <strong>Exemple : </strong>
        <pre>
            cn.clearFields()
        </pre>
        """
        self.fields.clear()

    def containsField(self, fieldName:str):
        """!
        <strong>containsField</strong> est une méthode qui permet de réinitialiser la liste des fields du node
        @param fieldName : valeur à vérifier
        @return true si la valeur appartient à la liste des valeurs du node, false sinon

        <hr>
        <strong>Exemple : </strong>
        <pre>
            if cn.containsField("fieldName"):
                ...
        </pre>
        """
        return this.indexOfField(fieldName) >= 0

    def fieldNames(self):
        """!
        <strong>fieldNames</strong> est une méthode qui permet de récupérer la liste des noms des fields du node
        @return la liste des noms des fields

        <hr>
        <strong>Exemple : </strong>
        <pre>
            fnList = cn.fieldNames()
        </pre>
        """
        return [ node.getTypeName() for node in self.fields]

    def indexOfField(self, fieldName:str):
        """!
        <strong>indexOfField</strong> est une méthode qui permet de récupérer l'index d'un field
        @param fieldName : valeur à chercher
        @return index de la valeur

        <hr>
        <strong>Exemple : </strong>
        <pre>
            index = cn.indexOfField("fieldName")
        </pre>
        """
        for index in range(len(self.fields)):
            node = self.fields[index]
            
            if node.getTypeName() == fieldName:
                return index

        return -1

    def initFields(self):
        """!
        <strong>initFields</strong> est une méthode qui permet d'initialiser la liste des fields du node

        <hr>
        <strong>Exemple : </strong>
        <pre>
            cn.initFields()
        </pre>
        """
        ##Cette propriété permet de gérer la liste des valeurs pour le node
        self.fields = []

    def removeField(self, fieldName:str):
        """!
        <strong>removeField</strong> est une méthode qui permet de supprimer un field de la liste des fields
        @param fieldName : valeur à supprimer

        <hr>
        <strong>Exemple : </strong>
        <pre>
            cn.removeField("fieldName")
        </pre>
        """
        try:
            self.fields.remove(fieldName)
        except:
            pass

    def removeFieldAt(self, index:int):
        """!
        <strong>removeFieldAt</strong> est une méthode qui permet de supprimer le field situé à un index précis
        @param fieldName : index de la valeur à supprimer

        <hr>
        <strong>Exemple : </strong>
        <pre>
            cn.removeFieldAt(3)
        </pre>
        """
        try:
            del self.fields[index]
        except:
            pass

    def removeAllFields(self):
        """!
        <strong>removeAllFields</strong> est une méthode qui permet de supprimer tous les fields de la liste des fields

        <hr>
        <strong>Exemple : </strong>
        <pre>
            cn.removeAllFields()
        </pre>
        """
        self.fields.clear()





    #    __  _______________ ______  ___  ________    _____________ ______________  __  __________
    #   /  |/  / __/_ __/  // / __ \/ _ \/ __/ __/   / __/_  __/ _ /_  __/  _/ __ \/ / / / __/ __/
    #  / /|_/ / _/  / / / _  / /_/ / // / _/_\ \    _\ \  / / / __ |/ / _/ // /_/ / /_/ / _/_\ \  
    # /_/  /_/___/ /_/ /_//_/\____/____/___/___/   /___/ /_/ /_/ |_/_/ /___/\___\_\____/___/___/  
    #
