#! /usr/bin/env python3
# coding: utf-8

import json
from SicpaOpenDataPython.Entity.BaseNode import BaseNode
from SicpaOpenDataPython.Entity.AuthorizedFields import AuthorizedFields
from SicpaOpenDataPython.Entity.SimplePrimitiveNode import SimplePrimitiveNode
from SicpaOpenDataPython.Entity.SimpleControlledVocabularyNode import SimpleControlledVocabularyNode
from SicpaOpenDataPython.Entity.MultiplePrimitiveNode import MultiplePrimitiveNode
from SicpaOpenDataPython.Entity.MultipleControlledVocabularyNode import MultipleControlledVocabularyNode

class SimpleCompoundNode(BaseNode):
    """!
    Classe implémentant les nodes à valeur composée unique
    @author Tom VINCENT
    @since Juillet 2022
    """   
    
    #    ___ _______________  _______  __  ____________
    #   / _ /_  __/_  __/ _ \/  _/ _ )/ / / /_  __/ __/
    #  / __ |/ /   / / / , _// // _  / /_/ / / / _\ \  
    # /_/ |_/_/   /_/ /_/|_/___/____/\____/ /_/ /___/  
    #                                                  





    #   _________  _  _______________  __  ___________________  _____  ____
    #  / ___/ __ \/ |/ / __/_  __/ _ \/ / / / ___/_  __/ __/ / / / _ \/ __/
    # / /__/ /_/ /    /\ \  / / / , _/ /_/ / /__  / / / _// /_/ / , _/\ \  
    # \___/\____/_/|_/___/ /_/ /_/|_|\____/\___/ /_/ /___/\____/_/|_/___/  
    #

    def __init__(self, typeName="", value=dict([("",BaseNode())])):
        """!
        Constructeur
        @param typeName :   nom du node
        @param value    :   valeur du node
        @see SicpaOpenDataPython.Entity.SimplePrimitiveNode
        @see SicpaOpenDataPython.Entity.SimpleControlledVocabularyNode
        @see SicpaOpenDataPython.Entity.MultiplePrimitiveNode
        @see SicpaOpenDataPython.Entity.MultipleControlledVocabularyNode
        
        <hr>
        <strong>Exemple : </strong>
        <pre>
            values = dict([("name1","value1"),
                           ("name2","value2"),
                           ("name3","value3")])
            scn = SimpleCompoundNode("name", values) 
        </pre>
        """
        super().__init__()
        self.setMultiple(False)
        self.setTypeClass("compound")
        self.setTypeName(typeName)
        self.setValue(value)





    #     ___  ____________________________  _____  ____
    #    / _ |/ ___/ ___/ __/ __/ __/ __/ / / / _ \/ __/
    #   / __ / /__/ /__/ _/_\ \_\ \/ _// /_/ / , _/\ \  
    #  /_/ |_\___/\___/___/___/___/___/\____/_/|_/___/  
    #

    def getValue(self):
        """!
        Permet d'obtenir la valeur de l'attribut <strong>value</strong> 
        @return valeur de l'attribut <strong>value</strong> 
        
        <hr>
        <strong>Exemple : </strong>
        <pre>
            value = scn.getValue()
        </pre>
        """
        return self.__value
    
    def setValue(self, value:dict):
        """!
        Permet de mettre à jour la valeur de l'attribut <strong>value</strong>
        @param value : valeur à enregistrer dans l'attribut <strong>value</strong>
        
        <hr>
        <strong>Exemple : </strong>
        <pre>
            dictValue = dict([("field1", SimplePrimitiveNode("field1", "value1")),
                              ("field2", SimpleControlledVocabularyNode("field2", "value2")),
                              ("field3", SimplePrimitiveNode("field3", "value3"))])
            scn.setValue(dictValue) 
        </pre>
        """
        ##Cette propriété contient la valeur du node
        self.__value = value

        if not self.isValueValid():
            self.__value = dict()
            return
        
        if not self.areSubNodesValid():
            self.__value = dict()
            return




    #    __  _______________ ______  ___  ________
    #   /  |/  / __/_  __/ // / __ \/ _ \/ __/ __/
    #  / /|_/ / _/  / / / _  / /_/ / // / _/_\ \  
    # /_/  /_/___/ /_/ /_//_/\____/____/___/___/  
    #    

    def areSubNodesValid(self):
        """!
        <strong>areSubNodesValid(self)</strong> est une méthode qui permet de s'assurer de la validité de tous les subnodes
        @return true si les subnodes sont valides, false sinon 

        <hr>
        <strong>Exemple : </strong>
        <pre>
            if scn.areSubNodesValid():
                ...
        </pre>
        """
        for key in self.__value.keys():

            subnode = self.__value.get(key)
            
            if not subnode.getTypeName() in AuthorizedFields.listAuthorizedFieldsForCategoryNode(self.getTypeName()):
                return False

            if not AuthorizedFields.getNodeType(subnode.getTypeName()) in subnode.__class__.__name__:
                return False

        return True

    def isValid(self):
        """!
        <strong>isValid</strong> est une méthode qui permet de s'assurer de la validité du node
        @return true si le node est valide, false sinon
        
        <hr>
        <strong>Exemple : </strong>
        <pre> 
            if scn.isValid():
                ...
        </pre>
        """
        return isinstance(self.getMultiple(), bool) and not self.getMultiple()              \
           and isinstance(self.getTypeClass(), str) and self.getTypeClass() == "compound"   \
           and isinstance(self.getTypeName(), str)                                          \
           and isinstance(self.getValue(), dict) and self.isValueValid()                    \
           and self.areSubNodesValid()

    def isValueValid(self):
        """!
        <strong>isValueValid</strong> est une méthode qui permet de s'assurer de la validité de la valeur
        @return true si la valeur est valide, false sinon
        @see SicpaOpenDataPython.Entity.SimplePrimitiveNode
        @see SicpaOpenDataPython.Entity.SimpleControlledVocabularyNode
        @see SicpaOpenDataPython.Entity.MultiplePrimitiveNode
        @see SicpaOpenDataPython.Entity.MultipleControlledVocabularyNode

        <hr>
        <strong>Exemple : </strong>
        <pre> 
            if scn.isValueValid():
                ...
        </pre>
        """
        for key in self.__value.keys():

            node = self.__value.get(key)

            if isinstance(node, SimplePrimitiveNode)                         \
                    or isinstance(node, SimpleControlledVocabularyNode)      \
                    or isinstance(node, MultiplePrimitiveNode)               \
                    or isinstance(node, MultipleControlledVocabularyNode):   
                continue
            else:
                return False

        return True

    def toDict(self):
        """!
        <strong>toDict</strong> est une méthode qui permet d'obtenir le node sous forme d'un tableau
        @return la représentation du node sous forme de chaine JSON
        
        <hr>
        <strong>Exemple : </strong>
        <pre>
            array = self.toDict() 
        </pre>
        """
        subDict=dict()
        for typeName in self.getValue().keys():
            subDict.update({typeName : (self.getValue().get(typeName).toDict())})
        return dict([
                        ('multiple',    self.getMultiple()  ),
                        ('typeClass',   self.getTypeClass() ),
                        ('typeName',    self.getTypeName()  ),
                        ('value',       subDict     )
                    ])

    def toJSON(self, prettyPrint=True):
        """!
        <strong>toJSON</strong> est une méthode qui permet d'obtenir une représentation JSON du node
        @param  prettyPrint : indique si l'on doit adapter la chaine JSON à la lecture par l'humain
        @return la représentation du node sous forme de chaine JSON
        
        <hr>
        <strong>Exemple : </strong>
        <pre>
            json = scn.toJSON() 
        </pre>
        """
        if not self.isValid():
            return ""

        if prettyPrint:
            return json.dumps(self.toDict(), indent=4)
        else:
            return json.dumps(self.toDict())

    def toString(self):
        """!
        <strong>toString</strong> est une méthode qui permet d'obtenir une représentation textuelle du node (JSON minifié)
        @return la représentation du node sous forme de chaine de caractère
        
        <hr>
        <strong>Exemple : </strong>
        <pre>
            json = scn.toString() 
        </pre>
        """
        return self.toJSON(False)





    #    __  _______________ ______  ___  ________    _____________ ______________  __  __________
    #   /  |/  / __/_ __/  // / __ \/ _ \/ __/ __/   / __/_  __/ _ /_  __/  _/ __ \/ / / / __/ __/
    #  / /|_/ / _/  / / / _  / /_/ / // / _/_\ \    _\ \  / / / __ |/ / _/ // /_/ / /_/ / _/_\ \  
    # /_/  /_/___/ /_/ /_//_/\____/____/___/___/   /___/ /_/ /_/ |_/_/ /___/\___\_\____/___/___/  
    #