#! /usr/bin/env python3
# coding: utf-8

class RequiredFields:
    """! 
    Classe détaillant les valeurs requises par nodes
     @author Thierry HEIRMAN
     @since Juillet 2021
    """

    #    __  _______________ ______  ___  ________    _____________ ______________  __  __________
    #   /  |/  / __/_ __/  // / __ \/ _ \/ __/ __/   / __/_  __/ _ /_  __/  _/ __ \/ / / / __/ __/
    #  / /|_/ / _/  / / / _  / /_/ / // / _/_\ \    _\ \  / / / __ |/ / _/ // /_/ / /_/ / _/_\ \  
    # /_/  /_/___/ /_/ /_//_/\____/____/___/___/   /___/ /_/ /_/ |_/_/ /___/\___\_\____/___/___/  
    #


    @staticmethod
    def listRequiredFields(node:str = ""):
        """!
        <strong>listRequiredFields</strong> est une méthode qui permet de récupérer la liste de tous les fields requis soit pour un node donné, soit pour le document complet des métadonnées
        @param node : le node pour lequel lister les champs requis
        @return la liste des fields requis
        
        <hr>
        <strong>Exemple : </strong>
        <pre> 
            requiredFields = RequiredFields.listRequiredFields("citation") 
        </pre>
        """
    
        if(not node):
            requiredFields = dict()
            requiredFields = { **requiredFields, **RequiredFields.listRequiredFieldsForBiomedicalNode()     }
            requiredFields = { **requiredFields, **RequiredFields.listRequiredFieldsForCitationNode()       }
            requiredFields = { **requiredFields, **RequiredFields.listRequiredFieldsForDerivedTextNode()    }
            requiredFields = { **requiredFields, **RequiredFields.listRequiredFieldsForGeospatialNode()     }
            requiredFields = { **requiredFields, **RequiredFields.listRequiredFieldsForJournalNode()        }
            requiredFields = { **requiredFields, **RequiredFields.listRequiredFieldsForSemanticsNode()      }
            requiredFields = { **requiredFields, **RequiredFields.listRequiredFieldsForSocialScienceNode()  }
            return requiredFields        
        
        else:
            nodes = dict([
                            ("biomedical",      RequiredFields.listRequiredFieldsForBiomedicalNode()),
                            ("citation",        RequiredFields.listRequiredFieldsForCitationNode()),
                            ("Derived-text",    RequiredFields.listRequiredFieldsForDerivedTextNode()),
                            ("geospatial",      RequiredFields.listRequiredFieldsForGeospatialNode()),
                            ("journal",         RequiredFields.listRequiredFieldsForJournalNode()),
                            ("semantics",       RequiredFields.listRequiredFieldsForSemanticsNode()),
                            ("socialscience",   RequiredFields.listRequiredFieldsForSocialScienceNode()),
                         ])
            
            return nodes.get(node, RequiredFields.listRequiredFields())
    

    @staticmethod
    def listRequiredFieldsForBiomedicalNode():
        """!
        <strong>listRequiredFieldsForBiomedicalNode</strong> est une méthode qui permet de récupérer la liste de tous les fields requis dans le noeud "biomedical"
        @return la liste des fields requis
        
        <hr>
        <strong>Exemple : </strong>
        <pre> 
            requiredFields = RequiredFields.listRequiredFieldsForBiomedicalNode() 
        </pre>
        """
    
        return dict()
    

    @staticmethod
    def listRequiredFieldsForCitationNode():
        """!
        <strong>listRequiredFieldsForCitationNode</strong> est une méthode qui permet de récupérer la liste de tous les fields requis dans le noeud "citation"
        @return la liste des fields requis
        
        <hr>
        <strong>Exemple : </strong>
        <pre> 
            requiredFields = RequiredFields.listRequiredFieldsForCitationNode() 
        </pre>
        """
    
        return dict([
                        ("title",             "SimplePrimitiveNode"),
                        ("author",            "MultipleCompoundNode"),
                        ("datasetContact",    "MultipleCompoundNode"),
                        ("dsDescription",     "MultipleCompoundNode"),
                        ("subject",           "MultipleControlledVocabularyNode"),
                        ("kindOfData",        "MultipleControlledVocabularyNode"),
                    ])
    

    @staticmethod
    def listRequiredFieldsForDerivedTextNode():
        """!
        <strong>listRequiredFieldsForDerivedTextNode</strong> est une méthode qui permet de récupérer la liste de tous les fields requis dans le noeud "Derived-text"
        @return la liste des fields requis
        
        <hr>
        <strong>Exemple : </strong>
        <pre> 
            requiredFields = RequiredFields.listRequiredFieldsForDerivedTextNode() 
        </pre>
        """
    
        return dict()
    

    @staticmethod
    def listRequiredFieldsForGeospatialNode():
        """!
        <strong>listRequiredFieldsForGeospatialNode</strong> est une méthode qui permet de récupérer la liste de tous les fields requis dans le noeud "geospatial"
        @return la liste des fields requis
        
        <hr>
        <strong>Exemple : </strong>
        <pre> 
            requiredFields = RequiredFields.listRequiredFieldsForGeospatialNode() 
        </pre>
        """
    
        return dict()
    

    @staticmethod
    def listRequiredFieldsForJournalNode():
        """!
        <strong>listRequiredFieldsForJournalNode</strong> est une méthode qui permet de récupérer la liste de tous les fields requis dans le noeud "journal"
        @return la liste des fields requis
        
        <hr>
        <strong>Exemple : </strong>
        <pre> 
            requiredFields = RequiredFields.listRequiredFieldsForJournalNode() 
        </pre>
        """
    
        return dict()
    

    @staticmethod
    def listRequiredFieldsForSemanticsNode():
        """!
        <strong>listRequiredFieldsForSemanticsNode</strong> est une méthode qui permet de récupérer la liste de tous les fields requis dans le noeud "semantics"
        @return la liste des fields requis
        
        <hr>
        <strong>Exemple : </strong>
        <pre> 
             requiredFields = RequiredFields.listRequiredFieldsForSemanticsNode() 
        </pre>
        """
    
        return dict()
    

    @staticmethod
    def listRequiredFieldsForSocialScienceNode():
        """!
        <strong>listRequiredFieldsForSocialScienceNode</strong> est une méthode qui permet de récupérer la liste de tous les fields requis dans le noeud "socialscience"
        @return la liste des fields requis
        
        <hr>
        <strong>Exemple : </strong>
        <pre> 
            requiredFields = RequiredFields.listRequiredFieldsForSocialScienceNode() 
        </pre>
        """
    
        return dict()
    

