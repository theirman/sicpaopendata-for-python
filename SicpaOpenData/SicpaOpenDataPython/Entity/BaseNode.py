#!/usr/bin/env python3
# coding: utf-8

class BaseNode:
    """!
    Classe implémentant les nodes de base
    @author Thierry HEIRMAN
    @since Juillet 2021
    """

    #    ___ _______________  _______  __  ____________
    #   / _ /_  __/_  __/ _ \/  _/ _ )/ / / /_  __/ __/
    #  / __ |/ /   / / / , _// // _  / /_/ / / / _\ \  
    # /_/ |_/_/   /_/ /_/|_/___/____/\____/ /_/ /___/  
    #                                                  


    


    #   _________  _  _______________  __  ___________________  _____  ____
    #  / ___/ __ \/ |/ / __/_  __/ _ \/ / / / ___/_  __/ __/ / / / _ \/ __/
    # / /__/ /_/ /    /\ \  / / / , _/ /_/ / /__  / / / _// /_/ / , _/\ \  
    # \___/\____/_/|_/___/ /_/ /_/|_|\____/\___/ /_/ /___/\____/_/|_/___/  
    #                                                                          

    def __init__(self):
        """!
        Constructeur sans paramètre
        
        <hr>
        <strong>Exemple : </strong>
        <pre>
            bn = BaseNode()
        </pre>
        """
        self.setMultiple(False)
        self.setTypeClass("")
        self.setTypeName("")





    #     ___  ____________________________  _____  ____
    #    / _ |/ ___/ ___/ __/ __/ __/ __/ / / / _ \/ __/
    #   / __ / /__/ /__/ _/_\ \_\ \/ _// /_/ / , _/\ \  
    #  /_/ |_\___/\___/___/___/___/___/\____/_/|_/___/  
    #

    def getMultiple(self):
        """!
        Permet d'obtenir la valeur de l'attribut <strong>multiple</strong> 
        @return valeur de l'attribut <strong>multiple</strong>
        
        <hr>
        <strong>Exemple : </strong>
        <pre>
            multiple = bn.getMultiple()
        </pre>
        """
        return self.__multiple


    def getTypeClass(self):
        """!
        Permet d'obtenir la valeur de l'attribut <strong>typeClass</strong> 
        @return valeur de l'attribut <strong>typeClass</strong>
        
        <hr>
        <strong>Exemple : </strong>
        <pre>
            typeClass = bn.getTypeClass() 
        </pre>
        """
        return self.__typeClass


    def getTypeName(self):
        """!
        Permet d'obtenir la valeur de l'attribut <strong>typeName</strong> 
        @return valeur de l'attribut <strong>typeName</strong>
        
        <hr>
        <strong>Exemple : </strong>
        <pre>
            String typeName = bn.getTypeName()
        </pre>
        """
        return self.__typeName


    def setMultiple(self, multiple:bool):
        """!
        Permet de mettre à jour la valeur de l'attribut <strong>multiple</strong>
        @param multiple : valeur à enregistrer dans l'attribut <strong>multiple</strong>
        
        <hr>
        <strong>Exemple : </strong>
        <pre>
            bn.setMultiple(true) 
        </pre>
        """
        ##Cette propriété indique si l'on est en présence d'un noeud mono- ou multi-valeur(s)
        self.__multiple = multiple


    def setTypeClass(self, typeClass:str):
        """!
        Permet de mettre à jour la valeur de l'attribut <strong>typeClass</strong>
        @param typeClass : valeur à enregistrer dans l'attribut <strong>typeClass</strong>
        
        <hr>
        <strong>Exemple : </strong>
        <pre>
            bn.setTypeClass("compound") 
        </pre>
        """
        ##Cette propriété indique le type de la valeur attendue 
        self.__typeClass = typeClass


    def setTypeName(self, typeName:str):
        """!
        Permet de mettre à jour la valeur de l'attribut <strong>typeName</strong>
        @param typeName : valeur à enregistrer dans l'attribut <strong>typeName</strong>
        
        <hr>
        <strong>Exemple : </strong>
        <pre>
            bn.setTypeName("language") 
        </pre>
        """
        ##Cette propriété identifie la valeur
        self.__typeName = typeName





    #    __  _______________ ______  ___  ________
    #   /  |/  / __/_  __/ // / __ \/ _ \/ __/ __/
    #  / /|_/ / _/  / / / _  / /_/ / // / _/_\ \  
    # /_/  /_/___/ /_/ /_//_/\____/____/___/___/  
    #                                             





    #    __  _______________ ______  ___  ________    _____________ ______________  __  __________
    #   /  |/  / __/_ __/  // / __ \/ _ \/ __/ __/   / __/_  __/ _ /_  __/  _/ __ \/ / / / __/ __/
    #  / /|_/ / _/  / / / _  / /_/ / // / _/_\ \    _\ \  / / / __ |/ / _/ // /_/ / /_/ / _/_\ \  
    # /_/  /_/___/ /_/ /_//_/\____/____/___/___/   /___/ /_/ /_/ |_/_/ /___/\___\_\____/___/___/  
    #










