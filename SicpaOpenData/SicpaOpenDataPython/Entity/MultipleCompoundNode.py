#! /usr/bin/env python3
# coding: utf-8

import json
from SicpaOpenDataPython.Entity.BaseNode import BaseNode
from SicpaOpenDataPython.Entity.AuthorizedFields import AuthorizedFields
from SicpaOpenDataPython.Entity.SimplePrimitiveNode import SimplePrimitiveNode
from SicpaOpenDataPython.Entity.SimpleControlledVocabularyNode import SimpleControlledVocabularyNode
from SicpaOpenDataPython.Entity.MultiplePrimitiveNode import MultiplePrimitiveNode
from SicpaOpenDataPython.Entity.MultipleControlledVocabularyNode import MultipleControlledVocabularyNode

class MultipleCompoundNode(BaseNode):
    """!
    Classe implémentant les nodes à valeur composée multiples
    @author Tom VINCENT
    @since Juillet 2022
    """   
    
    #    ___ _______________  _______  __  ____________
    #   / _ /_  __/_  __/ _ \/  _/ _ )/ / / /_  __/ __/
    #  / __ |/ /   / / / , _// // _  / /_/ / / / _\ \  
    # /_/ |_/_/   /_/ /_/|_/___/____/\____/ /_/ /___/  
    #                                                  





    #   _________  _  _______________  __  ___________________  _____  ____
    #  / ___/ __ \/ |/ / __/_  __/ _ \/ / / / ___/_  __/ __/ / / / _ \/ __/
    # / /__/ /_/ /    /\ \  / / / , _/ /_/ / /__  / / / _// /_/ / , _/\ \  
    # \___/\____/_/|_/___/ /_/ /_/|_|\____/\___/ /_/ /___/\____/_/|_/___/  
    #

    def __init__(self, typeName="", value=[dict([("",BaseNode())])]):
        """!
        Constructeur
        @param typeName :   nom du node
        @param value    :   valeur du node
        
        <hr>
        <strong>Exemple : </strong>
        <pre>
            values = [dict([("value1", SimplePrimitiveNode()),
                            ("value2", SimpleControlledVocabularyNode()),
                            ("value3", MultiplePrimitiveNode()),
                            ("value4", MultipleControlledVocabularyNode())])]
            mcn = MultipleCompoundNode("name", values) 
        </pre>
        """
        super().__init__()
        self.setMultiple(True)
        self.setTypeClass("compound")
        self.setTypeName(typeName)

        ##Cette propriété contient la valeur du node
        self.__value=[]

        self.addValues(value)





    #     ___  ____________________________  _____  ____
    #    / _ |/ ___/ ___/ __/ __/ __/ __/ / / / _ \/ __/
    #   / __ / /__/ /__/ _/_\ \_\ \/ _// /_/ / , _/\ \  
    #  /_/ |_\___/\___/___/___/___/___/\____/_/|_/___/  
    #

    def getValue(self):
        """!
        Permet d'obtenir la valeur de l'attribut <strong>value</strong> 
        @return valeur de l'attribut <strong>value</strong> 
        
        <hr>
        <strong>Exemple : </strong>
        <pre>
            value = mcn.getValue()
        </pre>
        """
        return self.__value





    #    __  _______________ ______  ___  ________
    #   /  |/  / __/_  __/ // / __ \/ _ \/ __/ __/
    #  / /|_/ / _/  / / / _  / /_/ / // / _/_\ \  
    # /_/  /_/___/ /_/ /_//_/\____/____/___/___/  
    #

    def addValue(self, value:dict):
        """!
        <strong>addValue</strong> est une méthode qui permet d'ajouter une valeur à la liste des valeurs du node
        @param value : valeur à ajouter
        @see SicpaOpenDataPython.Entity.SimplePrimitiveNode
        @see SicpaOpenDataPython.Entity.SimpleControlledVocabularyNode
        @see SicpaOpenDataPython.Entity.MultiplePrimitiveNode
        @see SicpaOpenDataPython.Entity.MultipleControlledVocabularyNode

        <hr>
        <strong>Exemple : </strong>
        <pre>
            value = dict([("value1", SimplePrimitiveNode()),
                           ("value2", SimpleControlledVocabularyNode()),
                           ("value3", MultiplePrimitiveNode()),
                           ("value4", MultipleControlledVocabularyNode())])
            mcn.addValue(value)
        </pre>
        """
        if not (self.isValueValid(value)):
            return
        if not MultipleCompoundNode.areSubnodesValid(self.getTypeName(), value):
            return

        self.__value.append(value)

    def addValues(self, values:[dict]):
        """!
        <strong>addValues</strong> est une méthode qui permet d'ajouter une liste de valeurs à la liste des valeurs du node
        @param values : valeurs à ajouter
        @see SicpaOpenDataPython.Entity.SimplePrimitiveNode
        @see SicpaOpenDataPython.Entity.SimpleControlledVocabularyNode
        @see SicpaOpenDataPython.Entity.MultiplePrimitiveNode
        @see SicpaOpenDataPython.Entity.MultipleControlledVocabularyNode

        <hr>
        <strong>Exemple : </strong>
        <pre>
            value1 = dict([("name11", SimplePrimitiveNode("name11","value11")),
                           ("name12", MultipleControlledVocabularyNode("name12",["controlledValue121","controlledVvalue122"]))])

            value2 = dict([("name21", SimpleControlledVocabularyNode("name21","controlledValue21")),
                           ("name22", MultiplePrimitiveNode("name22",["value221","value222"]))])

            values = [value1,value2]
            mcn.addValues(values)
        </pre>
        """
        for value in values:
            if not isinstance(value, dict):
                continue
            self.addValue(value)

    def areSubNodesValid(self):
        """!
        <strong>areSubNodesValid</strong> est une méthode qui permet de s'assurer de la validité de tous les subnodes
        @return true si les subnodes sont valides, false sinon 

        <hr>
        <strong>Exemple : </strong>
        <pre>
            if mcn.areSubNodesValid():
                ...
        </pre>
        """
        for val in self.__value:
            for key in val.keys():
                
                subnode = val.get(key)
            
            if not subnode.getTypeName() in AuthorizedFields.listAuthorizedFieldsForCategoryNode(self.getTypeName()):
                return False

            if not AuthorizedFields.getNodeType(subnode.getTypeName()) in subnode.__class__.__name__:
                return False

        return True

    def clearValues(self):
        """!
        <strong>clearValues</strong> est une méthode qui permet de réinitialiser la liste des valeurs du node

        <hr>
        <strong>Exemple :  </strong>
        <pre>
            mcn.clearValues()
        </pre>
        """
        self.__value.clear()

    def containsValue(self, value:dict):
        """!
        <strong>containsValue</strong> est une méthode qui permet de vérifier si une valeur existe dans la liste des valeurs du node
        @param value : valeur à vérifier
        @return true si la valeur appartient à la liste de valeurs du node, false sinon

        <hr>
        <strong>Exemple :  </strong>
        <pre>
            if mcn.containsValue(value):
                ...
        </pre>
        """
        return self.indexOf(value) >= 0

    def getValueAt(self, index:int):
        """!
        <strong>getValueAt</strong> est une méthode qui permet de récupérer la valeur située à un index précis
        @param index : index de la valeur à récupérer
        @return valeur située à l'index

        <hr>
        <strong>Exemple :  </strong>
        <pre>
            mcn.getValueAt(3)
        </pre>
        """
        try:
            return self.__value[index]
        except:
            return None

    def indexOf(self, value:dict):
        """!
        <strong>indexOf</strong> est une méthode qui permet de récupérer l'index d'une valeur
        @param value : valeur à rechercher
        @return index de la valeur
        @see SicpaOpenDataPython.Entity.SimplePrimitiveNode
        @see SicpaOpenDataPython.Entity.SimpleControlledVocabularyNode
        @see SicpaOpenDataPython.Entity.MultiplePrimitiveNode
        @see SicpaOpenDataPython.Entity.MultipleControlledVocabularyNode

        <hr>
        <strong>Exemple :  </strong>
        <pre>
            mcvn.indexOf("value")
        </pre>
        """
        for val in self.__value:
            contains = True

            for key in val.keys():
                if not key in value:
                    contains = False

                if val.get(key).__class__.__name__ != value.get(key).__class__.__name__:
                    contains = False

                if isinstance(val[key], SimplePrimitiveNode):
                    if not MultipleCompoundNode.isEquals(val.get(key), value.get(key)):
                        contains = False
                
                if isinstance(val[key], SimpleControlledVocabularyNode):
                    if not MultipleCompoundNode.isEquals(val.get(key), value.get(key)):
                        contains = False

                if isinstance(val[key], MultiplePrimitiveNode):
                    if not MultipleCompoundNode.isEquals(val.get(key), value.get(key)):
                        contains = False

                if isinstance(val[key], MultipleControlledVocabularyNode):
                    if not MultipleCompoundNode.isEquals(val.get(key), value.get(key)):
                        contains = False
            
            if contains:
                return self.__value.index(value)

        return -1

    def isValid(self):
        """!
        <strong>isValid</strong> est une méthode qui permet de s'assurer de la validité du node
        @return true si le node est valide, false sinon
        
        <hr>
        <strong>Exemple : </strong>
        <pre> 
            if mcn.isValid():
                ...
        </pre>
        """
        return isinstance(self.getMultiple(), bool) and self.getMultiple()                 \
           and isinstance(self.getTypeClass(), str) and self.getTypeClass() == "compound"  \
           and isinstance(self.getTypeName(), str)                                         \
           and isinstance(self.getValue(), list)                                           \
           and self.isValueValid()                                                         \
           and self.areSubNodesValid()

    def isValueValid(self, value=None):
        """!
        <strong>isValueValid</strong> est une méthode qui permet de s'assurer de la validité de la valeur
        @param value : valeur à tester
        @return true si la valeur est valide, false sinon
        @see SicpaOpenDataPython.Entity.SimplePrimitiveNode
        @see SicpaOpenDataPython.Entity.SimpleControlledVocabularyNode
        @see SicpaOpenDataPython.Entity.MultiplePrimitiveNode
        @see SicpaOpenDataPython.Entity.MultipleControlledVocabularyNode

        <hr>
        <strong>Exemple : </strong>
        <pre> 
            if self.isValueValid(value):
                ...
        </pre>
        """
        if value != None:
            for key in value.keys():
                if not isinstance(value[key], SimplePrimitiveNode)                          \
                        and not isinstance(value[key], SimpleControlledVocabularyNode)      \
                        and not isinstance(value[key], MultiplePrimitiveNode)               \
                        and not isinstance(value[key], MultipleControlledVocabularyNode):
                    return False

            return True

        if value == None:
            for value in self.__value:
                for key in value.keys():
                    if not isinstance(value[key], SimplePrimitiveNode)                          \
                            and not isinstance(value[key], SimpleControlledVocabularyNode)      \
                            and not isinstance(value[key], MultiplePrimitiveNode)               \
                            and not isinstance(value[key], MultipleControlledVocabularyNode):
                        return False

            return True

    def removeAll(self):
        """!
        <strong>removeAll</strong> est une méthode qui permet de supprimer toutes les valeurs de la liste de valeurs

        <hr>
        <strong>Exemple : </strong>
        <pre>
            mcn.removeAll()
        </pre>
        """
        self.clearValues()

    def removeValue(self, value:dict):
        """!
        <strong>removeValue</strong> est une méthode qui permet de supprimer une valeur de la liste des valeurs
        @param value : valeur à supprimer

        <hr>
        <strong>Exemple : </strong>
        <pre>
            mcn.removeValue(value)
        </pre>
        """
        try:
            self.__value.remove(value)
        except:
            pass

    def removeValueAt(self, index:int):
        """!
        <strong>removeValue</strong> est une méthode qui permet de supprimer la valeur située à un index précis
        @param index : index de la valeur à supprimer

        <hr>
        <strong>Exemple : </strong>
        <pre>
            mcn.removeValueAt(3)
        </pre>
        """
        try:
            del self.__value[index]
        except:
            pass

    def toDict(self):
        """!
        <strong>toDict</strong> est une méthode qui permet d'obtenir le node sous forme d'un tableau
        @return la représentation du node sous forme de chaine JSON
        
        <hr>
        <strong>Exemple : </strong>
        <pre>
            array = self.toDict() 
        </pre>
        """
        subNodes = []
        subDict = dict()
        for value in self.getValue():
            for typeName in value.keys():
                subDict.update({typeName : (value.get(typeName).toDict())})
            subNodes.append(subDict)
            subDict = dict()
        return dict([
                        ('multiple',    self.getMultiple()  ),
                        ('typeClass',   self.getTypeClass() ),
                        ('typeName',    self.getTypeName()  ),
                        ('value',       subNodes            )
                    ])

    def toJSON(self, prettyPrint=True):
        """!
        <strong>toJSON</strong> est une méthode qui permet d'obtenir une représentation JSON du node
        @param  prettyPrint : indique si l'on doit adapter la chaine JSON à la lecture par l'humain
        @return la représentation du node sous forme de chaine JSON
        
        <hr>
        <strong>Exemple : </strong>
        <pre>
            json = mcn.toJSON() 
        </pre>
        """
        if not self.isValid():
            return ""

        if prettyPrint:
            return json.dumps(self.toDict(), indent=4)
        else:
            return json.dumps(self.toDict())

    def toString(self):
        """!
        <strong>toString</strong> est une méthode qui permet d'obtenir une représentation textuelle du node (JSON minifié)
        @return la représentation du node sous forme de chaine de caractère
        
        <hr>
        <strong>Exemple : </strong>
        <pre>
            json = mcn.toString() 
        </pre>
        """
        return self.toJSON(False)





    #    __  _______________ ______  ___  ________    _____________ ______________  __  __________
    #   /  |/  / __/_ __/  // / __ \/ _ \/ __/ __/   / __/_  __/ _ /_  __/  _/ __ \/ / / / __/ __/
    #  / /|_/ / _/  / / / _  / /_/ / // / _/_\ \    _\ \  / / / __ |/ / _/ // /_/ / /_/ / _/_\ \  
    # /_/  /_/___/ /_/ /_//_/\____/____/___/___/   /___/ /_/ /_/ |_/_/ /___/\___\_\____/___/___/  
    #

    @staticmethod
    def areSubnodesValid(nodeName, value:dict):
        """!
        <strong>areSubnodesValid</strong> est une méthode qui permet de s'assurer de la validité de tous les subnodes
        @param nodeName : le nom du noeud pour lequel vérifier les subnodes
        @param value : un dictionnaire contenant les subnodes à vérifier
        @return true si les subnodes sont valides, false sinon 

        <hr>
        <strong>Exemple : </strong>
        <pre>
            if MultipleCompoundNode.areSubnodesValid("name", dictionnaireSubnodes):
                ...
        </pre>
        """
        for key in value.keys():
            
            subnode = value.get(key)
            
            if not subnode.getTypeName() in AuthorizedFields.listAuthorizedFieldsForCategoryNode(nodeName):
                return False

            if not AuthorizedFields.getNodeType(subnode.getTypeName()) in subnode.__class__.__name__:
                return False

        return True

    @staticmethod
    def isEquals(node1:SimplePrimitiveNode, node2:SimplePrimitiveNode):
        """!
        <strong>isEquals</strong> est une méthode qui permet de vérifier la similarité de deux SimplePrimitiveNodes
        @param node1 : premier node à tester
        @param node2 : second node à tester
        @return true si les deux nodes sont similaires, false sinon

        <hr>
        <strong>Exemple : </strong>
        <pre>
            if(SimplePrimitiveNode.isEquals(node1,node2):
                ...
        </pre>
        """
        if node1.__class__.__name__ != node2.__class__.__name__:
            return False

        if node1.getMultiple() != node2.getMultiple():
            return False

        if node1.getTypeClass() != node2.getTypeClass():
            return False

        if node1.getTypeName() != node2.getTypeName():
            return False

        if node1.getValue() != node2.getValue():
            return False

        return True

    @staticmethod
    def isEquals(node1:SimpleControlledVocabularyNode, node2:SimpleControlledVocabularyNode):
        """!
        <strong>isEquals</strong> est une méthode qui permet de vérifier la similarité de deux SimpleControlledVocabularyNode
        @param node1 : premier node à tester
        @param node2 : second node à tester
        @return true si les deux nodes sont similaires, false sinon

        <hr>
        <strong>Exemple : </strong>
        <pre>
            if(SimpleControlledVocabularyNode.isEquals(node1,node2):
                ...
        </pre>
        """
        if node1.__class__.__name__ != node2.__class__.__name__:
            return False

        if node1.getMultiple() != node2.getMultiple():
            return False

        if node1.getTypeClass() != node2.getTypeClass():
            return False

        if node1.getTypeName() != node2.getTypeName():
            return False

        if node1.getValue() != node2.getValue():
            return False

        return True

    @staticmethod
    def isEquals(node1:MultiplePrimitiveNode, node2:MultiplePrimitiveNode):
        """!
        <strong>isEquals</strong> est une méthode qui permet de vérifier la similarité de deux MultiplePrimitiveNodes
        @param node1 : premier node à tester
        @param node2 : second node à tester
        @return true si les deux nodes sont similaires, false sinon

        <hr>
        <strong>Exemple : </strong>
        <pre>
            if(MultiplePrimitiveNode.isEquals(node1,node2):
                ...
        </pre>
        """
        if node1.__class__.__name__ != node2.__class__.__name__:
            return False

        if node1.getMultiple() != node2.getMultiple():
            return False

        if node1.getTypeClass() != node2.getTypeClass():
            return False

        if node1.getTypeName() != node2.getTypeName():
            return False

        for val in node1.getValue():
            if not val in node2.getValue():
                return False

        for val in node2.getValue():
            if not val in node1.getValue():
                return False

        return True

    @staticmethod
    def isEquals(node1:MultipleControlledVocabularyNode, node2:MultipleControlledVocabularyNode):
        """!
        <strong>isEquals</strong> est une méthode qui permet de vérifier la similarité de deux MultipleControlledVocabularyNodes
        @param node1 : premier node à tester
        @param node2 : second node à tester
        @return true si les deux nodes sont similaires, false sinon

        <hr>
        <strong>Exemple : </strong>
        <pre>
            if(MultipleControlledVocabularyNode.isEquals(node1,node2):
                ...
        </pre>
        """
        if node1.__class__.__name__ != node2.__class__.__name__:
            return False

        if node1.getMultiple() != node2.getMultiple():
            return False

        if node1.getTypeClass() != node2.getTypeClass():
            return False

        if node1.getTypeName() != node2.getTypeName():
            return False

        for val in node1.getValue():
            if not val in node2.getValue():
                return False

        for val in node2.getValue():
            if not val in node1.getValue():
                return False

        return True