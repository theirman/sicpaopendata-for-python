#! /usr/bin/env python3
# coding: utf-8
import json
from SicpaOpenDataPython.Helper.Helper import Helper

class ControlledVocabulary:
    """! 
    Classe détaillant les vocabulaires contrôlés
    @author Thierry HEIRMAN
    @since Juilet 2021
    """

    @staticmethod
    def getControlledVocabularies():
        """!
        <strong>getControlledVocabularies</strong> est une méthode qui permet d'obtenir la liste des vocabulaires contrôlés
        @return une hashtable contenant la liste des vocabulaires contrôlés
        
        <hr>
        <strong>Exemple : </strong>
        <pre>
            cv = ControlledVocabulary.GetControlledVocabularies() 
        </pre>
        """
        
        jsonContent = dict()
        url = Helper.getProperty("controlledVocabularyWS.url")
        response = Helper.getFromWS(url)

        controlledVocabularies = json.loads(response)
        for categoryKey in controlledVocabularies.keys():
            if isinstance(controlledVocabularies.get(categoryKey), str):
                continue
            if isinstance(controlledVocabularies.get(categoryKey), dict):
                categoryCV = controlledVocabularies.get(categoryKey)
                for cvKey in categoryCV.keys():
                    jsonContent.update(dict([(cvKey, categoryCV.get(cvKey))]))
        
        return jsonContent    

    @staticmethod
    def listValuesFor(node:str):
        """!
        <strong>listValuesFor</strong> est une méthode qui permet d'obtenir la liste des vocabulaires contrôlés
        @param node le noeud pour lequel recupéré les valeurs autorisées
        @return la liste de valeurs autorisées
        
        <hr>
        <strong>Exemple : </strong>
        <pre>
            values = ControlledVocabulary.listValuesFor("language") 
        </pre>
        """
    
        url = Helper.getProperty("controlledVocabularyWS.url") + "/listValuesFor?key=" + node
        response = Helper.getFromWS(url)
        return json.loads(response)

    @staticmethod
    def listValuesForAuthorIdentifierSchemes():
        """!
        <strong>listValuesForAuthorIdentifierSchemes</strong> est une méthode qui permet d'obtenir la liste des valeurs autorisées pour 'authorIdentifierScheme'
        @return la liste de valeurs autorisées
        
        <hr>
        <strong>Exemple : </strong>
        <pre>
            values = ControlledVocabulary.listValuesForAuthorIdentifierSchemes() 
        </pre>
        """
        return ControlledVocabulary.listValuesFor("authorIdentifierScheme")
    

    @staticmethod
    def listValuesForCollectionMode():
        """!
        <strong>listValuesForCollectionMode</strong> est une méthode qui permet d'obtenir la liste des valeurs autorisées pour 'collectionMode'
        @return la liste de valeurs autorisées
        
        <hr>
        <strong>Exemple : </strong>
        <pre>
            values = ControlledVocabulary.listValuesForCollectionMode() 
        </pre>
        """
        return ControlledVocabulary.listValuesFor("collectionMode")
    

    @staticmethod
    def listValuesForContributorIdentifierScheme():
        """!
        <strong>listValuesForContributorIdentifierScheme</strong> est une méthode qui permet d'obtenir la liste des valeurs autorisées pour 'contributorIdentifierScheme'
        @return la liste de valeurs autorisées
        
        <hr>
        <strong>Exemple : </strong>
        <pre>
            values = ControlledVocabulary.listValuesForContributorIdentifierScheme() 
        </pre>
        """
        return ControlledVocabulary.listValuesFor("contributorIdentifierScheme")
    

    @staticmethod
    def listValuesForContributorType():
        """!
        <strong>listValuesForContributorType</strong> est une méthode qui permet d'obtenir la liste des valeurs autorisées pour 'contributorType'
        @return la liste de valeurs autorisées
        
        <hr>
        <strong>Exemple : </strong>
        <pre>
            values = ControlledVocabulary.listValuesForContributorType() 
        </pre>
        """
        return ControlledVocabulary.listValuesFor("contributorType")
    

    @staticmethod
    def listValuesForCountry():
        """!
        <strong>listValuesForCountry</strong> est une méthode qui permet d'obtenir la liste des valeurs autorisées pour 'country'
        @return la liste de valeurs autorisées
        
        <hr>
        <strong>Exemple : </strong>
        <pre>
            values = ControlledVocabulary.listValuesForCountry() 
        </pre>
        """
        return ControlledVocabulary.listValuesFor("country")
    

    @staticmethod
    def listValuesForDataOrigin():
        """!
        <strong>listValuesForDataOrigin</strong> est une méthode qui permet d'obtenir la liste des valeurs autorisées pour 'dataOrigin'
        @return la liste de valeurs autorisées
        
        <hr>
        <strong>Exemple : </strong>
        <pre>
            values = ControlledVocabulary.listValuesForDataOrigin() 
        </pre>
        """
        return ControlledVocabulary.listValuesFor("dataOrigin")
    

    @staticmethod
    def listValuesForDegree():
        """!
        <strong>listValuesForDegree</strong> est une méthode qui permet d'obtenir la liste des valeurs autorisées pour 'degree'
        @return la liste de valeurs autorisées
        
        <hr>
        <strong>Exemple : </strong>
        <pre>
            values = ControlledVocabulary.listValuesForDegree() 
        </pre>
        """
        return ControlledVocabulary.listValuesFor("degree")
    

    @staticmethod
    def listValuesForDesignForOntologyTask():
        """!
        <strong>listValuesForDesignForOntologyTask</strong> est une méthode qui permet d'obtenir la liste des valeurs autorisées pour 'designedForOntologyTask'
        @return la liste de valeurs autorisées
        
        <hr>
        <strong>Exemple : </strong>
        <pre>
            values = ControlledVocabulary.listValuesForDesignForOntologyTask() 
        </pre>
        """
        return ControlledVocabulary.listValuesFor("designedForOntologyTask")

    @staticmethod
    def listValuesForHasFormalityLevel():
        """!
        <strong>listValuesForHasFormalityLevel</strong> est une méthode qui permet d'obtenir la liste des valeurs autorisées pour 'hasFormalityLevel'
        @return la liste de valeurs autorisées
        
        <hr>
        <strong>Exemple : </strong>
        <pre>
            values = ControlledVocabulary.listValuesForHasFormalityLevel() 
        </pre>
        """
        return ControlledVocabulary.listValuesFor("hasFormalityLevel")
    

    @staticmethod
    def listValuesForHasOntologyLanguage():
        """!
        <strong>listValuesForHasOntologyLanguage</strong> est une méthode qui permet d'obtenir la liste des valeurs autorisées pour 'hasOntologyLanguage'
        @return la liste de valeurs autorisées
        
        <hr>
        <strong>Exemple : </strong>
        <pre>
            values = ControlledVocabulary.listValuesForHasOntologyLanguage() 
        </pre>
        """
        return ControlledVocabulary.listValuesFor("hasOntologyLanguage")
    

    @staticmethod
    def listValuesForJournalArticleType():
        """!
        <strong>listValuesForJournalArticleType</strong> est une méthode qui permet d'obtenir la liste des valeurs autorisées pour 'journalArticleType'
        @return la liste de valeurs autorisées
        
        <hr>
        <strong>Exemple : </strong>
        <pre>
            values = ControlledVocabulary.listValuesForJournalArticleType() 
        </pre>
        """
        return ControlledVocabulary.listValuesFor("journalArticleType")
    

    @staticmethod
    def listValuesForKindOfData():
        """!
        <strong>listValuesForKindOfData</strong> est une méthode qui permet d'obtenir la liste des valeurs autorisées pour 'kindOfData'
        @return la liste de valeurs autorisées
        
        <hr>
        <strong>Exemple : </strong>
        <pre>
            values = ControlledVocabulary.listValuesForKindOfData() 
        </pre>
        """
        return ControlledVocabulary.listValuesFor("kindOfData")

    

    @staticmethod
    def listValuesForLanguage():
        """!
        <strong>listValuesForLanguage</strong> est une méthode qui permet d'obtenir la liste des valeurs autorisées pour 'language'
        @return la liste de valeurs autorisées
        
        <hr>
        <strong>Exemple : </strong>
        <pre>
            values = ControlledVocabulary.listValuesForLanguage() 
        </pre>
        """
        return ControlledVocabulary.listValuesFor("language")
    

    @staticmethod
    def listValuesForLifeCycleStep():
        """!
        <strong>listValuesForLifeCycleStep</strong> est une méthode qui permet d'obtenir la liste des valeurs autorisées pour 'lifeCycleStep'
        @return la liste de valeurs autorisées
        
        <hr>
        <strong>Exemple : </strong>
        <pre>
            values = ControlledVocabulary.listValuesForLifeCycleStep() 
        </pre>
        """
        return ControlledVocabulary.listValuesFor("lifeCycleStep")
    

    @staticmethod
    def listValuesForPublicationIDType():
        """!
        <strong>listValuesForPublicationIDType</strong> est une méthode qui permet d'obtenir la liste des valeurs autorisées pour 'publicationIDType'
        @return la liste de valeurs autorisées
        
        <hr>
        <strong>Exemple : </strong>
        <pre>
            values = ControlledVocabulary.listValuesForPublicationIDType() 
        </pre>
        """
        return ControlledVocabulary.listValuesFor("publicationIDType")       

    @staticmethod
    def listValuesForRelatedDatasetIDType():
        """!
        <strong>listValuesForRelatedDatasetIDType</strong> est une méthode qui permet d'obtenir la liste des valeurs autorisées pour 'relatedDatasetIDType'
        @return la liste de valeurs autorisées
        
        <hr>
        <strong>Exemple : </strong>
        <pre>
            values = ControlledVocabulary.listValuesForRelatedDatasetIDType() 
        </pre>
        """
        return ControlledVocabulary.listValuesFor("relatedDatasetIDType")
    

    @staticmethod
    def listValuesForSamplingProcedure():
        """!
        <strong>listValuesForSamplingProcedure</strong> est une méthode qui permet d'obtenir la liste des valeurs autorisées pour 'samplingProcedure'
        @return la liste de valeurs autorisées
        
        <hr>
        <strong>Exemple : </strong>
        <pre>
            values = ControlledVocabulary.listValuesForSamplingProcedure() 
        </pre>
        """
        return ControlledVocabulary.listValuesFor("samplingProcedure")

    @staticmethod
    def listValuesForStudyAssayMeasurementType():
        """!
        <strong>listValuesForStudyAssayMeasurementType</strong> est une méthode qui permet d'obtenir la liste des valeurs autorisées pour 'studyAssayMeasurementType'
        @return la liste de valeurs autorisées
        
        <hr>
        <strong>Exemple : </strong>
        <pre>
            values = ControlledVocabulary.listValuesForStudyAssayMeasurementType() 
        </pre>
        """
        return ControlledVocabulary.listValuesFor("studyAssayMeasurementType")
    

    @staticmethod
    def listValuesForStudyAssayOrganism():
        """!
        <strong>listValuesForStudyAssayOrganism</strong> est une méthode qui permet d'obtenir la liste des valeurs autorisées pour 'studyAssayOrganism'
        @return la liste de valeurs autorisées
        
        <hr>
        <strong>Exemple : </strong>
        <pre>
            values = ControlledVocabulary.listValuesForStudyAssayOrganism() 
        </pre>
        """
        return ControlledVocabulary.listValuesFor("studyAssayOrganism")
    

    @staticmethod
    def listValuesForStudyAssayPlatform():
        """!
        <strong>listValuesForStudyAssayPlatform</strong> est une méthode qui permet d'obtenir la liste des valeurs autorisées pour 'studyAssayPlatform'
        @return la liste de valeurs autorisées
        
        <hr>
        <strong>Exemple : </strong>
        <pre>
            values = ControlledVocabulary.listValuesForStudyAssayPlatform() 
        </pre>
        """
        return ControlledVocabulary.listValuesFor("studyAssayPlatform")

    

    @staticmethod
    def listValuesForStudyAssayTechnologyType():
        """!
        <strong>listValuesForStudyAssayTechnologyType</strong> est une méthode qui permet d'obtenir la liste des valeurs autorisées pour 'studyAssayTechnologyType'
        @return la liste de valeurs autorisées
        
        <hr>
        <strong>Exemple : </strong>
        <pre>
            values = ControlledVocabulary.listValuesForStudyAssayTechnologyType() 
        </pre>
        """
        return ControlledVocabulary.listValuesFor("studyAssayTechnologyType")
    

    @staticmethod
    def listValuesForStudyDesignType():
        """!
        <strong>listValuesForStudyDesignType</strong> est une méthode qui permet d'obtenir la liste des valeurs autorisées pour 'studyDesignType'
        @return la liste de valeurs autorisées
        
        <hr>
        <strong>Exemple : </strong>
        <pre>
            values = ControlledVocabulary.listValuesForStudyDesignType() 
        </pre>
        """
        return ControlledVocabulary.listValuesFor("studyDesignType")
    

    @staticmethod
    def listValuesForStudyFactorType():
        """!
        <strong>listValuesForStudyFactorType</strong> est une méthode qui permet d'obtenir la liste des valeurs autorisées pour 'studyFactorType'
        @return la liste de valeurs autorisées
        
        <hr>
        <strong>Exemple : </strong>
        <pre>
            values = ControlledVocabulary.listValuesForStudyFactorType() 
        </pre>
        """
        return ControlledVocabulary.listValuesFor("studyFactorType")
    

    @staticmethod
    def listValuesForSubject():
        """!
        <strong>listValuesForSubject</strong> est une méthode qui permet d'obtenir la liste des valeurs autorisées pour 'subject'
        @return la liste de valeurs autorisées
        
        <hr>
        <strong>Exemple : </strong>
        <pre>
            values = ControlledVocabulary.listValuesForSubject() 
        </pre>
        """
        return ControlledVocabulary.listValuesFor("subject")
    

    @staticmethod
    def listValuesForTimeMethod():
        """!
        <strong>listValuesForTimeMethod</strong> est une méthode qui permet d'obtenir la liste des valeurs autorisées pour 'timeMethod'
        @return la liste de valeurs autorisées
        
        <hr>
        <strong>Exemple : </strong>
        <pre>
            values = ControlledVocabulary.listValuesForTimeMethod() 
        </pre>
        """
        return ControlledVocabulary.listValuesFor("timeMethod")
    

    @staticmethod
    def listValuesForTypeOfSR():
        """!
        <strong>listValuesForTypeOfSR</strong> est une méthode qui permet d'obtenir la liste des valeurs autorisées pour 'typeOfSR'
        @return la liste de valeurs autorisées
        
        <hr>
        <strong>Exemple : </strong>
        <pre>
            values = ControlledVocabulary.listValuesForTypeOfSR() 
        </pre>
        """
        return ControlledVocabulary.listValuesFor("typeOfSR")
    

    @staticmethod
    def listValuesForUnitOfAnalysis():
        """!
        <strong>listValuesForUnitOfAnalysis</strong> est une méthode qui permet d'obtenir la liste des valeurs autorisées pour 'unitOfAnalysis'
        @return la liste de valeurs autorisées
        
        <hr>
        <strong>Exemple : </strong>
        <pre>
            values = ControlledVocabulary.listValuesForUnitOfAnalysis() 
        </pre>
        """
        return ControlledVocabulary.listValuesFor("unitOfAnalysis")

    @staticmethod
    def listValuesForVersionStatus():
        """!
        <strong>listValuesForVersionStatus</strong> est une méthode qui permet d'obtenir la liste des valeurs autorisées pour 'versionStatus'
        @return la liste de valeurs autorisées
        
        <hr>
        <strong>Exemple : </strong>
        <pre>
            values = ControlledVocabulary.listValuesForVersionStatus() 
        </pre>
        """
        return ControlledVocabulary.listValuesFor("versionStatus")
