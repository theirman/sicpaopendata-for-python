#! /usr/bin/env python3
# coding: utf-8

import json
from SicpaOpenDataPython.Entity.BaseNode import BaseNode

class MultiplePrimitiveNode(BaseNode):
    """!
    Classe implémentant les nodes à valeurs textuelles multiples
    @author Tom VINCENT
    @since Juillet 2022
    """
    
    #    ___ _______________  _______  __  ____________
    #   / _ /_  __/_  __/ _ \/  _/ _ )/ / / /_  __/ __/
    #  / __ |/ /   / / / , _// // _  / /_/ / / / _\ \  
    # /_/ |_/_/   /_/ /_/|_/___/____/\____/ /_/ /___/  
    #                                                  





    #   _________  _  _______________  __  ___________________  _____  ____
    #  / ___/ __ \/ |/ / __/_  __/ _ \/ / / / ___/_  __/ __/ / / / _ \/ __/
    # / /__/ /_/ /    /\ \  / / / , _/ /_/ / /__  / / / _// /_/ / , _/\ \  
    # \___/\____/_/|_/___/ /_/ /_/|_|\____/\___/ /_/ /___/\____/_/|_/___/  
    #                                                                          


    def __init__(self, typeName="", values=[]):
        """!
        Constructeur
        @param typeName :   nom du node
        @param value    :   valeurs du node
    
        <hr>
        <strong>Exemple : </strong>
        <pre>
            values = ["value1","value2","value3"]
            mpn = MultiplePrimitiveNode("name", values) 
        </pre>
        """
        super().__init__()
        self.setMultiple(True)
        self.setTypeClass("primitive")
        self.setTypeName(typeName)

        ##Cette propriété contient la valeur du node
        self.__value=[]

        self.addValues(values)
        
        



    #     ___  ____________________________  _____  ____
    #    / _ |/ ___/ ___/ __/ __/ __/ __/ / / / _ \/ __/
    #   / __ / /__/ /__/ _/_\ \_\ \/ _// /_/ / , _/\ \  
    #  /_/ |_\___/\___/___/___/___/___/\____/_/|_/___/  
    #

    def getValue(self):
        """!
        Permet d'obtenir la valeur de l'attribut <strong>value</strong> 
        @return valeur de l'attribut <strong>value</strong> 
        
        <hr>
        <strong>Exemple : </strong>
        <pre>
            value = mpn.getValue()
        </pre>
        """
        return self.__value





    #    __  _______________ ______  ___  ________
    #   /  |/  / __/_  __/ // / __ \/ _ \/ __/ __/
    #  / /|_/ / _/  / / / _  / /_/ / // / _/_\ \  
    # /_/  /_/___/ /_/ /_//_/\____/____/___/___/  
    #

    def addValue(self, value:str):
        """!
        <strong>addValue</strong> est une méthode qui permet d'ajouter une valeur à la liste des valeurs du node
        @param value : valeur à ajouter

        <hr>
        <strong>Exemple :  </strong>
        <pre>
            mpn.addValue(value)
        </pre>
        """
        self.__value.append(value)

    def addValues(self, values:list):
        """!
        <strong>addValues</strong> est une méthode qui permet d'ajouter une liste de valeurs à la liste des valeurs du node
        @param values : valeurs à ajouter

        <hr>
        <strong>Exemple :  </strong>
        <pre>
            values = ["value1","value2","value3"]
            mpn.addValues(values)
        </pre>
        """
        for value in values:
            self.addValue(value)

    def clearValues(self):
        """!
        <strong>clearValues</strong> est une méthode qui permet de réinitialiser la liste des valeurs du node

        <hr>
        <strong>Exemple :  </strong>
        <pre>
            mpn.clearValues()
        </pre>
        """
        self.__value.clear()

    def containsValue(self, value:str):
        """!
        <strong>containsValue</strong> est une méthode qui permet de vérifier si une valeur existe dans la liste des valeurs du node
        @param value : valeur à vérifier
        @return true si la valeur appartient à la liste de valeurs du node, false sinon

        <hr>
        <strong>Exemple :  </strong>
        <pre>
            mpn.containsValue("value")
        </pre>
        """
        return value in self.__value
    
    def getValueAt(self, index:int):
        """!
        <strong>getValueAt</strong> est une méthode qui permet de récupérer la valeur située à un index précis
        @param index : index de la valeur à récupérer
        @return valeur située à l'index

        <hr>
        <strong>Exemple :  </strong>
        <pre>
            mpn.getValueAt(3)
        </pre>
        """
        try:
            return self.__value[index]
        except:
            return ""

    def indexOf(self, value:str):
        """!
        <strong>indexOf</strong> est une méthode qui permet de récupérer l'index d'une valeur
        @param value : valeur à rechercher
        @return index de la valeur

        <hr>
        <strong>Exemple :  </strong>
        <pre>
            mpn.indexOf("value")
        </pre>
        """
        return self.__value.index(value)

    def isValid(self):
        """!
        <strong>isValid</strong> est une méthode qui permet de s'assurer de la validité du node
        @return true si le node est valide, false sinon
        
        <hr>
        <strong>Exemple : </strong>
        <pre> 
            if mpn.isValid():
                ...
        </pre>
        """
        return isinstance(self.getMultiple(), bool) and self.getMultiple()                  \
           and isinstance(self.getTypeClass(), str) and self.getTypeClass() == "primitive"  \
           and isinstance(self.getTypeName(), str)                                          \
           and isinstance(self.getValue(), list) and self.isValueValid()

    def isValueValid(self):
        """!
        <strong>isValueValid</strong> est une méthode qui permet de s'assurer de la validité de la valeur <br/>
        => Chaque élément de la liste de valeurs du node doit être une chaîne de caractères
        @return true si la valeur est valide, false sinon

        <hr>
        <strong>Exemple : </strong>
        <pre>
            if self.isValueValid():
                ...
        </pre>
        """
        for val in self.__value:
            if not isinstance(val, str):
                return False
        return True

    def removeAll(self):
        """!
        <strong>removeAll</strong> est une méthode qui permet de supprimer toutes les valeurs de la liste de valeurs

        <hr>
        <strong>Exemple : </strong>
        <pre>
            mpn.removeAll()
        </pre>
        """
        self.clearValues()

    def removeValue(self, value:str):
        """!
        <strong>removeValue</strong> est une méthode qui permet de supprimer une valeur de la liste des valeurs
        @param value : valeur à supprimer

        <hr>
        <strong>Exemple : </strong>
        <pre>
            mpn.removeValue("value")
        </pre>
        """
        try:
            self.__value.remove(value)
        except:
            pass

    def removeValueAt(self, index:int):
        """!
        <strong>removeValue</strong> est une méthode qui permet de supprimer la valeur située à un index précis
        @param index : index de la valeur à supprimer

        <hr>
        <strong>Exemple : </strong>
        <pre>
            mpn.removeValueAt(3)
        </pre>
        """
        try:
            del self.__value[index]
        except:
            pass

    def toDict(self):
        """!
        <strong>toDict</strong> est une méthode qui permet d'obtenir le node sous forme d'un tableau
        @return la représentation du node sous forme de chaine JSON
        
        <hr>
        <strong>Exemple : </strong>
        <pre>
            array = self.toDict() 
        </pre>
        """
        return dict([
                        ('multiple',    self.getMultiple()  ),
                        ('typeClass',   self.getTypeClass() ),
                        ('typeName',    self.getTypeName()  ),
                        ('value',       self.getValue()     )
                    ])
        

    def toJSON(self, prettyPrint=True):        
        """!
        <strong>toJSON</strong> est une méthode qui permet d'obtenir une représentation JSON du node
        @param  prettyPrint : indique si l'on doit adapter la chaine JSON à la lecture par l'humain
        @return la représentation du node sous forme de chaine JSON
        
        <hr>
        <strong>Exemple : </strong>
        <pre>
            json = mpn.toJSON() 
        </pre>
        """
        if not self.isValid():
            return ""

        if prettyPrint:
            return json.dumps(self.toDict(), indent=4)
        else:
            return json.dumps(self.toDict())


    def toString(self):
        """!
        <strong>toString</strong> est une méthode qui permet d'obtenir une représentation textuelle du node (JSON minifié)
        @return la représentation du node sous forme de chaine de caractère
        
        <hr>
        <strong>Exemple : </strong>
        <pre>
            json = mpn.toString() 
        </pre>
        """
        return self.toJSON(False)




    #    __  _______________ ______  ___  ________    _____________ ______________  __  __________
    #   /  |/  / __/_ __/  // / __ \/ _ \/ __/ __/   / __/_  __/ _ /_  __/  _/ __ \/ / / / __/ __/
    #  / /|_/ / _/  / / / _  / /_/ / // / _/_\ \    _\ \  / / / __ |/ / _/ // /_/ / /_/ / _/_\ \  
    # /_/  /_/___/ /_/ /_//_/\____/____/___/___/   /___/ /_/ /_/ |_/_/ /___/\___\_\____/___/___/  
    #