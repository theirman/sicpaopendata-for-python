#! /usr/bin/env python3
# coding: utf-8

class AuthorizedFields:
    """!
    Classe détaillant les valeurs autorisées par nodes
    @author Thierry HEIRMAN
    @since Juillet 2021
    """


    #    __  _______________ ______  ___  ________    _____________ ______________  __  __________
    #   /  |/  / __/_ __/  // / __ \/ _ \/ __/ __/   / __/_  __/ _ /_  __/  _/ __ \/ / / / __/ __/
    #  / /|_/ / _/  / / / _  / /_/ / // / _/_\ \    _\ \  / / / __ |/ / _/ // /_/ / /_/ / _/_\ \  
    # /_/  /_/___/ /_/ /_//_/\____/____/___/___/   /___/ /_/ /_/ |_/_/ /___/\___\_\____/___/___/  
    #


    @staticmethod
    def getNodeType(node:str):
        """!
        <strong>getNodeType</strong> est une méthode qui permet de récupérer le type d'un node
        @param node : le node pour lequel on souhaite récupérer le type
        @return le type de node
        
        <hr>
        <strong>Exemple : </strong>
        <pre>
            nodeType = AuthorizedFields.getNodeType("author") 
        </pre>
        """
    
        nodes = dict([
                        ("accessToSources",                     "SimplePrimitiveNode"),
                        ("actionsToMinimizeLoss",               "SimplePrimitiveNode"),
                        ("ageOfSource",                         "MultiplePrimitiveNode"),
                        ("alternativeTitle",                    "SimplePrimitiveNode"),
                        ("alternativeURL",                      "SimplePrimitiveNode"),
                        ("author",                              "MultipleCompoundNode"),
                        ("authorAffiliation",                   "SimplePrimitiveNode"),
                        ("authorIdentifier",                    "SimplePrimitiveNode"),
                        ("authorIdentifierScheme",              "SimpleControlledVocabularyNode"),
                        ("authorName",                          "SimplePrimitiveNode"),
                        ("bugDatabase",                         "SimplePrimitiveNode"),
                        ("changes",                             "SimplePrimitiveNode"),
                        ("characteristicOfSources",             "SimplePrimitiveNode"),
                        ("citations",                           "MultiplePrimitiveNode"),
                        ("city",                                "SimplePrimitiveNode"),
                        ("cleaningOperations",                  "SimplePrimitiveNode"),
                        ("collectionMode",                      "SimpleControlledVocabularyNode"),
                        ("collectionModeOther",                 "SimplePrimitiveNode"),
                        ("collectorTraining",                   "SimplePrimitiveNode"),
                        ("conformity",                          "MultipleCompoundNode"),
                        ("contributor",                         "MultipleCompoundNode"),
                        ("contributorAffiliation",              "SimplePrimitiveNode"),
                        ("contributorIdentifier",               "SimplePrimitiveNode"),
                        ("contributorIdentifierScheme",         "SimpleControlledVocabularyNode"),
                        ("contributorName",                     "SimplePrimitiveNode"),
                        ("contributorType",                     "SimpleControlledVocabularyNode"),
                        ("controlOperations",                   "SimplePrimitiveNode"),
                        ("country",                             "SimpleControlledVocabularyNode"),
                        ("dataCollectionSituation",             "SimplePrimitiveNode"),
                        ("dataCollector",                       "SimplePrimitiveNode"),
                        ("dataOrigin",                          "MultipleControlledVocabularyNode"),
                        ("datasetContact",                      "MultipleCompoundNode"),
                        ("datasetContactAffiliation",           "SimplePrimitiveNode"),
                        ("datasetContactEmail",                 "SimplePrimitiveNode"),
                        ("datasetContactName",                  "SimplePrimitiveNode"),
                        ("datasetLevelErrorNotes",              "SimplePrimitiveNode"),
                        ("dataSources",                         "MultiplePrimitiveNode"),
                        ("dateOfCollection",                    "MultipleCompoundNode"),
                        ("dateOfCollectionEnd",                 "SimplePrimitiveNode"),
                        ("dateOfCollectionStart",               "SimplePrimitiveNode"),
                        ("dateOfDeposit",                       "SimplePrimitiveNode"),
                        ("degree",                              "SimpleControlledVocabularyNode"),
                        ("depositor",                           "SimplePrimitiveNode"),
                        ("designedForOntologyTask",             "MultipleControlledVocabularyNode"),
                        ("deviationsFromSampleDesign",          "SimplePrimitiveNode"),
                        ("distributionDate",                    "SimplePrimitiveNode"),
                        ("distributor",                         "MultipleCompoundNode"),
                        ("distributorAbbreviation",             "SimplePrimitiveNode"),
                        ("distributorAffiliation",              "SimplePrimitiveNode"),
                        ("distributorLogoURL",                  "SimplePrimitiveNode"),
                        ("distributorName",                     "SimplePrimitiveNode"),
                        ("distributorURL",                      "SimplePrimitiveNode"),
                        ("dsDescription",                       "MultipleCompoundNode"),
                        ("dsDescriptionDate",                   "SimplePrimitiveNode"),
                        ("dsDescriptionValue",                  "SimplePrimitiveNode"),
                        ("eastLongitude",                       "SimplePrimitiveNode"),
                        ("endpoint",                            "SimplePrimitiveNode"),
                        ("experimentNumber",                    "MultiplePrimitiveNode"),
                        ("frequencyOfDataCollection",           "SimplePrimitiveNode"),
                        ("geographicalReferential",             "MultipleCompoundNode"),
                        ("geographicBoundingBox",               "MultipleCompoundNode"),
                        ("geographicCoverage",                  "MultipleCompoundNode"),
                        ("geographicUnit",                      "MultiplePrimitiveNode"),
                        ("grantNumber",                         "MultipleCompoundNode"),
                        ("grantNumberAgency",                   "SimplePrimitiveNode"),
                        ("grantNumberValue",                    "SimplePrimitiveNode"),
                        ("hasFormalityLevel",                   "SimpleControlledVocabularyNode"),
                        ("hasOntologyLanguage",                 "MultipleControlledVocabularyNode"),
                        ("imports",                             "MultiplePrimitiveNode"),
                        ("journalArticleType",                  "SimpleControlledVocabularyNode"),
                        ("journalIssue",                        "SimplePrimitiveNode"),
                        ("journalPubDate",                      "SimplePrimitiveNode"),
                        ("journalVolume",                       "SimplePrimitiveNode"),
                        ("journalVolumeIssue",                  "MultipleCompoundNode"),
                        ("keyword",                             "MultipleCompoundNode"),
                        ("keywordTermURI",                      "SimplePrimitiveNode"),
                        ("keywordValue",                        "SimplePrimitiveNode"),
                        ("keywordVocabulary",                   "SimplePrimitiveNode"),
                        ("keywordVocabularyURI",                "SimplePrimitiveNode"),
                        ("kindOfData",                          "MultipleControlledVocabularyNode"),
                        ("kindOfDataOther",                     "MultiplePrimitiveNode"),
                        ("knownUsage",                          "SimplePrimitiveNode"),
                        ("language",                            "MultipleControlledVocabularyNode"),
                        ("level",                               "SimplePrimitiveNode"),
                        ("lifeCycleStep",                       "MultipleControlledVocabularyNode"),
                        ("lineage",                             "MultiplePrimitiveNode"),
                        ("modificationDate",                    "SimplePrimitiveNode"),
                        ("northLongitude",                      "SimplePrimitiveNode"),
                        ("notesText",                           "SimplePrimitiveNode"),
                        ("originOfSources",                     "SimplePrimitiveNode"),
                        ("otherDataAppraisal",                  "SimplePrimitiveNode"),
                        ("otherGeographicCoverage",             "SimplePrimitiveNode"),
                        ("otherId",                             "MultipleCompoundNode"),
                        ("otherIdAgency",                       "SimplePrimitiveNode"),
                        ("otherIdValue",                        "SimplePrimitiveNode"),
                        ("otherReferences",                     "MultiplePrimitiveNode"),
                        ("priorVersion",                        "MultiplePrimitiveNode"),
                        ("producer",                            "MultipleCompoundNode"),
                        ("producerAbbreviation",                "SimplePrimitiveNode"),
                        ("producerAffiliation",                 "SimplePrimitiveNode"),
                        ("producerLogoURL",                     "SimplePrimitiveNode"),
                        ("producerName",                        "SimplePrimitiveNode"),
                        ("producerURL",                         "SimplePrimitiveNode"),
                        ("productionDate",                      "SimplePrimitiveNode"),
                        ("productionPlace",                     "SimplePrimitiveNode"),
                        ("project",                             "SimpleCompoundNode"),
                        ("projectAcronym",                      "SimplePrimitiveNode"),
                        ("projectId",                           "SimplePrimitiveNode"),
                        ("projectTask",                         "SimplePrimitiveNode"),
                        ("projectTitle",                        "SimplePrimitiveNode"),
                        ("projectURL",                          "SimplePrimitiveNode"),
                        ("projectWorkPackage",                  "SimplePrimitiveNode"),
                        ("publication",                         "MultipleCompoundNode"),
                        ("publicationCitation",                 "SimplePrimitiveNode"),
                        ("publicationIDNumber",                 "SimplePrimitiveNode"),
                        ("publicationIDType",                   "SimpleControlledVocabularyNode"),
                        ("publicationURL",                      "SimplePrimitiveNode"),
                        ("qualityValidity",                     "MultipleCompoundNode"),
                        ("relatedDataset",                      "MultipleCompoundNode"),
                        ("relatedDatasetCitation",              "SimplePrimitiveNode"),
                        ("relatedDatasetIDNumber",              "SimplePrimitiveNode"),
                        ("relatedDatasetIDType",                "SimpleControlledVocabularyNode"),
                        ("relatedDatasetURL",                   "SimplePrimitiveNode"),
                        ("relatedMaterial",                     "MultiplePrimitiveNode"),
                        ("researchInstrument",                  "SimplePrimitiveNode"),
                        ("resourceVersion",                     "SimpleCompoundNode"),
                        ("responseRate",                        "SimplePrimitiveNode"),
                        ("samplingErrorEstimates",              "SimplePrimitiveNode"),
                        ("samplingProcedure",                   "SimpleControlledVocabularyNode"),
                        ("samplingProcedureOther",              "SimplePrimitiveNode"),
                        ("series",                              "SimpleCompoundNode"),
                        ("seriesInformation",                   "SimplePrimitiveNode"),
                        ("seriesName",                          "SimplePrimitiveNode"),
                        ("socialScienceNotes",                  "SimpleCompoundNode"),
                        ("socialScienceNotesSubject",           "SimplePrimitiveNode"),
                        ("socialScienceNotesText",              "SimplePrimitiveNode"),
                        ("socialScienceNotesType",              "SimplePrimitiveNode"),
                        ("software",                            "MultipleCompoundNode"),
                        ("softwareName",                        "SimplePrimitiveNode"),
                        ("softwareVersion",                     "SimplePrimitiveNode"),
                        ("source",                              "MultipleCompoundNode"),
                        ("southLongitude",                      "SimplePrimitiveNode"),
                        ("spatialResolution",                   "MultiplePrimitiveNode"),
                        ("specification",                       "MultiplePrimitiveNode"),
                        ("state",                               "SimplePrimitiveNode"),
                        ("studyAssayCellType",                  "MultiplePrimitiveNode"),
                        ("studyAssayMeasurementType",           "MultipleControlledVocabularyNode"),
                        ("studyAssayOrganism",                  "MultipleControlledVocabularyNode"),
                        ("studyAssayOtherMeasurmentType",       "MultiplePrimitiveNode"),
                        ("studyAssayOtherOrganism",             "MultiplePrimitiveNode"),
                        ("studyAssayPlatform",                  "MultipleControlledVocabularyNode"),
                        ("studyAssayPlatformOther",             "MultiplePrimitiveNode"),
                        ("studyAssayTechnologyType",            "MultipleControlledVocabularyNode"),
                        ("studyAssayTechnologyTypeOther",       "MultiplePrimitiveNode"),
                        ("studyDesignType",                     "MultipleControlledVocabularyNode"),
                        ("studyDesignTypeOther",                "MultiplePrimitiveNode"),
                        ("studyFactorType",                     "MultipleControlledVocabularyNode"),
                        ("studyFactorTypeOther",                "MultiplePrimitiveNode"),
                        ("studyProtocolType",                   "MultiplePrimitiveNode"),
                        ("studySampleType",                     "MultiplePrimitiveNode"),
                        ("subject",                             "MultipleControlledVocabularyNode"),
                        ("subtitle",                            "SimplePrimitiveNode"),
                        ("targetSampleActualSize",              "SimplePrimitiveNode"),
                        ("targetSampleSize",                    "SimpleCompoundNode"),
                        ("targetSampleSizeFormula",             "SimplePrimitiveNode"),
                        ("timeMethod",                          "SimpleControlledVocabularyNode"),
                        ("timeMethodOther",                     "SimplePrimitiveNode"),
                        ("timePeriodCovered",                   "MultipleCompoundNode"),
                        ("timePeriodCoveredEnd",                "SimplePrimitiveNode"),
                        ("timePeriodCoveredStart",              "SimplePrimitiveNode"),
                        ("title",                               "SimplePrimitiveNode"),
                        ("topicClassification",                 "MultipleCompoundNode"),
                        ("topicClassValue",                     "SimplePrimitiveNode"),
                        ("topicClassVocab",                     "SimplePrimitiveNode"),
                        ("topicClassVocabURI",                  "SimplePrimitiveNode"),
                        ("typeOfSource",                        "MultiplePrimitiveNode"),
                        ("typeOfSR",                            "SimpleControlledVocabularyNode"),
                        ("unitOfAnalysis",                      "MultipleControlledVocabularyNode"),
                        ("unitOfAnalysisOther",                 "SimplePrimitiveNode"),
                        ("universe",                            "MultiplePrimitiveNode"),
                        ("URI",                                 "MultiplePrimitiveNode"),
                        ("version",                             "SimplePrimitiveNode"),
                        ("versionInfo",                         "SimplePrimitiveNode"),
                        ("versionStatus",                       "MultipleControlledVocabularyNode"),
                        ("weighting",                           "SimplePrimitiveNode"),
                        ("westLongitude",                       "SimplePrimitiveNode"),
                    ])

        return nodes.get(node, "")
    

    @staticmethod
    def listAuthorizedFields():
        """!
        <strong>listAuthorizedFields</strong> est une méthode qui permet de récupérer la liste de tous les fields autorisés dans le document des métadonnées
        @return la liste des fields autorisés
        
        <hr>
        <strong>Exemple : </strong>
        <pre>
            authorizedFields = AuthorizedFields.listAuthorizedFields()
        </pre>
        """
    
        authorizedFields = dict()
        authorizedFields = { **authorizedFields, **AuthorizedFields.listAuthorizedFieldsForBiomedicalNode()     }
        authorizedFields = { **authorizedFields, **AuthorizedFields.listAuthorizedFieldsForCitationNode()       }
        authorizedFields = { **authorizedFields, **AuthorizedFields.listAuthorizedFieldsForDerivedTextNode()    }
        authorizedFields = { **authorizedFields, **AuthorizedFields.listAuthorizedFieldsForGeospatialNode()     }
        authorizedFields = { **authorizedFields, **AuthorizedFields.listAuthorizedFieldsForJournalNode()        }
        authorizedFields = { **authorizedFields, **AuthorizedFields.listAuthorizedFieldsForSemanticsNode()      }
        authorizedFields = { **authorizedFields, **AuthorizedFields.listAuthorizedFieldsForSocialScienceNode()  }

        return authorizedFields        
    

    @staticmethod
    def listAuthorizedFieldsForCategoryNode(node:str):
        """!
        <strong>listAuthorizedFieldsForCategoryNode</strong> est une méthode qui permet de récupérer la liste de tous les fields autorisés pour un node donné
        @param node : le node pour lequel lister les champs autorisés
        @return la liste des fields autorisés pour le node passé en paramètre
        
        <hr>
        <strong>Exemple : </strong>
        <pre>
            authorizedFields = AuthorizedFields.listAuthorizedFieldsForCategoryNode("citation")
        </pre>
        """

        nodes = dict([
                        ("biomedical",      AuthorizedFields.listAuthorizedFieldsForBiomedicalNode()),
                        ("citation",        AuthorizedFields.listAuthorizedFieldsForCitationNode()),
                        ("Derived-text",    AuthorizedFields.listAuthorizedFieldsForDerivedTextNode()),
                        ("geospatial",      AuthorizedFields.listAuthorizedFieldsForGeospatialNode()),
                        ("journal",         AuthorizedFields.listAuthorizedFieldsForJournalNode()),
                        ("semantics",       AuthorizedFields.listAuthorizedFieldsForSemanticsNode()),
                        ("socialscience",   AuthorizedFields.listAuthorizedFieldsForSocialScienceNode()),
                    ])

        return nodes.get(node, AuthorizedFields.listAuthorizedFieldsForCompoundSubnode(node))
        
    

    @staticmethod
    def listAuthorizedFieldsForCompoundSubnode(compoundSubnode:str):
        """!
        <strong>listAuthorizedFieldsForCompoundSubNode</strong> est une méthode qui permet de récupérer la liste de tous les fields autorisés pour un subnode donné
        @param compoundSubnode : le subnode pour lequel lister les champs autorisés
        @return la liste des fields autorisés pour le subnode passé en paramètre
        
        <hr>
        <strong>Exemple : </strong>
        <pre>
            authorizedFields = AuthorizedFields.listAuthorizedFieldsForCompoundSubNode("author") 
        </pre>
        """
    
        nodes = dict([
                        ("author",                      dict([
                                                                ("authorAffiliation",               "SimplePrimitiveNode"),
                                                                ("authorIdentifier",                "SimplePrimitiveNode"),
                                                                ("authorIdentifierScheme",          "SimpleControlledVocabularyNode"),
                                                                ("authorName",                      "SimplePrimitiveNode"),
                                                            ])
                        ),
                        ("contributor",                 dict([
                                                                ("contributorAffiliation",          "SimplePrimitiveNode"),
                                                                ("contributorIdentifier",           "SimplePrimitiveNode"),
                                                                ("contributorIdentifierScheme",     "SimpleControlledVocabularyNode"),
                                                                ("contributorName",                 "SimplePrimitiveNode"),
                                                                ("contributorType",                 "SimpleControlledVocabularyNode"),
                                                            ])
                        ),
                        ("datasetContact",              dict([
                                                                ("datasetContactAffiliation",       "SimplePrimitiveNode"),
                                                                ("datasetContactEmail",             "SimplePrimitiveNode"),
                                                                ("datasetContactName",              "SimplePrimitiveNode"),
                                                            ])
                        ),
                        ("dateOfCollection",            dict([
                                                                ("dateOfCollectionStart",           "SimplePrimitiveNode"),
                                                                ("dateOfCollectionEnd",             "SimplePrimitiveNode"),
                                                            ])
                        ),
                        ("distributor",                 dict([
                                                                ("distributorAbbreviation",         "SimplePrimitiveNode"),
                                                                ("distributorAffiliation",          "SimplePrimitiveNode"),
                                                                ("distributorLogoURL",              "SimplePrimitiveNode"),
                                                                ("distributorName",                 "SimplePrimitiveNode"),
                                                                ("distributorURL",                  "SimplePrimitiveNode"),
                                                            ])
                        ),
                        ("dsDescription",               dict([
                                                                ("dsDescriptionDate",               "SimplePrimitiveNode"),
                                                                ("dsDescriptionValue",              "SimplePrimitiveNode"),
                                                            ])
                        ),
                        ("grantNumber",                 dict([
                                                                ("grantNumberAgency",               "SimplePrimitiveNode"),
                                                                ("grantNumberValue",                "SimplePrimitiveNode"),
                                                            ])
                        ),
                        ("keyword",                     dict([
                                                                ("keywordTermURI",                  "SimplePrimitiveNode"),
                                                                ("keywordValue",                    "SimplePrimitiveNode"),
                                                                ("keywordVocabulary",               "SimplePrimitiveNode"),
                                                                ("keywordVocabularyURI",            "SimplePrimitiveNode"),
                                                            ])
                        ),
                        ("otherId",                     dict([
                                                                ("otherIdAgency",                   "SimplePrimitiveNode"),
                                                                ("otherIdValue",                    "SimplePrimitiveNode"),
                                                            ])
                        ),
                        ("producer",                    dict([
                                                                ("producerAbbreviation",            "SimplePrimitiveNode"),
                                                                ("producerAffiliation",             "SimplePrimitiveNode"),
                                                                ("producerLogoURL",                 "SimplePrimitiveNode"),
                                                                ("producerName",                    "SimplePrimitiveNode"),
                                                                ("producerURL",                     "SimplePrimitiveNode"),
                                                            ])
                        ),
                        ("project",                     dict([
                                                                ("projectAcronym",                  "SimplePrimitiveNode"),
                                                                ("projectId",                       "SimplePrimitiveNode"),
                                                                ("projectTask",                     "SimplePrimitiveNode"),
                                                                ("projectTitle",                    "SimplePrimitiveNode"),
                                                                ("projectURL",                      "SimplePrimitiveNode"),
                                                                ("projectWorkPackage",              "SimplePrimitiveNode"),
                                                            ])
                        ),
                        ("publication",                 dict([
                                                                ("publicationCitation",             "SimplePrimitiveNode"),
                                                                ("publicationIDNumber",             "SimplePrimitiveNode"),
                                                                ("publicationIDType",               "SimpleControlledVocabularyNode"),
                                                                ("publicationURL",                  "SimplePrimitiveNode"),
                                                            ])
                        ),
                        ("relatedDataset",              dict([
                                                                ("relatedDatasetCitation",          "SimplePrimitiveNode"),
                                                                ("relatedDatasetIDNumber",          "SimplePrimitiveNode"),
                                                                ("relatedDatasetIDType",            "SimpleControlledVocabularyNode"),
                                                                ("relatedDatasetURL",               "SimplePrimitiveNode"),
                                                            ])
                        ),
                        ("series",                      dict([
                                                                ("seriesInformation",               "SimplePrimitiveNode"),
                                                                ("seriesName",                      "SimplePrimitiveNode"),
                                                            ])
                        ),
                        ("software",                    dict([
                                                                ("softwareName",                    "SimplePrimitiveNode"),
                                                                ("softwareVersion",                 "SimplePrimitiveNode"),
                                                            ])
                        ),
                        ("timePeriodCovered",           dict([
                                                                ("timePeriodCoveredStart",          "SimplePrimitiveNode"),
                                                                ("timePeriodCoveredEnd",            "SimplePrimitiveNode"),
                                                            ])
                        ),
                        ("topicClassification",         dict([
                                                                ("topicClassValue",                 "SimplePrimitiveNode"),
                                                                ("topicClassVocab",                 "SimplePrimitiveNode"),
                                                                ("topicClassVocabURI",              "SimplePrimitiveNode"),
                                                            ])
                        ),
                        ("conformity",                  dict([
                                                                ("degree",                          "SimpleControlledVocabularyNode"),
                                                                ("specification",                   "MultiplePrimitiveNode"),
                                                            ])
                        ),
                        ("geographicBoundingBox",       dict([
                                                                ("eastLongitude",                   "SimplePrimitiveNode"),
                                                                ("northLongitude",                  "SimplePrimitiveNode"),
                                                                ("southLongitude",                  "SimplePrimitiveNode"),
                                                                ("westLongitude",                   "SimplePrimitiveNode"),
                                                            ])
                        ),
                        ("geographicCoverage",          dict([
                                                                ("city",                            "SimplePrimitiveNode"),
                                                                ("country",                         "SimpleControlledVocabularyNode"),
                                                                ("otherGeographicCoverage",         "SimplePrimitiveNode"),
                                                                ("state",                           "SimplePrimitiveNode"),
                                                            ])
                        ),
                        ("qualityValidity",             dict([
                                                                ("lineage",                         "MultiplePrimitiveNode"),
                                                                ("spatialResolution",               "MultiplePrimitiveNode"),
                                                            ])
                        ),
                        ("geographicalReferential",     dict([
                                                                ("level",                           "SimplePrimitiveNode"),
                                                                ("version",                         "SimplePrimitiveNode"),
                                                            ])
                        ),
                        ("socialScienceNotes",          dict([
                                                                ("socialScienceNotesSubject",       "SimplePrimitiveNode"),
                                                                ("socialScienceNotesText",          "SimplePrimitiveNode"),
                                                                ("socialScienceNotesType",          "SimplePrimitiveNode"),
                                                            ])
                        ),
                        ("targetSampleSize",            dict([
                                                                ("targetSampleActualSize",          "SimplePrimitiveNode"),
                                                                ("targetSampleSizeFormula",         "SimplePrimitiveNode"),
                                                            ])
                        ),
                        ("journalVolumeIssue",          dict([
                                                                ("journalIssue",                    "SimplePrimitiveNode"),
                                                                ("journalPubDate",                  "SimplePrimitiveNode"),
                                                                ("journalVolume",                   "SimplePrimitiveNode"),
                                                            ])
                        ),
                        ("source",                      dict([
                                                                ("ageOfSource",                     "MultiplePrimitiveNode"),
                                                                ("citations",                       "MultiplePrimitiveNode"),
                                                                ("experimentNumber",                "MultiplePrimitiveNode"),
                                                                ("typeOfSource",                    "MultiplePrimitiveNode"),
                                                            ])
                        ),
                        ("resourceVersion",             dict([
                                                                ("changes",                         "SimplePrimitiveNode"),
                                                                ("modificationDate",                "SimplePrimitiveNode"),
                                                                ("priorVersion",                    "MultiplePrimitiveNode"),
                                                                ("versionInfo",                     "SimplePrimitiveNode"),
                                                                ("versionStatus",                   "MultipleControlledVocabularyNode"),
                                                            ])
                        ),
                    ])

        return nodes.get(compoundSubnode, dict())
        
    

    @staticmethod
    def listAuthorizedFieldsForBiomedicalNode():
        """!
        <strong>listAuthorizedFieldsForBiomedicalNode</strong> est une méthode qui permet de récupérer la liste de tous les fields autorisés dans le noeud "biomedical"
        @return la liste des fields autorisés
        
        <hr>
        <strong>Exemple : </strong>
        <pre>
            authorizedFields = AuthorizedFields.listAuthorizedFieldsForBiomedicalNode() 
        </pre>
        """
    
        return dict([
            ("studyAssayCellType",                  "MultiplePrimitiveNode"),
            ("studyAssayMeasurementType",           "MultipleControlledVocabularyNode"),
            ("studyAssayOrganism",                  "MultipleControlledVocabularyNode"),
            ("studyAssayOtherMeasurmentType",       "MultiplePrimitiveNode"),
            ("studyAssayOtherOrganism",             "MultiplePrimitiveNode"),
            ("studyAssayPlatform",                  "MultipleControlledVocabularyNode"),
            ("studyAssayPlatformOther",             "MultiplePrimitiveNode"),
            ("studyAssayTechnologyType",            "MultipleControlledVocabularyNode"),
            ("studyAssayTechnologyTypeOther",       "MultiplePrimitiveNode"),
            ("studyDesignType",                     "MultipleControlledVocabularyNode"),
            ("studyDesignTypeOther",                "MultiplePrimitiveNode"),
            ("studyFactorType",                     "MultipleControlledVocabularyNode"),
            ("studyFactorTypeOther",                "MultiplePrimitiveNode"),
            ("studyProtocolType",                   "MultiplePrimitiveNode"),
            ("studySampleType",                     "MultiplePrimitiveNode"),
        ])
    

    @staticmethod
    def listAuthorizedFieldsForCitationNode():
        """!
        <strong>listAuthorizedFieldsForCitationNode</strong> est une méthode qui permet de récupérer la liste de tous les fields autorisés dans le noeud "citation"
        @return la liste des fields autorisés
        
        <hr>
        <strong>Exemple : </strong>
        <pre>
            authorizedFields = AuthorizedFields.listAuthorizedFieldsForCitationNode() 
        </pre>
        """
    
        return dict([
            ("accessToSources",               "SimplePrimitiveNode"),
            ("alternativeTitle",              "SimplePrimitiveNode"),
            ("alternativeURL",                "SimplePrimitiveNode"),
            ("author",                        "MultipleCompoundNode"),
            ("characteristicOfSources",       "SimplePrimitiveNode"),
            ("contributor",                   "MultipleCompoundNode"),
            ("dataOrigin",                    "MultipleControlledVocabularyNode"),
            ("datasetContact",                "MultipleCompoundNode"),
            ("dataSources",                   "MultiplePrimitiveNode"),
            ("dateOfCollection",              "MultipleCompoundNode"),
            ("dateOfDeposit",                 "SimplePrimitiveNode"),
            ("depositor",                     "SimplePrimitiveNode"),
            ("distributionDate",              "SimplePrimitiveNode"),
            ("distributor",                   "MultipleCompoundNode"),
            ("dsDescription",                 "MultipleCompoundNode"),
            ("grantNumber",                   "MultipleCompoundNode"),
            ("keyword",                       "MultipleCompoundNode"),
            ("kindOfData",                    "MultipleControlledVocabularyNode"),
            ("kindOfDataOther",               "MultiplePrimitiveNode"),
            ("language",                      "MultipleControlledVocabularyNode"),
            ("lifeCycleStep",                 "MultipleControlledVocabularyNode"),
            ("notesText",                     "SimplePrimitiveNode"),
            ("originOfSources",               "SimplePrimitiveNode"),
            ("otherId",                       "MultipleCompoundNode"),
            ("otherReferences",               "MultiplePrimitiveNode"),
            ("producer",                      "MultipleCompoundNode"),
            ("productionDate",                "SimplePrimitiveNode"),
            ("productionPlace",               "SimplePrimitiveNode"),
            ("project",                       "SimpleCompoundNode"),
            ("publication",                   "MultipleCompoundNode"),
            ("relatedDataset",                "MultipleCompoundNode"),
            ("relatedMaterial",               "MultiplePrimitiveNode"),
            ("series",                        "SimpleCompoundNode"),
            ("software",                      "MultipleCompoundNode"),
            ("subject",                       "MultipleControlledVocabularyNode"),
            ("subtitle",                      "SimplePrimitiveNode"),
            ("timePeriodCovered",             "MultipleCompoundNode"),
            ("title",                         "SimplePrimitiveNode"),
            ("topicClassification",           "MultipleCompoundNode"),
        ])
    

    @staticmethod
    def listAuthorizedFieldsForDerivedTextNode():
        """!
        <strong>listAuthorizedFieldsForDerivedTextNode</strong> est une méthode qui permet de récupérer la liste de tous les fields autorisés dans le noeud "Derived-text"
        @return la liste des fields autorisés
        
        <hr>
        <strong>Exemple : </strong>
        <pre>
            authorizedFields = AuthorizedFields.listAuthorizedFieldsForDerivedTextNode() 
        </pre>
        """
    
        return dict([
            ("source",    "MultipleCompoundNode"),
        ])
    

    @staticmethod
    def listAuthorizedFieldsForGeospatialNode():
        """!
        <strong>listAuthorizedFieldsForGeospatialNode</strong> est une méthode qui permet de récupérer la liste de tous les fields autorisés dans le noeud "geospatial"
        @return la liste des fields autorisés
        
        <hr>
        <strong>Exemple : </strong>
        <pre>
            authorizedFields = AuthorizedFields.listAuthorizedFieldsForGeospatialNode() 
        </pre>
        """
    
        return dict([
            ("conformity",                "MultipleCompoundNode"),
            ("geographicBoundingBox",     "MultipleCompoundNode"),
            ("geographicCoverage",        "MultipleCompoundNode"),
            ("geographicUnit",            "MultiplePrimitiveNode"),
            ("qualityValidity",           "MultipleCompoundNode"),
        ])
    

    @staticmethod
    def listAuthorizedFieldsForJournalNode():
        """!
        <strong>listAuthorizedFieldsForJournalNode</strong> est une méthode qui permet de récupérer la liste de tous les fields autorisés dans le noeud "journal"
        @return la liste des fields autorisés
        
        <hr>
        <strong>Exemple : </strong>
        <pre>
            authorizedFields = AuthorizedFields.listAuthorizedFieldsForJournalNode() 
        </pre>
        """
    
        return dict([
            ("journalArticleType",    "SimpleControlledVocabularyNode"),
            ("journalVolumeIssue",    "MultipleCompoundNode"),
        ])
    

    @staticmethod
    def listAuthorizedFieldsForSemanticsNode():
        """!
        <strong>listAuthorizedFieldsForSemanticsNode</strong> est une méthode qui permet de récupérer la liste de tous les fields autorisés dans le noeud "semantics"
        @return la liste des fields autorisés
        
        <hr>
        <strong>Exemple : </strong>
        <pre>
            authorizedFields = AuthorizedFields.listAuthorizedFieldsForSemanticsNode() 
        </pre>
        """
    
        return dict([
            ("bugDatabase",                   "SimplePrimitiveNode"),
            ("designedForOntologyTask",       "MultipleControlledVocabularyNode"),
            ("endpoint",                      "SimplePrimitiveNode"),
            ("hasFormalityLevel",             "SimpleControlledVocabularyNode"),
            ("hasOntologyLanguage",           "MultipleControlledVocabularyNode"),
            ("knownUsage",                    "SimplePrimitiveNode"),
            ("imports",                       "MultiplePrimitiveNode"),
            ("resourceVersion",               "SimpleCompoundNode"),
            ("typeOfSR",                      "SimpleControlledVocabularyNode"),
            ("URI",                           "MultiplePrimitiveNode"),
        ])
    

    @staticmethod
    def listAuthorizedFieldsForSocialScienceNode():
        """!
        <strong>listAuthorizedFieldsForSocialScienceNode</strong> est une méthode qui permet de récupérer la liste de tous les fields autorisés dans le noeud "socialscience"
        @return la liste des fields autorisés
        
        <hr>
        <strong>Exemple : </strong>
        <pre>
            authorizedFields = AuthorizedFields.listAuthorizedFieldsForSocialScienceNode() 
        </pre>
        """
    
        return dict([
            ("actionsToMinimizeLoss",         "SimplePrimitiveNode"),
            ("cleaningOperations",            "SimplePrimitiveNode"),
            ("collectionMode",                "SimpleControlledVocabularyNode"),
            ("collectionModeOther",           "SimplePrimitiveNode"),
            ("collectorTraining",             "SimplePrimitiveNode"),
            ("controlOperations",             "SimplePrimitiveNode"),
            ("dataCollectionSituation",       "SimplePrimitiveNode"),
            ("dataCollector",                 "SimplePrimitiveNode"),
            ("datasetLevelErrorNotes",        "SimplePrimitiveNode"),
            ("deviationsFromSampleDesign",    "SimplePrimitiveNode"),
            ("frequencyOfDataCollection",     "SimplePrimitiveNode"),
            ("geographicalReferential",       "MultipleCompoundNode"),
            ("otherDataAppraisal",            "SimplePrimitiveNode"),
            ("researchInstrument",            "SimplePrimitiveNode"),
            ("responseRate",                  "SimplePrimitiveNode"),
            ("samplingErrorEstimates",        "SimplePrimitiveNode"),
            ("samplingProcedure",             "SimpleControlledVocabularyNode"),
            ("samplingProcedureOther",        "SimplePrimitiveNode"),
            ("socialScienceNotes",            "SimpleCompoundNode"),
            ("targetSampleSize",              "SimpleCompoundNode"),
            ("timeMethod",                    "SimpleControlledVocabularyNode"),
            ("timeMethodOther",               "SimplePrimitiveNode"),
            ("unitOfAnalysis",                "MultipleControlledVocabularyNode"),
            ("unitOfAnalysisOther",           "SimplePrimitiveNode"),
            ("universe",                      "MultiplePrimitiveNode"),
            ("weighting",                     "SimplePrimitiveNode"),
        ])
    
