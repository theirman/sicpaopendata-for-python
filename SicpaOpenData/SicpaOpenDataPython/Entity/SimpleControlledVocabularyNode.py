#! /usr/bin/env python3
# coding: utf-8

import json
from SicpaOpenDataPython.Entity.BaseNode import BaseNode
from SicpaOpenDataPython.Entity.ControlledVocabulary import ControlledVocabulary

class SimpleControlledVocabularyNode(BaseNode):
    """!
    Classe implémentant les nodes à valeur textuelle unique, valeur contrainte par un vocabulaire contrôlé
    @author Tom VINCENT
    @since Juillet 2022
    """    
    
    #    ___ _______________  _______  __  ____________
    #   / _ /_  __/_  __/ _ \/  _/ _ )/ / / /_  __/ __/
    #  / __ |/ /   / / / , _// // _  / /_/ / / / _\ \  
    # /_/ |_/_/   /_/ /_/|_/___/____/\____/ /_/ /___/  
    #                                                  





    #   _________  _  _______________  __  ___________________  _____  ____
    #  / ___/ __ \/ |/ / __/_  __/ _ \/ / / / ___/_  __/ __/ / / / _ \/ __/
    # / /__/ /_/ /    /\ \  / / / , _/ /_/ / /__  / / / _// /_/ / , _/\ \  
    # \___/\____/_/|_/___/ /_/ /_/|_|\____/\___/ /_/ /___/\____/_/|_/___/  
    #            
   
    def __init__(self, typeName="", value=""):
        """!
        Constructeur
        @param typeName :   nom du node
        @param value    :   valeur du node
        
        <hr>
        <strong>Exemple : </strong>
        <pre>
            scvn = SimpleControlledVocabularyNode("name", "value") 
        </pre>
        """
        super().__init__()
        self.setMultiple(False)
        self.setTypeClass("controlledVocabulary")
        self.setTypeName(typeName)
        self.setValue(value)





    #     ___  ____________________________  _____  ____
    #    / _ |/ ___/ ___/ __/ __/ __/ __/ / / / _ \/ __/
    #   / __ / /__/ /__/ _/_\ \_\ \/ _// /_/ / , _/\ \  
    #  /_/ |_\___/\___/___/___/___/___/\____/_/|_/___/  
    #

    def getValue(self):
        """!
        Permet d'obtenir la valeur de l'attribut <strong>value</strong> 
        @return valeur de l'attribut <strong>value</strong> 
        
        <hr>
        <strong>Exemple : </strong>
        <pre>
            value = scvn.getValue()
        </pre>
        """
        return self.__value
    
    def setValue(self, value:str):
        """!
        Permet de mettre à jour la valeur de l'attribut <strong>value</strong>
        @param value : valeur à enregistrer dans l'attribut <strong>value</strong>
        
        <hr>
        <strong>Exemple : </strong>
        <pre>
            scvn.setValue("value") 
        </pre>
        """
        if self.isAuthorizedValue(value):
            ##Cette propriété contient la valeur du node
            self.__value = value
        else:
            ##Cette propriété contient la valeur du node
            self.__value = ""
        




    #    __  _______________ ______  ___  ________
    #   /  |/  / __/_  __/ // / __ \/ _ \/ __/ __/
    #  / /|_/ / _/  / / / _  / /_/ / // / _/_\ \  
    # /_/  /_/___/ /_/ /_//_/\____/____/___/___/  
    #
    
    def isAuthorizedValue(self, value:str):
        """!
        <strong>isAuthorizedValue</strong> est une méthode qui permet de s'assurer que la valeur en paramètre est bien une valeur autorisée par le vocabulaire contrôlé
        @param value : valeur à tester
        @return true si la valeur est valide, false sinon
        
        <hr>
        <strong>Exemple : </strong>
        <pre> 
            if scvn.isAuthorizedValue("value"):
                ...      
        </pre>
        """      
        controlledVocabulary = ControlledVocabulary.listValuesFor(self.getTypeName())
        return value in controlledVocabulary
        

    def isValid(self):
        """!
        <strong>isValid</strong> est une méthode qui permet de s'assurer de la validité du node
        @return true si le node est valide, false sinon
        
        <hr>
        <strong>Exemple : </strong>
        <pre> 
            if scvn.isValid():
                ...
        </pre>
        """
        return isinstance(self.getMultiple(), bool) and not self.getMultiple()                          \
           and isinstance(self.getTypeClass(), str) and self.getTypeClass() == "controlledVocabulary"   \
           and isinstance(self.getTypeName(), str)                                                      \
           and isinstance(self.getValue(), str)
     
    def toDict(self):
        """!
        <strong>toDict</strong> est une méthode qui permet d'obtenir le node sous forme d'un tableau
        @return la représentation du node sous forme de chaine JSON
        
        <hr>
        <strong>Exemple : </strong>
        <pre>
            array = self.toDict() 
        </pre>
        """
        return dict([
                        ('multiple',    self.getMultiple()  ),
                        ('typeClass',   self.getTypeClass() ),
                        ('typeName',    self.getTypeName()  ),
                        ('value',       self.getValue()     )
                    ])

    def toJSON(self, prettyPrint=True):
        """!
        <strong>toJSON</strong> est une méthode qui permet d'obtenir une représentation JSON du node
        @param  prettyPrint : indique si l'on doit adapter la chaine JSON à la lecture par l'humain
        @return la représentation du node sous forme de chaine JSON
        
        <hr>
        <strong>Exemple : </strong>
        <pre>
            json = scvn.toJSON() 
        </pre>
        """
        if not self.isValid():
            return ""

        if prettyPrint:
            return json.dumps(self.toDict(), indent=4)
        else:
            return json.dumps(self.toDict())

    def toString(self):
        """!
        <strong>toString</strong> est une méthode qui permet d'obtenir une représentation textuelle du node (JSON minifié)
        @return la représentation du node sous forme de chaine de caractère
        
        <hr>
        <strong>Exemple : </strong>
        <pre>
            json = scvn.toString() 
        </pre>
        """
        return self.toJSON(False)




    #    __  _______________ ______  ___  ________    _____________ ______________  __  __________
    #   /  |/  / __/_ __/  // / __ \/ _ \/ __/ __/   / __/_  __/ _ /_  __/  _/ __ \/ / / / __/ __/
    #  / /|_/ / _/  / / / _  / /_/ / // / _/_\ \    _\ \  / / / __ |/ / _/ // /_/ / /_/ / _/_\ \  
    # /_/  /_/___/ /_/ /_//_/\____/____/___/___/   /___/ /_/ /_/ |_/_/ /___/\___\_\____/___/___/  
    #