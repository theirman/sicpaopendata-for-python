#! /usr/bin/env python3
# coding: utf-8

from urllib.request import urlopen, Request

class Helper():
    """Utilisation de webservices pour récupérer le vocabulaire contrôlé"""

    def getFromWS(url):
        request = Request(url, headers={"Content-Type" : "application/json"})
        try:
            with urlopen(request) as response:
                return response.read()
        except HTTPError as error:
            print(error.status, error.reason)
        except URLError as error:
            print(error.reason)

    def getProperty(propertyName):
        properties = dict([("controlledVocabularyWS.url", "https://sicpa-interop.inra.fr/ControlledVocabularyWS-prod/rest/controlledVocabularies")])
        return properties.get(propertyName)
